-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2020 at 02:57 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fecpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `picture` text NOT NULL,
  `is_trash` int(11) DEFAULT 0,
  `cat_shortdesc` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `picture`, `is_trash`, `cat_shortdesc`) VALUES
(1, 'Alkaram Studio', '', 0, 'Alkaram Studio'),
(2, 'Khaadi', '', 0, 'Khaadi'),
(3, 'Bareeze', '', 0, 'Bareeze'),
(4, 'HSY Studio', '', 0, 'HSY Studio'),
(5, 'ChenOne', '', 0, 'ChenOne'),
(6, 'Gul Ahmed', '', 0, 'Gul Ahmed'),
(7, 'Junaid Jamshed', '', 0, 'Junaid Jamshed');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `phone_no` varchar(100) DEFAULT NULL,
  `fax_no` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `is_trash` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `phone_no`, `fax_no`, `email`, `address`, `is_trash`) VALUES
(1, 'Nove Global', '000', '000', 'www.noveglobal.com', NULL, 0),
(2, 'Distributer of Fauji serial', '0912600103', 'NIL', 'NIL', NULL, 0),
(3, 'National Masala jaat', '03459050201', 'Nil', 'Nil', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `core_settings`
--

CREATE TABLE `core_settings` (
  `c_set_id` int(11) NOT NULL,
  `purchase_price` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `cusomer_name` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `cr_by` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `contact` varchar(200) DEFAULT NULL,
  `is_trash` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `cusomer_name`, `address`, `cr_by`, `email`, `contact`, `is_trash`) VALUES
(1, 'john wick', 'peshawar', 1, 'john@gmail.com', '0303450285', 0),
(100343, NULL, '', NULL, '', '', 0),
(100344, NULL, '', NULL, '', '', 0),
(100345, NULL, NULL, NULL, NULL, NULL, 0),
(100346, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `layout_settings`
--

CREATE TABLE `layout_settings` (
  `layset_id` int(11) NOT NULL,
  `sidebar` varchar(200) DEFAULT NULL,
  `header` varchar(200) DEFAULT NULL,
  `sidebar_icon` varchar(200) DEFAULT NULL,
  `submenu_icon` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `layout_settings`
--

INSERT INTO `layout_settings` (`layset_id`, `sidebar`, `header`, `sidebar_icon`, `submenu_icon`) VALUES
(1, '', '', 'icon-style-1', 'icon-list-style-4');

-- --------------------------------------------------------

--
-- Table structure for table `logo_setting`
--

CREATE TABLE `logo_setting` (
  `logo_set_id` int(11) NOT NULL,
  `logo_set_contact` varchar(200) DEFAULT NULL,
  `logo_set_align` varchar(100) DEFAULT NULL,
  `logo_set_email` varchar(100) DEFAULT NULL,
  `logo_set_logo` varchar(200) DEFAULT NULL,
  `logo_set_address` varchar(200) DEFAULT NULL,
  `app_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logo_setting`
--

INSERT INTO `logo_setting` (`logo_set_id`, `logo_set_contact`, `logo_set_align`, `logo_set_email`, `logo_set_logo`, `logo_set_address`, `app_name`) VALUES
(1, '03333334455', 'center', 'email@gmail.com', 'img3.jpg', '     islamabad pakistan ', 'Effie Pos');

-- --------------------------------------------------------

--
-- Table structure for table `prd_tag`
--

CREATE TABLE `prd_tag` (
  `prdt_id` int(11) NOT NULL,
  `prd_id` int(11) DEFAULT 0,
  `tag_id` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_tag`
--

INSERT INTO `prd_tag` (`prdt_id`, `prd_id`, `tag_id`) VALUES
(1, 9, 2),
(2, 9, 3),
(3, 10, 4),
(4, 11, 5),
(5, 12, 6),
(6, 13, 7),
(7, 14, 8),
(8, 15, 9),
(9, 16, 10),
(10, 17, 11),
(11, 18, 10),
(12, 18, 12),
(13, 19, 2),
(14, 20, 2),
(15, 21, 2),
(16, 22, 2),
(17, 23, 2),
(18, 24, 2),
(19, 25, 2),
(20, 26, 2),
(21, 5, 2),
(22, 5, 13),
(23, 5, 2),
(24, 5, 14),
(25, 5, 2),
(26, 5, 15),
(27, 5, 2),
(28, 5, 13),
(29, 5, 14),
(30, 5, 15),
(31, 5, 2),
(32, 5, 13),
(33, 5, 14),
(34, 5, 15),
(35, 5, 2),
(36, 5, 5),
(37, 5, 13),
(38, 5, 2),
(39, 5, 5),
(40, 5, 13),
(41, 5, 14),
(42, 5, 15),
(43, 5, 2),
(44, 5, 5),
(45, 5, 13),
(46, 5, 14),
(47, 5, 15),
(48, 5, 2),
(49, 5, 5),
(50, 5, 13),
(51, 5, 14),
(52, 5, 15),
(53, 5, 2),
(54, 5, 5),
(55, 5, 13),
(56, 5, 14),
(57, 5, 15),
(58, 5, 2),
(59, 5, 5),
(60, 5, 13),
(61, 5, 14),
(62, 5, 15),
(63, 5, 2),
(64, 5, 5),
(65, 5, 13),
(66, 5, 14),
(67, 5, 15),
(68, 5, 2),
(69, 5, 5),
(70, 5, 13),
(71, 5, 14),
(72, 5, 15),
(73, 5, 2),
(74, 5, 5),
(75, 5, 13),
(76, 5, 14),
(77, 5, 15),
(78, 5, 2),
(79, 5, 5),
(80, 5, 13),
(81, 5, 14),
(82, 5, 15),
(83, 5, 2),
(84, 5, 5),
(85, 5, 13),
(86, 5, 14),
(87, 5, 15),
(88, 27, 3),
(89, 27, 4);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `tags` varchar(100) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `purchase_price` double DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `is_trash` int(11) DEFAULT 0,
  `discount_tags` varchar(100) DEFAULT NULL,
  `discount_price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `title`, `barcode`, `tags`, `type_id`, `category_id`, `sale_price`, `purchase_price`, `color`, `is_trash`, `discount_tags`, `discount_price`) VALUES
(2, 'jackson bachmen', '100001', 'what ever', 3, 3, 200, 200, '#b22e2e', 0, 'this', 0),
(6, 'Brown Lawn', '221312', NULL, 1, 1, 400, 300, '#b21f1f', 1, '2% OFF', 0),
(15, 'Sweater', '', NULL, 1, 1, 400, 300, '#f5efef', 1, '350', NULL),
(17, 'Sneakers', 'xnytlIECTp2jbfUz', NULL, 1, 1, 400, 300, '#e77474', 1, '350', NULL),
(18, 'Chair', '03112020193438', NULL, 3, 1, 400, 300, '#922020', 0, '12 %', NULL),
(27, 'new product', '04112020062057', NULL, 2, 3, 200, 200, '#000000', 0, '23', 23);

-- --------------------------------------------------------

--
-- Table structure for table `product_img`
--

CREATE TABLE `product_img` (
  `img_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `img_url` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_img`
--

INSERT INTO `product_img` (`img_id`, `product_id`, `img_url`) VALUES
(25, 4, ''),
(30, 9, 'chair.jpg'),
(31, 9, 'chair.png'),
(32, 9, 'chair2.jpg'),
(33, 10, '4.jpg'),
(34, 10, '5.jpg'),
(35, 10, '6.jpg'),
(36, 13, '4.jpg'),
(37, 13, '5.jpg'),
(38, 14, '4.jpg'),
(39, 14, '5.jpg'),
(40, 15, '4.jpg'),
(41, 15, '5.jpg'),
(42, 16, '4.jpg'),
(43, 16, '5.jpg'),
(44, 17, 'chair.png'),
(45, 17, 'chair2.png'),
(46, 18, '4.jpg'),
(47, 18, 'chair.jpg'),
(48, 19, '3.png'),
(49, 19, '4.jpg'),
(50, 19, '5.jpg'),
(51, 20, '3.png'),
(52, 20, '4.jpg'),
(53, 20, '5.jpg'),
(54, 21, '3.png'),
(55, 21, '4.jpg'),
(56, 21, '5.jpg'),
(57, 22, '3.png'),
(58, 22, '4.jpg'),
(59, 22, '5.jpg'),
(60, 23, '3.png'),
(61, 23, '4.jpg'),
(62, 23, '5.jpg'),
(63, 24, '3.png'),
(64, 24, '4.jpg'),
(65, 24, '5.jpg'),
(66, 25, '3.png'),
(67, 25, '4.jpg'),
(68, 25, '5.jpg'),
(69, 26, '3.png'),
(70, 26, '4.jpg'),
(71, 26, '5.jpg'),
(96, 5, '1.png'),
(97, 5, '1-old.jpg'),
(98, 5, '2.jpg'),
(99, 5, '3.png'),
(100, 27, 'photo4.jpg'),
(101, 27, 'photo5.jpg'),
(108, 6, 'photo4.jpg'),
(109, 6, 'photo3.jpg'),
(111, 2, '1.png');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `pur_id` int(11) NOT NULL,
  `pur_vendor_id` int(11) DEFAULT 0,
  `pur_date` date DEFAULT NULL,
  `pur_total` double DEFAULT NULL,
  `pur_created_by` int(11) DEFAULT 0,
  `pur_created_on` datetime DEFAULT NULL,
  `pur_remarks` longtext DEFAULT NULL,
  `pur_attachment` varchar(200) DEFAULT NULL,
  `is_stock` int(11) DEFAULT 0,
  `is_cancel` int(11) DEFAULT 0,
  `cancel_by` int(11) DEFAULT NULL,
  `cancel_on` datetime DEFAULT NULL,
  `cancel_reason` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`pur_id`, `pur_vendor_id`, `pur_date`, `pur_total`, `pur_created_by`, `pur_created_on`, `pur_remarks`, `pur_attachment`, `is_stock`, `is_cancel`, `cancel_by`, `cancel_on`, `cancel_reason`) VALUES
(1, 18, '2020-11-03', 300, 2, '2020-11-03 22:48:02', 'these are the remarks', NULL, 0, 0, NULL, NULL, NULL),
(2, 19, '2020-11-03', 500, 2, '2020-11-03 22:49:02', 'these are the remarksadf', NULL, 0, 0, NULL, NULL, NULL),
(3, 20, '2020-11-03', 500, 2, '2020-11-03 22:49:15', 'these are the remarksadf', NULL, 0, 0, NULL, NULL, NULL),
(4, 21, '2020-11-03', 300, 2, '2020-11-03 22:50:58', 'asdf', NULL, 0, 1, NULL, NULL, NULL),
(5, 22, '2020-11-03', 300, 2, '2020-11-03 22:51:09', 'asdf', 'banner-img.png', 0, 0, NULL, NULL, NULL),
(6, 23, '2020-11-03', 7500, 2, '2020-11-03 22:58:57', 'asdf', 'banner-img.png', 0, 0, NULL, NULL, NULL),
(7, 14, '2020-11-04', 200, 2, '2020-11-04 06:16:13', '', '', 0, 0, NULL, NULL, NULL),
(8, 14, '2020-11-04', 200, 2, '2020-11-04 06:19:06', '', '', 1, 0, NULL, NULL, NULL),
(9, 27, '2020-11-05', 36900, 4, '2020-11-05 14:44:30', '', '', 1, 0, NULL, NULL, NULL),
(10, 31, '2020-11-10', 6000, 4, '2020-11-10 09:26:45', '', '', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_items`
--

CREATE TABLE `purchase_items` (
  `puritem_id` int(11) NOT NULL,
  `puritem_itemid` int(11) DEFAULT 0,
  `puritem_price` double DEFAULT NULL,
  `puritem_qty` int(11) DEFAULT 0,
  `puritem_total` double DEFAULT NULL,
  `puritem_purid` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_items`
--

INSERT INTO `purchase_items` (`puritem_id`, `puritem_itemid`, `puritem_price`, `puritem_qty`, `puritem_total`, `puritem_purid`) VALUES
(1, 300, 300, 200, NULL, 0),
(2, 300, 300, 200, NULL, 0),
(3, 2, 200, 200, 200, 7),
(4, 2, 200, 200, 200, 8),
(5, 6, 300, 200, 36900, 9),
(6, 27, 200, 30, 6000, 10);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_payment`
--

CREATE TABLE `purchase_payment` (
  `pur_payid` int(11) NOT NULL,
  `pur_purchase_id` int(11) DEFAULT NULL,
  `purpay_amount` double DEFAULT NULL,
  `payment_by` int(11) DEFAULT NULL,
  `payment_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_payment`
--

INSERT INTO `purchase_payment` (`pur_payid`, `pur_purchase_id`, `purpay_amount`, `payment_by`, `payment_on`) VALUES
(1, 8, 10, 2, '2020-11-04 06:19:07'),
(2, 8, 90, 2, '2020-11-04 06:19:45'),
(3, 9, 36900, 4, '2020-11-05 14:44:34'),
(4, 8, 100, 4, '2020-11-06 05:11:57'),
(5, 10, 6000, 4, '2020-11-10 09:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE `sale` (
  `sale_id` int(11) NOT NULL,
  `sale_cust_id` int(11) DEFAULT 0,
  `sale_date` date DEFAULT NULL,
  `sale_total` double DEFAULT NULL,
  `sale_created_by` int(11) DEFAULT 0,
  `sale_created_on` datetime DEFAULT NULL,
  `sale_remarks` longtext DEFAULT NULL,
  `is_cancel` int(11) DEFAULT 0,
  `cancel_by` int(11) DEFAULT 0,
  `cancel_on` datetime DEFAULT NULL,
  `cancel_reason` longtext DEFAULT NULL,
  `sales_discount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`sale_id`, `sale_cust_id`, `sale_date`, `sale_total`, `sale_created_by`, `sale_created_on`, `sale_remarks`, `is_cancel`, `cancel_by`, `cancel_on`, `cancel_reason`, `sales_discount`) VALUES
(1000034, 100345, '2020-11-10', 400, 4, '2020-11-10 17:47:01', '', 0, 0, NULL, NULL, 0),
(1000035, 100346, '2020-11-10', 400, 4, '2020-11-10 17:48:27', '', 0, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE `sale_items` (
  `salitem_id` int(11) NOT NULL,
  `salitem_itemid` int(11) DEFAULT NULL,
  `salitem_price` double DEFAULT NULL,
  `salitem_qty` int(11) DEFAULT NULL,
  `salitem_total` double DEFAULT NULL,
  `salitem_saleid` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sale_items`
--

INSERT INTO `sale_items` (`salitem_id`, `salitem_itemid`, `salitem_price`, `salitem_qty`, `salitem_total`, `salitem_saleid`) VALUES
(1, 300, 300, 50, 300, 0),
(2, 200, 200, 50, 200, 0),
(3, 5, 200, 50, 2400, 0),
(4, 6, 300, 50, 3600, 0),
(5, 2, 200, 50, 2400, 3),
(6, 6, 300, 50, 3600, 3),
(7, 6, 300, 50, 3900, 5),
(8, 17, 300, 50, 3900, 5),
(9, 18, 400, 50, 4800, 6),
(10, 15, 400, 50, 4800, 6),
(11, 2, 200, 50, 200, 7),
(12, 6, 400, 50, 400, 8),
(13, 6, 400, 150, 400, 10),
(14, 6, 400, 50, 400, 11),
(15, 2, 200, 50, 200, 14),
(16, 2, 200, 50, 2400, 15),
(17, 27, 200, 12, 2400, 16),
(18, 2, 200, 1, 200, 17),
(19, 18, 400, 1, 400, 18),
(20, 18, 400, 1, 400, 19),
(21, 18, 400, 1, 400, 1000035);

-- --------------------------------------------------------

--
-- Table structure for table `sale_payment`
--

CREATE TABLE `sale_payment` (
  `salpay_id` int(11) NOT NULL,
  `sal_sale_id` int(11) DEFAULT 0,
  `salpay_amount` double DEFAULT NULL,
  `salpay_on` datetime DEFAULT NULL,
  `salpay_by` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sale_payment`
--

INSERT INTO `sale_payment` (`salpay_id`, `sal_sale_id`, `salpay_amount`, `salpay_on`, `salpay_by`) VALUES
(1, 5, 7800, '2020-11-03 23:54:08', 2),
(2, 5, 123, '2020-11-04 00:56:16', NULL),
(3, 5, 2477, '2020-11-04 00:56:45', NULL),
(4, 6, 9467, '2020-11-04 18:51:52', 4),
(5, 6, 9467, '2020-11-04 18:52:13', NULL),
(6, 7, 100, '2020-11-04 19:06:28', 4),
(7, 7, 100, '2020-11-04 19:06:45', NULL),
(8, 8, 400, '2020-11-06 05:11:05', NULL),
(9, 10, 400, '2020-11-09 10:29:17', NULL),
(10, 11, 400, '2020-11-09 10:38:52', 4),
(11, 12, 400, '2020-11-09 10:39:30', 4),
(12, 14, 200, '2020-11-09 11:07:55', NULL),
(13, 15, 2400, '2020-11-10 09:17:37', NULL),
(14, 16, 2400, '2020-11-10 09:25:09', NULL),
(15, 17, 200, '2020-11-10 16:51:30', 4),
(16, 18, 400, '2020-11-10 16:53:26', 4),
(17, 19, 400, '2020-11-10 17:47:01', 4),
(18, 1000035, 400, '2020-11-10 17:48:27', 4);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `stock_id` int(11) NOT NULL,
  `stock_voucheritemline_id` int(11) DEFAULT 0,
  `stock_itemid` int(11) NOT NULL DEFAULT 0,
  `stock_price` double DEFAULT NULL,
  `stock_condition` varchar(1) DEFAULT NULL,
  `stock_entry_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stock_id`, `stock_voucheritemline_id`, `stock_itemid`, `stock_price`, `stock_condition`, `stock_entry_date`) VALUES
(1, 1, 300, 300, '-', '2020-11-03'),
(2, 2, 200, 200, '-', '2020-11-03'),
(3, 3, 2, 200, '-', '2020-11-03'),
(4, 3, 2, 200, '-', '2020-11-03'),
(5, 3, 2, 200, '-', '2020-11-03'),
(6, 3, 2, 200, '-', '2020-11-03'),
(7, 3, 2, 200, '-', '2020-11-03'),
(8, 3, 2, 200, '-', '2020-11-03'),
(9, 3, 2, 200, '-', '2020-11-03'),
(10, 3, 2, 200, '-', '2020-11-03'),
(11, 3, 2, 200, '-', '2020-11-03'),
(12, 3, 2, 200, '-', '2020-11-03'),
(13, 3, 2, 200, '-', '2020-11-03'),
(14, 3, 2, 200, '-', '2020-11-03'),
(15, 4, 6, 300, '-', '2020-11-03'),
(16, 4, 6, 300, '-', '2020-11-03'),
(17, 4, 6, 300, '-', '2020-11-03'),
(18, 4, 6, 300, '-', '2020-11-03'),
(19, 4, 6, 300, '-', '2020-11-03'),
(20, 4, 6, 300, '-', '2020-11-03'),
(21, 4, 6, 300, '-', '2020-11-03'),
(22, 4, 6, 300, '-', '2020-11-03'),
(23, 4, 6, 300, '-', '2020-11-03'),
(24, 4, 6, 300, '-', '2020-11-03'),
(25, 4, 6, 300, '-', '2020-11-03'),
(26, 4, 6, 300, '-', '2020-11-03'),
(27, 5, 2, 200, '-', '2020-11-03'),
(28, 5, 2, 200, '-', '2020-11-03'),
(29, 5, 2, 200, '-', '2020-11-03'),
(30, 5, 2, 200, '-', '2020-11-03'),
(31, 5, 2, 200, '-', '2020-11-03'),
(32, 5, 2, 200, '-', '2020-11-03'),
(33, 5, 2, 200, '-', '2020-11-03'),
(34, 5, 2, 200, '-', '2020-11-03'),
(35, 5, 2, 200, '-', '2020-11-03'),
(36, 5, 2, 200, '-', '2020-11-03'),
(37, 5, 2, 200, '-', '2020-11-03'),
(38, 5, 2, 200, '-', '2020-11-03'),
(39, 6, 6, 300, '-', '2020-11-03'),
(40, 6, 6, 300, '-', '2020-11-03'),
(41, 6, 6, 300, '-', '2020-11-03'),
(42, 6, 6, 300, '-', '2020-11-03'),
(43, 6, 6, 300, '-', '2020-11-03'),
(44, 6, 6, 300, '-', '2020-11-03'),
(45, 6, 6, 300, '-', '2020-11-03'),
(46, 6, 6, 300, '-', '2020-11-03'),
(47, 6, 6, 300, '-', '2020-11-03'),
(48, 6, 6, 300, '-', '2020-11-03'),
(49, 6, 6, 300, '-', '2020-11-03'),
(50, 6, 6, 300, '-', '2020-11-03'),
(51, 7, 6, 300, '-', '2020-11-03'),
(52, 7, 6, 300, '-', '2020-11-03'),
(53, 7, 6, 300, '-', '2020-11-03'),
(54, 7, 6, 300, '-', '2020-11-03'),
(55, 7, 6, 300, '-', '2020-11-03'),
(56, 7, 6, 300, '-', '2020-11-03'),
(57, 7, 6, 300, '-', '2020-11-03'),
(58, 7, 6, 300, '-', '2020-11-03'),
(59, 7, 6, 300, '-', '2020-11-03'),
(60, 7, 6, 300, '-', '2020-11-03'),
(61, 7, 6, 300, '-', '2020-11-03'),
(62, 7, 6, 300, '-', '2020-11-03'),
(63, 7, 6, 300, '-', '2020-11-03'),
(64, 8, 17, 300, '-', '2020-11-03'),
(65, 8, 17, 300, '-', '2020-11-03'),
(66, 8, 17, 300, '-', '2020-11-03'),
(67, 8, 17, 300, '-', '2020-11-03'),
(68, 8, 17, 300, '-', '2020-11-03'),
(69, 8, 17, 300, '-', '2020-11-03'),
(70, 8, 17, 300, '-', '2020-11-03'),
(71, 8, 17, 300, '-', '2020-11-03'),
(72, 8, 17, 300, '-', '2020-11-03'),
(73, 8, 17, 300, '-', '2020-11-03'),
(74, 8, 17, 300, '-', '2020-11-03'),
(75, 8, 17, 300, '-', '2020-11-03'),
(76, 8, 17, 300, '-', '2020-11-03'),
(77, 4, 2, 200, '+', '2020-11-04'),
(78, 9, 18, 400, '-', '2020-11-04'),
(79, 9, 18, 400, '-', '2020-11-04'),
(80, 9, 18, 400, '-', '2020-11-04'),
(81, 9, 18, 400, '-', '2020-11-04'),
(82, 9, 18, 400, '-', '2020-11-04'),
(83, 9, 18, 400, '-', '2020-11-04'),
(84, 9, 18, 400, '-', '2020-11-04'),
(85, 9, 18, 400, '-', '2020-11-04'),
(86, 9, 18, 400, '-', '2020-11-04'),
(87, 9, 18, 400, '-', '2020-11-04'),
(88, 9, 18, 400, '-', '2020-11-04'),
(89, 9, 18, 400, '-', '2020-11-04'),
(90, 10, 15, 400, '-', '2020-11-04'),
(91, 10, 15, 400, '-', '2020-11-04'),
(92, 10, 15, 400, '-', '2020-11-04'),
(93, 10, 15, 400, '-', '2020-11-04'),
(94, 10, 15, 400, '-', '2020-11-04'),
(95, 10, 15, 400, '-', '2020-11-04'),
(96, 10, 15, 400, '-', '2020-11-04'),
(97, 10, 15, 400, '-', '2020-11-04'),
(98, 10, 15, 400, '-', '2020-11-04'),
(99, 10, 15, 400, '-', '2020-11-04'),
(100, 10, 15, 400, '-', '2020-11-04'),
(101, 10, 15, 400, '-', '2020-11-04'),
(102, 11, 2, 200, '-', '2020-11-04'),
(103, 5, 6, 300, '+', '2020-11-05'),
(104, 5, 6, 300, '+', '2020-11-05'),
(105, 5, 6, 300, '+', '2020-11-05'),
(106, 5, 6, 300, '+', '2020-11-05'),
(107, 5, 6, 300, '+', '2020-11-05'),
(108, 5, 6, 300, '+', '2020-11-05'),
(109, 5, 6, 300, '+', '2020-11-05'),
(110, 5, 6, 300, '+', '2020-11-05'),
(111, 5, 6, 300, '+', '2020-11-05'),
(112, 5, 6, 300, '+', '2020-11-05'),
(113, 5, 6, 300, '+', '2020-11-05'),
(114, 5, 6, 300, '+', '2020-11-05'),
(115, 5, 6, 300, '+', '2020-11-05'),
(116, 5, 6, 300, '+', '2020-11-05'),
(117, 5, 6, 300, '+', '2020-11-05'),
(118, 5, 6, 300, '+', '2020-11-05'),
(119, 5, 6, 300, '+', '2020-11-05'),
(120, 5, 6, 300, '+', '2020-11-05'),
(121, 5, 6, 300, '+', '2020-11-05'),
(122, 5, 6, 300, '+', '2020-11-05'),
(123, 5, 6, 300, '+', '2020-11-05'),
(124, 5, 6, 300, '+', '2020-11-05'),
(125, 5, 6, 300, '+', '2020-11-05'),
(126, 5, 6, 300, '+', '2020-11-05'),
(127, 5, 6, 300, '+', '2020-11-05'),
(128, 5, 6, 300, '+', '2020-11-05'),
(129, 5, 6, 300, '+', '2020-11-05'),
(130, 5, 6, 300, '+', '2020-11-05'),
(131, 5, 6, 300, '+', '2020-11-05'),
(132, 5, 6, 300, '+', '2020-11-05'),
(133, 5, 6, 300, '+', '2020-11-05'),
(134, 5, 6, 300, '+', '2020-11-05'),
(135, 5, 6, 300, '+', '2020-11-05'),
(136, 5, 6, 300, '+', '2020-11-05'),
(137, 5, 6, 300, '+', '2020-11-05'),
(138, 5, 6, 300, '+', '2020-11-05'),
(139, 5, 6, 300, '+', '2020-11-05'),
(140, 5, 6, 300, '+', '2020-11-05'),
(141, 5, 6, 300, '+', '2020-11-05'),
(142, 5, 6, 300, '+', '2020-11-05'),
(143, 5, 6, 300, '+', '2020-11-05'),
(144, 5, 6, 300, '+', '2020-11-05'),
(145, 5, 6, 300, '+', '2020-11-05'),
(146, 5, 6, 300, '+', '2020-11-05'),
(147, 5, 6, 300, '+', '2020-11-05'),
(148, 5, 6, 300, '+', '2020-11-05'),
(149, 5, 6, 300, '+', '2020-11-05'),
(150, 5, 6, 300, '+', '2020-11-05'),
(151, 5, 6, 300, '+', '2020-11-05'),
(152, 5, 6, 300, '+', '2020-11-05'),
(153, 5, 6, 300, '+', '2020-11-05'),
(154, 5, 6, 300, '+', '2020-11-05'),
(155, 5, 6, 300, '+', '2020-11-05'),
(156, 5, 6, 300, '+', '2020-11-05'),
(157, 5, 6, 300, '+', '2020-11-05'),
(158, 5, 6, 300, '+', '2020-11-05'),
(159, 5, 6, 300, '+', '2020-11-05'),
(160, 5, 6, 300, '+', '2020-11-05'),
(161, 5, 6, 300, '+', '2020-11-05'),
(162, 5, 6, 300, '+', '2020-11-05'),
(163, 5, 6, 300, '+', '2020-11-05'),
(164, 5, 6, 300, '+', '2020-11-05'),
(165, 5, 6, 300, '+', '2020-11-05'),
(166, 5, 6, 300, '+', '2020-11-05'),
(167, 5, 6, 300, '+', '2020-11-05'),
(168, 5, 6, 300, '+', '2020-11-05'),
(169, 5, 6, 300, '+', '2020-11-05'),
(170, 5, 6, 300, '+', '2020-11-05'),
(171, 5, 6, 300, '+', '2020-11-05'),
(172, 5, 6, 300, '+', '2020-11-05'),
(173, 5, 6, 300, '+', '2020-11-05'),
(174, 5, 6, 300, '+', '2020-11-05'),
(175, 5, 6, 300, '+', '2020-11-05'),
(176, 5, 6, 300, '+', '2020-11-05'),
(177, 5, 6, 300, '+', '2020-11-05'),
(178, 5, 6, 300, '+', '2020-11-05'),
(179, 5, 6, 300, '+', '2020-11-05'),
(180, 5, 6, 300, '+', '2020-11-05'),
(181, 5, 6, 300, '+', '2020-11-05'),
(182, 5, 6, 300, '+', '2020-11-05'),
(183, 5, 6, 300, '+', '2020-11-05'),
(184, 5, 6, 300, '+', '2020-11-05'),
(185, 5, 6, 300, '+', '2020-11-05'),
(186, 5, 6, 300, '+', '2020-11-05'),
(187, 5, 6, 300, '+', '2020-11-05'),
(188, 5, 6, 300, '+', '2020-11-05'),
(189, 5, 6, 300, '+', '2020-11-05'),
(190, 5, 6, 300, '+', '2020-11-05'),
(191, 5, 6, 300, '+', '2020-11-05'),
(192, 5, 6, 300, '+', '2020-11-05'),
(193, 5, 6, 300, '+', '2020-11-05'),
(194, 5, 6, 300, '+', '2020-11-05'),
(195, 5, 6, 300, '+', '2020-11-05'),
(196, 5, 6, 300, '+', '2020-11-05'),
(197, 5, 6, 300, '+', '2020-11-05'),
(198, 5, 6, 300, '+', '2020-11-05'),
(199, 5, 6, 300, '+', '2020-11-05'),
(200, 5, 6, 300, '+', '2020-11-05'),
(201, 5, 6, 300, '+', '2020-11-05'),
(202, 5, 6, 300, '+', '2020-11-05'),
(203, 5, 6, 300, '+', '2020-11-05'),
(204, 5, 6, 300, '+', '2020-11-05'),
(205, 5, 6, 300, '+', '2020-11-05'),
(206, 5, 6, 300, '+', '2020-11-05'),
(207, 5, 6, 300, '+', '2020-11-05'),
(208, 5, 6, 300, '+', '2020-11-05'),
(209, 5, 6, 300, '+', '2020-11-05'),
(210, 5, 6, 300, '+', '2020-11-05'),
(211, 5, 6, 300, '+', '2020-11-05'),
(212, 5, 6, 300, '+', '2020-11-05'),
(213, 5, 6, 300, '+', '2020-11-05'),
(214, 5, 6, 300, '+', '2020-11-05'),
(215, 5, 6, 300, '+', '2020-11-05'),
(216, 5, 6, 300, '+', '2020-11-05'),
(217, 5, 6, 300, '+', '2020-11-05'),
(218, 5, 6, 300, '+', '2020-11-05'),
(219, 5, 6, 300, '+', '2020-11-05'),
(220, 5, 6, 300, '+', '2020-11-05'),
(221, 5, 6, 300, '+', '2020-11-05'),
(222, 5, 6, 300, '+', '2020-11-05'),
(223, 5, 6, 300, '+', '2020-11-05'),
(224, 5, 6, 300, '+', '2020-11-05'),
(225, 5, 6, 300, '+', '2020-11-05'),
(226, 12, 6, 400, '-', '2020-11-06'),
(227, 13, 6, 400, '-', '2020-11-09'),
(228, 14, 6, 400, '-', '2020-11-09'),
(229, 15, 2, 200, '-', '2020-11-09'),
(230, 16, 2, 200, '-', '2020-11-10'),
(231, 16, 2, 200, '-', '2020-11-10'),
(232, 16, 2, 200, '-', '2020-11-10'),
(233, 16, 2, 200, '-', '2020-11-10'),
(234, 16, 2, 200, '-', '2020-11-10'),
(235, 16, 2, 200, '-', '2020-11-10'),
(236, 16, 2, 200, '-', '2020-11-10'),
(237, 16, 2, 200, '-', '2020-11-10'),
(238, 16, 2, 200, '-', '2020-11-10'),
(239, 16, 2, 200, '-', '2020-11-10'),
(240, 16, 2, 200, '-', '2020-11-10'),
(241, 16, 2, 200, '-', '2020-11-10'),
(242, 17, 27, 200, '-', '2020-11-10'),
(243, 17, 27, 200, '-', '2020-11-10'),
(244, 17, 27, 200, '-', '2020-11-10'),
(245, 17, 27, 200, '-', '2020-11-10'),
(246, 17, 27, 200, '-', '2020-11-10'),
(247, 17, 27, 200, '-', '2020-11-10'),
(248, 17, 27, 200, '-', '2020-11-10'),
(249, 17, 27, 200, '-', '2020-11-10'),
(250, 17, 27, 200, '-', '2020-11-10'),
(251, 17, 27, 200, '-', '2020-11-10'),
(252, 17, 27, 200, '-', '2020-11-10'),
(253, 17, 27, 200, '-', '2020-11-10'),
(254, 6, 27, 200, '+', '2020-11-10'),
(255, 6, 27, 200, '+', '2020-11-10'),
(256, 6, 27, 200, '+', '2020-11-10'),
(257, 6, 27, 200, '+', '2020-11-10'),
(258, 6, 27, 200, '+', '2020-11-10'),
(259, 6, 27, 200, '+', '2020-11-10'),
(260, 6, 27, 200, '+', '2020-11-10'),
(261, 6, 27, 200, '+', '2020-11-10'),
(262, 6, 27, 200, '+', '2020-11-10'),
(263, 6, 27, 200, '+', '2020-11-10'),
(264, 6, 27, 200, '+', '2020-11-10'),
(265, 6, 27, 200, '+', '2020-11-10'),
(266, 6, 27, 200, '+', '2020-11-10'),
(267, 6, 27, 200, '+', '2020-11-10'),
(268, 6, 27, 200, '+', '2020-11-10'),
(269, 6, 27, 200, '+', '2020-11-10'),
(270, 6, 27, 200, '+', '2020-11-10'),
(271, 6, 27, 200, '+', '2020-11-10'),
(272, 6, 27, 200, '+', '2020-11-10'),
(273, 6, 27, 200, '+', '2020-11-10'),
(274, 6, 27, 200, '+', '2020-11-10'),
(275, 6, 27, 200, '+', '2020-11-10'),
(276, 6, 27, 200, '+', '2020-11-10'),
(277, 6, 27, 200, '+', '2020-11-10'),
(278, 6, 27, 200, '+', '2020-11-10'),
(279, 6, 27, 200, '+', '2020-11-10'),
(280, 6, 27, 200, '+', '2020-11-10'),
(281, 6, 27, 200, '+', '2020-11-10'),
(282, 6, 27, 200, '+', '2020-11-10'),
(283, 6, 27, 200, '+', '2020-11-10'),
(284, 18, 2, 200, '-', '2020-11-10'),
(285, 19, 18, 400, '-', '2020-11-10'),
(286, 20, 18, 400, '-', '2020-11-10'),
(287, 21, 18, 400, '-', '2020-11-10');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag_name`) VALUES
(1, 'Tag 1'),
(2, 'Tag 1'),
(3, 'Tag2'),
(4, 'Khadi lawn'),
(5, 'Khadi lawn'),
(6, 'Khadi lawn'),
(7, 'sweater'),
(8, 'sweater'),
(9, 'sweater'),
(10, 'sweater'),
(11, 'shoes'),
(12, 'chair'),
(13, 'new tag'),
(14, 'new tag'),
(15, 'new tag');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

CREATE TABLE `tbl_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(200) DEFAULT NULL,
  `type_shortdesc` varchar(200) DEFAULT NULL,
  `is_trash` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_type`
--

INSERT INTO `tbl_type` (`type_id`, `type_name`, `type_shortdesc`, `is_trash`) VALUES
(1, 'Stitched Male Dress', 'Stitched Male Dress', 0),
(2, 'Stitched Female Dress', 'Stitched Female Dress', 0),
(3, 'Unstitched Male Dress', 'Unstitched Male Dress', 0),
(4, 'Unstitched Female Dress', 'Unstitched Female Dress', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `uid` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT 0,
  `role` int(11) DEFAULT NULL,
  `cnic` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `contact` int(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `user_name` varchar(200) DEFAULT NULL,
  `u_password` varchar(200) DEFAULT NULL,
  `u_image` varchar(200) DEFAULT NULL,
  `is_trash` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`uid`, `name`, `user_type`, `role`, `cnic`, `email`, `contact`, `address`, `created_by`, `created_date`, `user_name`, `u_password`, `u_image`, `is_trash`) VALUES
(4, 'John Gram', 2, 1, '173013333', 'admin@pos.com', 2147483647, 'His House, that street, Nowhere.', NULL, NULL, 'admin@pos.com', '827ccb0eea8a706c4c34a16891f84e7b', 'photo8.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `short_desc` varchar(200) NOT NULL,
  `is_trash` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`role_id`, `role_name`, `short_desc`, `is_trash`) VALUES
(1, 'Sales Person', 'The role for Sales Person / Sales Man / Employ', 0),
(2, 'Admin', 'Admin to view all activities of software.', 0),
(3, 'Accountant', 'This is accountant to manage the accounting vouchers\r\n', 0),
(4, 'Developer', 'He is developer. He helps us improve our software.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendor_id` int(11) NOT NULL,
  `vendor_name` varchar(100) DEFAULT NULL,
  `phone_no` varchar(100) DEFAULT NULL,
  `fax_no` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `is_trash` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `phone_no`, `fax_no`, `email`, `company_id`, `is_trash`) VALUES
(1, 'Syed Asad Khan', '333', '0000', 'ABCD@YAHOO.COM', 0, 0),
(2, 'Anees', '333', '0000', 'ABCD@YAHOO.COM', 0, 0),
(3, 'Imran', '1233', '12133', 'admin@yahoo.com', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `core_settings`
--
ALTER TABLE `core_settings`
  ADD PRIMARY KEY (`c_set_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `layout_settings`
--
ALTER TABLE `layout_settings`
  ADD PRIMARY KEY (`layset_id`);

--
-- Indexes for table `logo_setting`
--
ALTER TABLE `logo_setting`
  ADD PRIMARY KEY (`logo_set_id`);

--
-- Indexes for table `prd_tag`
--
ALTER TABLE `prd_tag`
  ADD PRIMARY KEY (`prdt_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_img`
--
ALTER TABLE `product_img`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`pur_id`);

--
-- Indexes for table `purchase_items`
--
ALTER TABLE `purchase_items`
  ADD PRIMARY KEY (`puritem_id`);

--
-- Indexes for table `purchase_payment`
--
ALTER TABLE `purchase_payment`
  ADD PRIMARY KEY (`pur_payid`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`sale_id`);

--
-- Indexes for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD PRIMARY KEY (`salitem_id`);

--
-- Indexes for table `sale_payment`
--
ALTER TABLE `sale_payment`
  ADD PRIMARY KEY (`salpay_id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `tbl_type`
--
ALTER TABLE `tbl_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `core_settings`
--
ALTER TABLE `core_settings`
  MODIFY `c_set_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100347;

--
-- AUTO_INCREMENT for table `layout_settings`
--
ALTER TABLE `layout_settings`
  MODIFY `layset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logo_setting`
--
ALTER TABLE `logo_setting`
  MODIFY `logo_set_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prd_tag`
--
ALTER TABLE `prd_tag`
  MODIFY `prdt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `product_img`
--
ALTER TABLE `product_img`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `pur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `purchase_items`
--
ALTER TABLE `purchase_items`
  MODIFY `puritem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `purchase_payment`
--
ALTER TABLE `purchase_payment`
  MODIFY `pur_payid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `sale_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000036;

--
-- AUTO_INCREMENT for table `sale_items`
--
ALTER TABLE `sale_items`
  MODIFY `salitem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sale_payment`
--
ALTER TABLE `sale_payment`
  MODIFY `salpay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_type`
--
ALTER TABLE `tbl_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
