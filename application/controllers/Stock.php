<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * 	@author : Imran Shah
 *  @support: shahmian@gmail.com
 * 	date	: 18 April, 2018
 * 	Kandi Inventory Management System
 * website: kelextech.com
 *  version: 1.0
 */

class Stock extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user')) {
            redirect(base_url('index.php/Users/login'));
        }
    }

    public function stock()
    {
        $data = $this->db->join('category', 'category.category_id = products.category_id', 'left')
            ->join('tbl_type', 'tbl_type.type_id = products.type_id')
            ->where('products.is_trash', 0)
            ->where('products.instant_id', $this->session->userdata('user')['inst_id'])
            ->get('products')->result();
        $abc = [];
        if (!empty($data)) {
            foreach ($data as $products) {

                $abc[$products->product_id]['product'] = $products;
                $abc[$products->product_id]['pur'] = $pur = $this->db->where('stock_itemid', $products->product_id)
                    ->where('stock_condition', '+')->get('stock')->num_rows();
                $abc[$products->product_id]['sal'] = $sal = $this->db->where('stock_itemid', $products->product_id)
                    ->where('stock_condition', '-')->get('stock')->num_rows();
                $abc[$products->product_id]['stock'] = $pur - $sal;
                $abc[$products->product_id]['proimage'] = $this->db->where('product_id', $products->product_id)->get('product_img')->result();
            }
        }
        $result['pro'] = $abc;


        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('products/view_stock', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }
}
