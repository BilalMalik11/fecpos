 
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user')['user_id']) {
            redirect('Auth/login');
        }
    }

    public function insert_sale()
    {


        $customer = '';
        if (is_numeric($this->input->post('customerid'))) {
            $customer = $this->input->post('customerid');
        } else {
            if ($this->input->post('customerid') == '') {
                $venderdata = [
                    'email' => $this->input->post('email'),
                    'contact' => $this->input->post('contact'),
                    'address' => $this->input->post('Address'),
                    'instant_id' => $this->session->userdata('user')['inst_id'],

                    

                ];
                $customer = $this->Model_p->create('customers', $venderdata);
            }
        }

        $sale = [
            'sale_cust_id' => $customer,
            'sale_date' => $this->input->post('sales_date'),
            'sale_total' => $this->input->post('total'),
            'sale_created_by' => $this->input->post('created_by'),
            'sale_created_on' => date('Y-m-d H:i:s'),
            'sale_remarks' => $this->input->post('remarks'),
            'sales_discount' => $this->input->post('discount'),
            'instant_id' => $this->session->userdata('user')['inst_id'],

        ];

        $salitem_saleid = $this->Model_p->create('sale', $sale);

        if (!empty($this->input->post('paid'))) {

            $sale_pay = [
                'sal_sale_id' => $salitem_saleid,
                'salpay_amount' => $this->input->post('paid'),
                'salpay_by' => $this->session->userdata('user')['user_id'],
                'salpay_on' => date('Y-m-d H:i:s'),
                'instant_id' => $this->session->userdata('user')['inst_id'],

            ];

            $this->Model_p->create('sale_payment', $sale_pay);
        }

        $id = $this->input->post('p_id');
        $price = $this->input->post('product_price');
        $quatity = $this->input->post('product_quantity');
        $subtotal = $this->input->post('subtotal');

        foreach ($id as $key => $value) {
            $item = [
                'salitem_itemid' => $value,
                'salitem_price' => $price[$key],
                'salitem_qty' => $quatity[$key],
                'salitem_total' => $subtotal[$key],
                'salitem_saleid' => $salitem_saleid,
                'instant_id' => $this->session->userdata('user')['inst_id'],

            ];
            $item_idd = $this->Model_p->create('sale_items', $item);

            if (!empty($quatity[$key])) {
                for ($i = 0; $i < $quatity[$key]; $i++) {
                    $stoook = [
                        'stock_voucheritemline_id' => $item_idd,
                        'stock_itemid' => $value,
                        'stock_price' => $price[$key],
                        'stock_condition' => '-',
                        'stock_entry_date' => date('Y-m-d H:i:s'),
                    'instant_id' => $this->session->userdata('user')['inst_id'],

                    ];
                    $this->Model_p->create('stock', $stoook);
                }
            }
        }
        redirect('Sales/sale_print/' . $salitem_saleid);
    }

    public function single_sales($id)
    {
        $data['sale'] = $this->Model_p->get_globalSingWithCond('sale', ['sale_id' => $id]);
        $data['pro'] = $this->Model_p->get_globalMultiWithCond('products', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);

        $data['cat'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['proitems'] = $this->Model_p->getpaidproducts($id);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['customers'] = $this->Model_p->get_globalMultiWithCond('tbl_user', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['paid'] = $this->Model_p->getSalePayments($id);



        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('sales/single_sale');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }
    public function sale_print($id)
    {


                    
                    $data['sales'] = $this->Model_p->get_globalSingWithCond('sale', ['sale_id' => $id], ['instant_id' => $this->session->userdata('user')['inst_id']]);


        $data['purchase_vendor'] = $this->Model_p->get_globalSingWithCond('customers', ['customer_id' => $data['sales']->sale_cust_id],['instant_id' => $this->session->userdata('user')['inst_id']] );



        $data['cat'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['purchase_item'] = $this->Model_p->getpaidproducts($id);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['customers'] = $this->Model_p->get_globalMultiWithCond('tbl_user', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['purchase_payment'] = $this->Model_p->getSalePayments($id);
        $data['settings'] = $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);

        //  $this->load->view('include/head');
        //  $this->load->view('include/header');
        //$this->load->view('include/menusetting', $data);
            //$this->load->view('include/sidebar');
            $this->load->view('sales/sale_print2', $data);
        //$this->load->view('include/foot');
        //$this->load->view('include/footer');
    }

    public function update_payment()
    {
        if (!empty($this->input->post('amount'))) {

            $sale_pay = [
                'sal_sale_id' => $this->input->post('id'),
                'salpay_amount' => $this->input->post('amount'),
                'salpay_by' => $this->input->post('created_by'),
                'salpay_on' => date('Y-m-d H:i:s'),
            ];

            $this->Model_p->create('sale_payment', $sale_pay);
        }
        redirect('Sales/single_sales/' . $this->input->post('id'));
    }

    public function sales_voucer()
    {
        $data['sale'] = $this->Model_p->getAllsales();

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('sales/sales_vouchers');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function update_sale()
    {
        $id =  $this->input->post('sales_return');
        $data =  $this->db->where('salitem_saleid', $id)->get('sale_items')->row();
        $stockid =    $data->salitem_id;

        $this->db->delete('sale', ['sale_id' => $id]);
        $this->db->delete('sale_payment', ['sal_sale_id' => $id]);
        $this->db->delete('sale_items', ['salitem_saleid' => $id]);
        $this->db->delete('stock', ['stock_voucheritemline_id' => $stockid]);

        redirect('sales/sales_voucer');
    }
}
