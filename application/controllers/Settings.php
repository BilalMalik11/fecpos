<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('user')['user_id']) {
            redirect('Auth/login');
        }
    }

    public function logo_settings() {

        $result['da'] = $this->db->where('instant_id', $this->session->userdata('user')['inst_id'])->get('logo_setting');
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('settings/logo_setting', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function update_logo_set() {
        $new_name = $this->input->post('hiddenimg');
        if ($_FILES['file']['name'] != '') {
            $new_name = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/logo');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new_name
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new_name;
            } else {
                $file = $this->upload->display_errors();
            }
        }
 


        $id['da'] = $this->Model_p->chek_global('logo_setting');
        $up = $id['da']->logo_set_id;

        if ($id) {

            $data = array(
                'app_name' => $this->input->post('app_name'),
                'logo_set_email' => $this->input->post('logo_set_email'),
                'logo_set_contact' => $this->input->post('logo_set_contact'),
                'logo_set_align' => $this->input->post('logo_set_align'),
                'logo_set_address' => $this->input->post('logo_set_address'),
                'logo_set_align' => $this->input->post('logo_set_align'),
                'logo_set_logo' => $new_name,

            );
            $this->Model_p->updateRecord('logo_setting', ['logo_set_id', $up], $data);
        } else {
            $data = array(
                'app_name' => $this->input->post('app_name'),
                'logo_set_email' => $this->input->post('logo_set_email'),
                'logo_set_contact' => $this->input->post('logo_set_contact'),
                'logo_set_align' => $this->input->post('logo_set_align'),
                'logo_set_address' => $this->input->post('logo_set_address'),
                'logo_set_logo' => $new_name,
                'instant_id' => $this->session->userdata('user')['inst_id']
            );

            $this->Model_p->create('logo_setting', $data);
        }

        redirect('settings/logo_settings');
    }

}
