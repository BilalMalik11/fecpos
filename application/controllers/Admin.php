
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('user')['user_id']) {

            redirect('Auth/login');
        }
    }

    public function layout_settings() {
        $header = $this->input->post('header');
        $sidebar = $this->input->post('sidebar');
        $menu = $this->input->post('menu');
        $submenu = $this->input->post('submenu');
        $data = array(
            'submenu_icon' => $submenu,
            'sidebar_icon' => $menu,
            'header' => $header,
            'sidebar' => $sidebar,
        );
        $this->Model_p->updateRecord('layout_settings', ['layset_id' => 1], $data);
    }

    public function add_user() {
        $result['data'] = $this->Model_p->get_globalMultiWithCond('user_roles', ['is_trash' => 0],['instant_id'=>$this->session->userdata('user')['inst_id']]);
        $result['com'] = $this->Model_p->get_globalMultiWithCond('company', ['is_trash' => 0], ['instant_id'=>$this->session->userdata('user')['inst_id']]);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/add_user', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function add_new_user() {
         $new_name = '';

        if ($_FILES['file']['name'] != '') {
            $new_name = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new_name
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new_name;
            } else {
                $file = $this->upload->display_errors();
            }
        }

        $data = array(
            'name' => $this->input->post('nam'),
            'role' => $this->input->post('role'),
            'salary' => $this->input->post('salary'),
            
            'cnic' => $this->input->post('cni'),
            'email' => $this->input->post('emal'),
            'user_name' => $this->input->post('emal'),
            'contact' => $this->input->post('contct'),
            'address' => $this->input->post('adres'),
            'u_image' => $new_name,
            'u_password' => md5($this->input->post('pass')),
            'instant_id' => $this->session->userdata('user')['user_id']
        );

        $this->Model_p->create('tbl_user', $data);
        
            redirect('admin/administrators');
       
    }

    public function vendors() {
        $result['data'] = $this->Model_p->getVendors(); 
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/all_users', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function customers() {
        $result['data'] = $this->Model_p->get_globalMultiWithCond('customers', ['is_trash' => 0]);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/all_users', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function administrators() {
        $result['data'] = $this->Model_p->get_globalMultiWithCond('tbl_user', ['is_trash' => 0], ['instant_id'=>$this->session->userdata('user')['inst_id']]);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/all_users', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function single_user() {
        $result['data'] = $this->Model_p->get_globalSingWithCond('tbl_user', ['uid' => $this->session->userdata('user')['user_id']]);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/single_user', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
        $this->load->view('modals/new_category');
    }

    public function update_single_user() {
        $id = $this->input->post('hid');
         $new_name = '';

        if ($_FILES['file']['name'] != '') {
            $new_name = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new_name
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new_name;
            } else {
                $file = $this->upload->display_errors();
            }
        }
        if(!empty( $new_name)){
             $new_name;

}
else{
       $new_name =  $this->input->post('hiddenimg');
}
 
        $data = array(
            'name' => $this->input->post('nam'),
            'cnic' => $this->input->post('cni'),
            'email' => $this->input->post('emal'),
            'user_name' => $this->input->post('emal'),
            'contact' => $this->input->post('contct'),
            'address' => $this->input->post('adres'),
            'u_image' => $new_name,
        );
        $this->Model_p->updateRecord('tbl_user', ['uid' => $id], $data);
        redirect('Admin/single_user');
    }

    public function logout() {
        $this->session->unset_userdata('user');
        redirect('auth/login');
    }

    public function roles() {



        $result['data'] = $this->db->where('is_trash' , 0)->get('user_roles')->result();

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/roles', $result);
        $this->load->view('include/foot');
        $this->load->view('modals/new_role');
        $this->load->view('include/footer');
    }

    public function new_role() {

        $data = [
            'role_name' => $this->input->post('title'),
            'short_desc' => $this->input->post('desc'),
        ];

        $this->Model_p->create('user_roles', $data);
        redirect('Admin/roles');
    }

    public function update_role() {
        $id = $this->input->post('hid');
        $data = [
            'role_name' => $this->input->post('title'),
            'short_desc' => $this->input->post('desc'),
        ];

        $this->Model_p->updateRecord('user_roles', ['role_id' => $id], $data);
        redirect('Admin/roles');
    }

    public function delete_Role($id) {

        $data = ['is_trash' => 1];

        $this->Model_p->updateRecord('user_roles', ['role_id' => $id], $data);
        redirect('Admin/roles');
    }
      public function delete_user($id) {

        $data = ['is_trash' => 1];

        $this->Model_p->updateRecord('tbl_user', ['uid' => $id], $data);
        redirect('Admin/administrators');
    }

    public function update_user($id) {

        $result['data'] = $this->Model_p->get_globalMultiWithCond('user_roles', ['is_trash' => 0]);
        $result['user'] = $this->Model_p->get_globalSingWithCond('tbl_user', ['uid' => $id]);


        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar', $result);
        $this->load->view('operation/update_user');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function update__user() {
        $id = $this->input->post('hid');
        if ($_FILES['file']['name'] != '') {
            $new_name = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new_name
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new_name;
            } else {
                $file = $this->upload->display_errors();
            }
        } else {
            $new_name = $this->input->post("hidenimg");
        }


        $data = array(
            'name' => $this->input->post('nam'),
            'role' => $this->input->post('role'),
            
            'cnic' => $this->input->post('cni'),
            'email' => $this->input->post('emal'),
            'user_name' => $this->input->post('emal'),
            'contact' => $this->input->post('contct'),
            'address' => $this->input->post('adres'),
            'u_image' => $new_name,
        );
        $this->Model_p->updateRecord('tbl_user', ['uid' => $id], $data);
        redirect('Admin/update_user/' . $id);
    }

}
