
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user')['user_id']) {
            redirect('Auth/login');
        }
    }

    public function products()
    {
        $data['Pro'] = $this->Model_p->getproducts();
        $data['cate'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0],  ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0],  ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/all_products', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }
    public function sales_report()
    {
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('reports/sale_report');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function stock()
    {
        $data['Pro'] = $this->Model_p->getproducts();

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('products/view_stock', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function add_product()
    {

        $data['cate'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['tags'] = $this->Model_p->get_global('tags');



        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/new_product', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function single_product($id)
    {
        $data['pro'] = $this->Model_p->getsingleproduct($id);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('products/single_product', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function edit_product($id)
    {

        $data['Pro'] = $this->Model_p->getsingleproduct($id);
        $data['cate'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['prd_tags'] = $this->Model_p->get_globalMultiWithCond('prd_tag', ['prd_id' => $id], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['tags'] = $this->Model_p->get_global('tags');

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/update_product', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function prd_settings()
    {
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/prd_settings');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function Invoices()
    {

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/invoice');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function reports()
    {

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/reports');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function categories()
    {


        $result['data'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/categories', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
        $this->load->view('modals/new_category');
    }

    public function sales()
    {

        $data['cat'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['pro'] = $this->Model_p->get_globalMultiWithCond('products', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['customers'] = $this->Model_p->get_globalMultiWithCond('customers ', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['number'] = $this->db->query('SELECT * FROM sale ORDER BY sale_id DESC LIMIT 1')->row();
        $data['saleno'] = 0000001;
        if (!empty($data['number'])) {
            $data['saleno'] =  $data['number']->sale_id + 1;
        }

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('sales/sales');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function sales_products()
    {
        // $data['cat'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0]);
        // $data['pro'] = $this->Model_p->get_globalMultiWithCond('products', ['is_trash' => 0]);
        // $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0]);
        $data['customers'] = $this->Model_p->get_globalMultiWithCond('customers ', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['number'] = $this->db->query('SELECT * FROM sale ORDER BY sale_id DESC LIMIT 1')->row();
        $data['saleno'] = 0000001;
        if (!empty($data['number'])) {
            $data['saleno'] =  $data['number']->sale_id + 1;
        }

        $data['pro'] = $this->Model_p->get_globalMultiWithCond('products', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);

        // echo "<pre>";
        //  print_r($data['pro']);
        //  die;

        $this->load->view('include/header');

        $this->load->view('sales/sales_product', $data);
        $this->load->view('include/footer');
    }

    public function types()
    {
        $result['data'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/all_types', $result);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
        $this->load->view('modals/new_type');
    }

    public function new_category()
    {
        $data = array(
            'category_name' => $this->input->post('category'),
            'cat_shortdesc' => $this->input->post('desc'),
            'instant_id' => $this->session->userdata('user')['inst_id']

        );

        $this->Model_p->create('category', $data);
        redirect('Products/categories');
    }

    public function update_category()
    {
        $id = $this->input->post('rec_id');


        $data = array(
            'category_name' => $this->input->post('category'),
            'cat_shortdesc' => $this->input->post('desc'),
        );

        $this->Model_p->updateRecord('category', ['category_id' => $id], $data);
        redirect('Products/categories');
    }

    public function delete_category($d)
    {
        $this->Model_p->updateRecord('category', ['category_id' => $d], ['is_trash' => 1]);
        redirect('Products/categories');
    }

    function getBarCode()
    {
        $bar = random_string('num', 3) . date('dHis');
        $a = $this->Model_p->get_globalSingWithCond('products', ['barcode' => $bar]);
        if ($a) {
            getBarCode();
        } else {
            return $bar;
        }
    }

    public function new_Product()
    {
        $barcode = $this->input->post("barcode");
        if (empty($this->input->post("barcode"))) {
            $barcode = $this->getBarCode();
        }

        $data = [
            'title' => $this->input->post("nam"),
            'barcode' => $barcode,
            'type_id' => $this->input->post("type"),
            'category_id' => $this->input->post("category"),
            'sale_price' => $this->input->post("saleprice"),
            'purchase_price' => $this->input->post("purchase_price"),
            'color' => $this->input->post('color'),
            'discount_tags' => $this->input->post('discount_tag'),
            'discount_price' => $this->input->post('discount_price'),
            'instant_id' => $this->session->userdata('user')['inst_id']
        ];
        $response = $this->Model_p->create('products', $data);

        $this->load->library('Ciqrcode');

        // if ($response) {

        //     $barc = $barcode;
        //     $params['data'] = $barc;
        //     $params['level'] = 'H';
        //     $params['size'] = 10;

        //     $params['savename'] = FCPATH  . 'media/qrcodes/' .  $barc . '.png';

        //     $params = $this->ciqrcode->generate($params);
        //     echo" xcvbnm";
        //     die;
        // }
        foreach ($this->input->post("tags") as $tag) {
            if (is_numeric($tag)) {
                $prdtag = [
                    "prd_id" => $response,
                    "tag_id" => $tag,
                ];
                $this->Model_p->create('prd_tag', $prdtag);
            } else {
                $tagdata = [
                    "tag_name" => $tag,
                ];
                $tag_id = $this->Model_p->create('tags', $tagdata);
                $prdtag = [
                    "prd_id" => $response,
                    "tag_id" => $tag_id,
                ];
                $this->Model_p->create('prd_tag', $prdtag);
            }
        }

        $files = $_FILES;
        $count = count($_FILES['file']['name']);
        $khan = '';


        for ($name = 0; $name < $count; $name++) {

            $khan = $files['file']['name'][$name];
            $_FILES['file']['name'] = $files['file']['name'][$name];
            $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$name];
            $_FILES['file']['type'] = $files['file']['type'][$name];
            $_FILES['file']['size'] = $files['file']['size'][$name];

            $path = realpath(APPPATH . '../media/products');


            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            $this->upload->do_upload('file');

            $data = array(
                'img_url' => $khan,
                'product_id' => $response
            );

            $this->Model_p->create('product_img', $data);
        }

        redirect('Products/products');
    }

    public function purchase()
    {

        $data['cat'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['pro'] = $this->Model_p->get_globalMultiWithCond('products', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['vendors'] = $this->Model_p->get_globalMultiWithCond('tbl_user', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['number'] = $this->db->get('purchase')->num_rows() + 1;



        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('operation/purchase');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
        //  $this->load->view('include/select2/select2_js');
    }

    public function delete_pro($id)
    {
        $this->Model_p->updateRecord('products', ['product_id' => $id], ['is_trash' => 1]);
        redirect('Products/products');
    }

    public function update_product()
    {
        $id = $this->input->post('hid');

        $data = [
            'title' => $this->input->post("nam"),
            'barcode' => $this->input->post("barcode"),
            'type_id' => $this->input->post("type"),
            'category_id' => $this->input->post("category"),
            'sale_price' => $this->input->post("saleprice"),
            'purchase_price' => $this->input->post("purchase_price"),
            'color' => $this->input->post('color'),
            'discount_tags' => $this->input->post('discount_tag'),
            'discount_price' => $this->input->post('discount_price'),
        ];
        foreach ($this->input->post("tags") as $tag) {
            if (is_numeric($tag)) {
                $prdtag = [
                    "prd_id" => $id,
                    "tag_id" => $tag,
                ];
                $this->Model_p->create('prd_tag', $prdtag);
            } else {
                $tagdata = [
                    "tag_name" => $tag,
                ];
                $tag_id = $this->Model_p->create('tags', $tagdata);
                $prdtag = [
                    "prd_id" => $id,
                    "tag_id" => $tag_id,
                ];
                $this->Model_p->create('prd_tag', $prdtag);
            }
        }

        $this->Model_p->updateRecord('products', ['product_id' => $id], $data);
        $this->db->where('product_id', $id)->delete('product_img');
        $files = $_FILES;
        $img = $_FILES['file']['name'];
        $count = count($img);
        $khan = '';
        if (!empty($count)) {

            for ($name = 0; $name < $count; $name++) {
                $khan = $files['file']['name'][$name];
                if (!empty($khan)) {

                    $_FILES['file']['name'] = $files['file']['name'][$name];
                    $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$name];
                    $_FILES['file']['type'] = $files['file']['type'][$name];
                    $_FILES['file']['size'] = $files['file']['size'][$name];

                    $path = realpath(APPPATH . '../media/products/');


                    $config['upload_path'] = $path;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('file');

                    $data = array(
                        'img_url' => $khan,
                        'product_id' => $id
                    );

                    $this->Model_p->create('product_img', $data);
                }
            }
        }
        if (!empty($this->input->post('oldimgs'))) {
            foreach ($this->input->post('oldimgs') as $v) {

                $data = array(
                    'img_url' => $v,
                    'product_id' => $id
                );

                $this->Model_p->create('product_img', $data);
            }
        }
        redirect('Products/edit_product/' . $id);
    }

    public function getslaledata()
    {
        $id = $this->input->post('id');
        $data['data'] = $this->Model_p->getsaledata($id);
        echo json_encode($data);
    }

    public function get_data_for_purchased()
    {
        $item_id = $this->input->post('id');
        $count = $this->input->post('total');

        $where = array('item_id' => $item_id);
        $data = $this->Model_p->get_purchased($item_id);

        if ($item_id != 0) {
            $output = '';
            $output .= '<tr id="entry_row_' . $count . '">';
            $output .= '<td id="serial_' . $count . '">' . $count . '</td>';
            $output .= '<td><input type="hidden" name="item_id[]" value="' . $data->item_id . '"> ' . $data->item_id . '</td>';
            $output .= '<input type="hidden" name="category_id[]" value="' . $data->category_id . '">';
            $output .= '<td>' . $data->item_name . '</td>';
            $output .= '<td><div id="spinner4">

     <input type="text" name="quantity[]" tabindex="1" id="quantity_' . $count . '" onclick="calculate_single_entry_sum(' . $count . ')" size="4" value="1" class="form-control col-lg-3" onkeyup="calculate_single_entry_sum(' . $count . ')">

                                </div>
                            </div></td>';
            $output .= '<td><input type="text" name="unit_price[]" readonly="readonly" id="unit_price_' . $count . '" size="6" value="' . $data->purchase_rate . '"></td>';
            $output .= '<td>
        <input type="text" name="purchase_amount[]" readonly="readonly" id="single_entry_total_' . $count . '" size="6" value="' . $data->purchase_rate . '">
        </td>';
            $output .= '<td>
<i style="cursor: pointer;" id="delete_button_' . $count . '" onclick="delete_row(' . $count . ')" class="fa fa-trash"></i>
				</td>';
            $output .= '</tr>';

            echo $output;
        } else {
            echo $output = 0;
        }
    }

    function insert_purchase()
    {
        $this->load->helper('date');
        $item_id = $this->input->post('item_id');
        $category_id = $this->input->post('category_id');
        $itemID = $this->input->post('unit_price');
        $unit_price = $this->input->post('unit_price');
        $quantity = $this->input->post('quantity');
        $purchase_code = $this->input->post('purchase_code');

        $purchase_amount = $this->input->post('purchase_amount');
        for ($i = 0; $i < count($item_id); $i++) {
            extract($_POST);

            if (strlen($unit_price[$i]) > 0) {
                $data1 = $this->Model_p->check_stock_record($item_id[$i], $category_id[$i]);
                if ($data1) {

                    $data = $this->Model_p->get_stock_qty($item_id[$i], $category_id[$i]);
                    $id = $data->stock_qty;
                    $new_id = $id + $quantity[$i];

                    $data = array(
                        "stock_qty" => $new_id,
                        "purchase_rate" => $unit_price[$i],
                    );
                    $where = array('item_id' => $item_id[$i], 'category_id' => $category_id[$i]);
                    $this->Model_p->updateRecord('stock', $where, $data);
                } else {
                    $data = array(
                        "item_id" => $item_id[$i],
                        "category_id" => $category_id[$i],
                        "stock_qty" => $quantity[$i],
                        "purchase_rate" => $unit_price[$i],
                    );
                    $this->Model_p->create('stock', $data);
                }
            }

            $maxID = $this->Model_p->get_purchase_max1();
            $max = $maxID->purchase_id;
            $pu_id = $max + 1;
            $purchase_data = array(
                'purchase_id' => $pu_id,
                'purchase_no' => $purchase_code,
                'item_id' => $item_id[$i],
                'category_id' => $category_id[$i],
                'purchase_qty' => $quantity[$i],
                'purchase_amount' => $purchase_amount[$i],
                'purchase_rate' => $unit_price[$i],
                'expire_date' => date('Y-m-d')
            );


            $data_entry = $this->Model_p->create('purchase', $purchase_data);
        } //for loop
        $vendor_id = $this->input->post('vendor_id');
        $company_id = $this->input->post('company_id');
        $paymentTotal = $this->input->post('paymentTotal');
        $discount = $this->input->post('discount');
        $due_amount = $this->input->post('due_amount');
        $sub_total = $this->input->post('sub_total');
        $pur = "PUR-" . $purchase_code . date('Y-m');
        $Purchase_comp_Ins = array(
            'purchase_no' => $purchase_code,
            'pur_no' => $pur,
            'purchase_date' => $this->input->post('purchase_date'),
            'vendor_id' => $vendor_id,
            'company_id' => $company_id,
            'purchase_amount_total' => $paymentTotal,
            'purchase_discount' => $discount,
            'due_amount' => $due_amount,
            'grand_total' => $sub_total,
            'purchase_status' => 1,
            'purchase_user_id' => $this->session->userdata('user_id')
        );
        $data_entry = $this->Model_p->create('purchase_company', $Purchase_comp_Ins);

        if ($data_entry) {

            $this->session->set_flashdata('success', 'Record added Successfully..!');
            redirect('Products/show_purchase_history/' . $purchase_code . '');
        }
    }

    public function show_purchase_history()
    {
        $id = $this->uri->segment(3);
        $id = explode('%', $id);
        print_r($id);
        exit();
        $data['history'] = $this->Model_p->get_purchaseHistory($id);
        $sql = $this->db->query("select * from purchase_company as p,vendor as v, company as c where purchase_no=$id AND p.vendor_id=v.vendor_id and c.company_id = v.company_id");
        $data['amount'] = $sql->row();
        $this->load->view('operation/item_purchase_history', $data);
    }

    public function new_purchase()
    {
        $id = $this->input->post('dat');


        $dat['str'] = $this->Model_p->get_globalMultiWithCond('tbl_product', ['p_cat' => $id]);

        echo json_encode($dat);





        //sprint_r($result['dat']);
        //   echo $result['dat'];
    }

    public function add_type()
    {
        $data = array(
            'type_name' => $this->input->post('typ'),
            'type_shortdesc' => $this->input->post('description'),
            'instant_id' => $this->session->userdata('user')['inst_id'],
        );

        $this->Model_p->create('tbl_type', $data);
        redirect('Products/types');
    }

    public function dalete_type($d)
    {
        $this->Model_p->updateRecord('tbl_type', ['type_id' => $d], ['is_trash' => 1]);
        redirect('Products/types');
    }

    public function update_Type()
    {
        $id = $this->input->post('hid');
        $data = array(
            'type_name' => $this->input->post('typ'),
            'type_shortdesc' => $this->input->post('description'),
        );

        $this->Model_p->updateRecord('tbl_type', ['type_id' => $id], $data);
        redirect('Products/types');
    }
    public function getsalreportdata()
    {
        $from = $this->input->post('fromdate');
        $to = $this->input->post('todate');
        $data['rep'] = $this->db->select('*');
        // ->join('products' , 'products.product_id = sale_items.salitem_id '  ); 
        if (!empty($from)) {
            $data['rep'] =   $this->db->where('sale_date >=', $from);
        }
        if (!empty($to)) {
            $data['rep'] =   $this->db->where('sale_date <=', $to);
        }

        $data['rep'] =  $this->db->where('instant_id', $this->session->userdata('user')['inst_id']);

        $data['rep'] =     $this->db->get('sale')->result();
        $data['settings'] = $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);

        // $this->load->view('include/head');
        // $this->load->view('include/header');
        // $this->load->view('include/menusetting');
        // $this->load->view('include/sidebar');
        $this->load->view('sales/sales_report', $data);
        // $this->load->view('include/foot');
        // $this->load->view('include/footer');
    }
    public function getpurreportdata()
    {
        $from = $this->input->post('fromdate');
        $to = $this->input->post('todate');
        $data['rep'] = $this->db->join('purchase_items', 'purchase_items.puritem_purid = purchase.pur_id')
            ->join('tbl_user', 'tbl_user.uid = purchase.pur_created_by ')
            ->join('products', 'products.product_id = purchase_items.puritem_itemid ');
        // $result =   $this->db->join('products' , 'products.product_id = purchase_items.puritem_itemid ');
        if (!empty($from)) {
            $data['rep'] =   $this->db->where('pur_date >=', $from);
        }
        if (!empty($to)) {
            $data['rep'] =   $this->db->where('pur_date <=', $to);
        }
        $data['rep'] =     $this->db->get('purchase')->result();


        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die;

        $data['settings'] =  $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        // $this->load->view('include/head');
        // $this->load->view('include/header');
        // $this->load->view('include/menusetting');
        // $this->load->view('include/sidebar');
        $this->load->view('reports/purchase_report', $data);
        // $this->load->view('include/foot');
        // $this->load->view('include/footer');
    }
    public function expense()
    {
        $data['coa_type'] = $this->Model_p->get_global('coa_type');
        $data['coa_subtype'] = $this->Model_p->get_global('coa_subtype');
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/expense', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function add_expense()
    {
        $new = $this->input->post('hiddenimg');
        if ($_FILES['file']['name'] != '') {
            $new = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/logo');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new;
            } else {
                $file = $this->upload->display_errors();
            }
        }



        $id['coast'] = $this->Model_p->chek_global('chart_of_account');
        $up = $id['coast']->file;

        $data = [
            'coa_name  ' => $this->input->post('rec_name'),
            'subtype_id' => $this->input->post('coa_subtype'),
            'amount' => $this->input->post('coa_amount'),
            'coa_description' => $this->input->post('coa_desc'),
            'img' => $new,
            'cr_by' => $this->session->userdata('user')['user_id'],
            'coa_user_id' => $this->session->userdata('user')['user_id'],
            'instant_id' => $this->session->userdata('user')['inst_id'],

        ];


        $this->Model_p->createtbl('chart_of_account', $data);
        redirect('products/expense');
    }

    public function expense_list()
    {
        $data['expense'] = $this->Model_p->getAllexpense();




        $data['coa_type'] = $this->Model_p->get_global('coa_type');
        $data['coa_subtype'] = $this->Model_p->get_global('coa_subtype');

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/expense_list', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }
    public function update_expense()
    {


        $id =  $this->input->post('coa');
        $file = '';
        if (!empty($_FILES['file'])) {

            $file = $_FILES['file']['name'];
            $path = APPPATH . "../media/heads/";
            $config = array(
                'upload_path' => $path,
                'allowed_types' => "gif|jpg|png|jpeg|docx|doc|pdf|xls|xlxs|csv",
                'overwrite' => TRUE,


            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('file');
        } else {
            $file = $this->input->post('hiddenfile');
        }

        $data = [
            'coa_name  ' => $this->input->post('recipient'),
            'subtype_id' => $this->input->post('coa_subtype'),
            'amount' =>  $this->input->post('amount'),
            'coa_description' =>  $this->input->post('coa_desc'),
            'img' => $file,
            'cr_by' => $this->session->userdata('user')['user_id'],
            'coa_user_id' => $this->session->userdata('user')['user_id'],
            'instant_id' => $this->session->userdata('user')['inst_id'],

        ];


        $id =   $this->Model_p->createtbl('chart_of_account', $data);
        redirect('products/expense_list');
    }

    // public function post($id)
    //   {
    //      $data = [
    //       'is_approved' => 1,  
    //      ];
    //      $this->Api_user->update('requests' , $data , ['req_id' => $id]);

    //      redirect('productslead_request');

    //   }

    public function salary()
    {
        $data['employee'] = $this->Model_p->get_global('tbl_user');


        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/salary', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }
    public function add_salary()
    {

        $data = [
            'salary_userid' => $this->input->post('emp_name'),
            'salary_month' => $this->input->post('s_month'),
            'salary' => $this->input->post('salary'),
            'instant_id' => $this->session->userdata('user')['inst_id'],
            'user_id' => $this->session->userdata('user')['user_id'],

        ];

        $this->Model_p->createtbl('salary', $data);
        redirect('products/salary');
    }

    public function postcoa($id)
    {

        $data = [
            'is_update' => 0
        ];
        $this->Model_p->updateRecord('chart_of_account', ['coa_id' => $id], $data);
        redirect('products/expense_list');
    }
    public function deletecoa($id)
    {

        $data = [
            'is_trash' => 1
        ];
        $this->Model_p->updateRecord('chart_of_account', ['coa_id' => $id], $data);
        redirect('products/expense_list');
    }

    public function exp_report()
    {
        $data['coa_type'] = $this->Model_p->get_global('coa_type');
        $data['coa_subtype'] = $this->Model_p->get_global('coa_subtype');
        $data['coa_name'] = $this->db->group_by('coa_name')->get('chart_of_account')->result();


        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/exp_reports', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function getheadsreport()
    {

        $coa_type = $this->input->post('coa_type');
        $coa_sub = $this->input->post('coa_subtype');
        $coa_name = $this->input->post('coa_name');
        $from_date = $this->input->post('fromdate');
        $to_date = $this->input->post('todate');
        $result['rep'] = $this->Model_p->getheadsrepot($coa_type, $coa_sub, $coa_name, $from_date, $to_date);

        $this->load->view('expenses/expense_report', $result);
    }


    public function getsalaryreport()
    {

        $month = $this->input->post('s_month');
        $data['salary'] = $this->db->join('tbl_user', 'tbl_user.uid = salary.salary_userid');
        if (!empty($month)) {
            $data['salary'] = $this->db->like('salary_month', $month);
        }
        $data['salary'] = $this->db->where(
            'salary.instant_id',
            $this->session->userdata('user')['inst_id']
        );

        $data['salary'] = $this->db->get('salary')->result();



        $this->load->view('expenses/salar_report', $data);
    }

    public function salary_report_view()
    {

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/salary_report_view');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }
    //   public function getpurreportdata()
    //   {
    //       $from = $this->input->post('fromdate');
    //       $to = $this->input->post('todate');
    //       $data['rep'] = $this->db->join('purchase_items', 'purchase_items.puritem_purid = purchase.pur_id')
    //           ->join('tbl_user', 'tbl_user.uid = purchase.pur_created_by ');
    //       // $result =   $this->db->join('products' , 'products.product_id = purchase_items.puritem_itemid ');
    //       if (!empty($from)) {
    //           $data['rep'] =   $this->db->where('pur_date >=', $from);
    //       }
    //       if (!empty($to)) {
    //           $data['rep'] =   $this->db->where('pur_date <=', $to);
    //       }
    //       $data['rep'] =     $this->db->get('purchase')->result();
    //       $data['settings'] = $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);
    //       // $this->load->view('include/head');
    //       // $this->load->view('include/header');
    //       // $this->load->view('include/menusetting');
    //       // $this->load->view('include/sidebar');
    //       $this->load->view('reports/purchase_report', $data);
    //       // $this->load->view('include/foot');
    //       // $this->load->view('include/footer');
    //   }

    public function getprofitlossstt()
    {
        $from = $this->input->post('fromdate');
        $to = $this->input->post('todate');

        $data['pur'] = $this->db->select('*, sum(pur_total) as totalpriceofpurchse');
        // $result =   $this->db->join('products' , 'products.product_id = purchase_items.puritem_itemid ');
        if (!empty($from)) {
            $data['pur'] =   $this->db->where('pur_date >=', $from);
        }
        if (!empty($to)) {
            $data['pur'] =   $this->db->where('pur_date <=', $to);
        }
        $data['pur'] =  $this->db->where('instant_id', $this->session->userdata('user')['inst_id']);
        $data['pur'] =     $this->db->get('purchase')->row();



        $data['sale'] = $this->db->select('*,  sum(sale_total) as totalpriceofsell');
        // ->join('products' , 'products.product_id = sale_items.salitem_id '  ); 
        if (!empty($from)) {
            $data['sale'] =   $this->db->where('sale_date >=', $from);
        }
        if (!empty($to)) {
            $data['sale'] =   $this->db->where('sale_date <=', $to);
        }

        $data['sale'] =  $this->db->where('instant_id', $this->session->userdata('user')['inst_id']);

        $data['sale'] =     $this->db->get('sale')->row();


        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die;

        $data['settings'] =  $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);



        $data['heads'] = $this->db->select('*, sum(amount) as ttlheadamunt')->join('coa_subtype', 'coa_subtype.coa_subtype_id = chart_of_account.subtype_id')
            ->join('coa_type', 'coa_type.coa_type_id = coa_subtype.coa_subtype_typeid');
        if (!empty($from)) {
            $data['heads'] = $this->db->where('date >=', $from);
        }
        if (!empty($to)) {
            $data['heads'] = $this->db->where('date <=', $to);
        }


        $data['heads'] = $this->db->where(
            'instant_id',
            $this->session->userdata('user')['inst_id']
        );

        $data['heads'] = $this->db->group_by('coa_subtype.coa_subtype_name');
        $data['heads'] = $this->db->get('chart_of_account')->row();

        $data['fromdate'] = $this->input->post('fromdate');
        $data['todate'] = $this->input->post('todate');



        $this->load->view('reports/profitandlossreport', $data);
    }
    public function purchasepaymenthistory()
    {
        $from = $this->input->post('fromdate');
        $to = $this->input->post('todate');
        $data['settings'] =  $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['purchase_payment'] = $this->Model_p->getPurchasesPaymentsall($from, $to);


        $data['single'] =  $this->db
            ->join('tbl_user', 'tbl_user.uid = purchase_payment.payment_by', 'left')
            ->join('purchase', 'purchase.pur_id = purchase_payment.pur_purchase_id')
            ->order_by('purchase.pur_id', 'asc')
            ->group_by('purchase_payment.pur_purchase_id')
            ->get('purchase_payment')->result();


        $this->load->view('reports/creditdepitrpt', $data);
    }

    public function receivable()
    {

        $from = $this->input->post('fromdate');
        $to = $this->input->post('todate');
        $data['settings'] =  $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['sell_payment'] = $this->Model_p->getsellPaymentsall($from, $to);
        $this->load->view('reports/recievablerpt', $data);
    }
}
