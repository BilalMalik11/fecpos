<?php

defined('BASEPATH') or exit('No direct script access allowed');

class expenses extends CI_Controller {

public function expense()
{
  $data['coa_type'] = $this->Model_p->get_global('coa_type');
  $data['coa_subtype'] = $this->Model_p->get_global('coa_subtype');
	$this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/expense' , $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');

}

public function add_expense()
{
   $new = $this->input->post('hiddenimg');
        if ($_FILES['file']['name'] != '') {
            $new = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/logo');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new;
            } else {
                $file = $this->upload->display_errors();
            }
        }
 


        $id['coast'] = $this->Model_p->chek_global('chart_of_account');
        $up = $id['coast']->file;

         $data =[
               'coa_name  ' => $this->input->post('rec_name'),
               'subtype_id' => $this->input->post('coa_subtype'),
               'amount' => $this->input->post('coa_amount'),
               'coa_description' => $this->input->post('coa_desc'),
               'img' => $new,
               'cr_by' => $this->session->userdata('user')['user_id'],
               'coa_user_id' => $this->session->userdata('user')['user_id'],
               'instant_id' => $this->session->userdata('user')['inst_id'],
               
                 ];

                
                        $this->Model_p->createtbl('chart_of_account' , $data);
                        redirect('expenses/expense');


}

public function expense_list()
{
  $data['expense']= $this->Model_p->getAllexpense();

 


  $data['coa_type'] = $this->Model_p->get_global('coa_type');
  $data['coa_subtype'] = $this->Model_p->get_global('coa_subtype');
  
         $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/expense_list', $data );
        $this->load->view('include/foot');
        $this->load->view('include/footer');

}
  public function update_expense()
    { 
       

        $id =  $this->input->post('coa');
        $file = '';
        if (!empty($_FILES['file'])) {
      
            $file = $_FILES['file']['name'];
            $path = APPPATH . "../media/heads/";
            $config = array(
                'upload_path' => $path,
                'allowed_types' => "gif|jpg|png|jpeg|docx|doc|pdf|xls|xlxs|csv",
                'overwrite' => TRUE,

            
            );
            $this->load->library('upload', $config);
             $this->upload->do_upload('file');
        } else {
            $file = $this->input->post('hiddenfile');
        }

        $data = [
          'coa_name  ' => $this->input->post('recipient'),
           'subtype_id' => $this->input->post('coa_subtype'),
            'amount' =>  $this->input->post('amount'),
            'coa_description' =>  $this->input->post('coa_desc'),
            'img' => $file,
            'cr_by' => $this->session->userdata('user')['user_id'],
            'coa_user_id' => $this->session->userdata('user')['user_id'],
            'instant_id' => $this->session->userdata('user')['inst_id'],
            
        ];

    
         $id =   $this->Model_p->createtbl('chart_of_account'  , $data);
         redirect('expenses/expense_list');


    }
  
    
public function salary()
{
        $data['employee'] = $this->Model_p->get_global('tbl_user'); 


       $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/salary',$data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
	
}
public function add_salary()
{

             $data =[
               'salary_userid' => $this->input->post('emp_name'),
               'salary_month' => $this->input->post('s_month'),
               'salary' => $this->input->post('salary'),
               'instant_id' => $this->session->userdata('user')['inst_id'],
               'user_id' => $this->session->userdata('user')['user_id'],
               
                 ];

                        $this->Model_p->createtbl('salary' , $data);
                        redirect('expenses/salary');


}

  public function postcoa($id){

    $data = [
      'is_update' => 0
    ];
    $this->Model_p->updateRecord('chart_of_account' , ['coa_id' => $id] , $data);
    redirect('expenses/expense_list');
  }
    public function deletecoa($id){

    $data = [
      'is_trash' => 1
    ];
    $this->Model_p->updateRecord('chart_of_account' , ['coa_id' => $id] , $data);
    redirect('expenses/expense_list');
  }

  public function exp_report()
  {
     $data['coa_type'] = $this->Model_p->get_global('coa_type');
     $data['coa_subtype'] = $this->Model_p->get_global('coa_subtype');
     $data['coa_name'] = $this->db->group_by('coa_name')->get('chart_of_account')->result();


       $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/exp_reports',$data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');

  }

  public function getheadsreport(){

$coa_type = $this->input->post('coa_type');
$coa_sub = $this->input->post('coa_subtype');
$coa_name = $this->input->post('coa_name');
$from_date = $this->input->post('fromdate');
$to_date = $this->input->post('todate');
$result['rep'] = $this->Model_p->getheadsrepot($coa_type , $coa_sub , $coa_name , $from_date , $to_date); 
 
 $this->load->view('expenses/expense_report' , $result);
  }


    public function getsalaryreport(){

$month = $this->input->post('s_month'); 
$data['salary'] = $this->db->join('tbl_user' , 'tbl_user.uid = salary.salary_userid');
if(!empty($month)){
$data['salary'] = $this->db->like('salary_month' , $month);
}  
$data['salary'] = $this->db->where('instant_id', 
 $this->session->userdata('user')['inst_id']) ;

$data['salary'] = $this->db->get('salary')->result();

 
 
 $this->load->view('expenses/salar_report' , $data);





  }

    public function salary_report_view()
    { 

       $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('expenses/salary_report_view' );
        $this->load->view('include/foot');
        $this->load->view('include/footer');

  }
}
