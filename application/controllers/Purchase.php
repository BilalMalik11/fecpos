<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * 	@author : Imran Shah
 *  @support: shahmian@gmail.com
 * 	date	: 18 April, 2018
 * 	Kandi Inventory Management System
 * website: kelextech.com
 *  version: 1.0
 */

class Purchase extends CI_Controller
{

    public function request()
    {
        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('purchase/request');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function single_purchase($id)
    {
        $data['cat'] = $this->Model_p->get_globalMultiWithCond('category', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['pro'] = $this->Model_p->get_globalMultiWithCond('products', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['type'] = $this->Model_p->get_globalMultiWithCond('tbl_type', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['vendors'] = $this->Model_p->get_globalMultiWithCond('tbl_user', ['is_trash' => 0], ['instant_id' => $this->session->userdata('user')['inst_id']]);

        //        single data
        $data['purchase'] = $this->Model_p->get_globalSingWithCond('purchase', ['pur_id' => $id], ['instant_id' => $this->session->userdata('user')['inst_id']]);
        $data['purchase_item'] = $this->Model_p->getPurchaseItems($id);
        $data['purchase_payment'] = $this->Model_p->getPurchasesPayments($id);
        //echo '<pre>';print_r($data['purchase_payment']);exit();

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('purchase/single_purchase');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function insert_purchase()
    {
        $is_stock = 0;
        if (isset($_POST['stocked'])) {
            $is_stock = 1;
        }
        $new_name = '';
        if ($_FILES['file']['name'] != '') {
            $new_name = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new_name
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new_name;
            } else {
                $file = $this->upload->display_errors();
            }
        }
        $vanderid = '';
        if (is_numeric($this->input->post('vendorid'))) {
            $vanderid = $this->input->post('vendorid');
        } else {

            $venderdata = [
                'email' => $this->input->post('email'),
                'user_name' => $this->input->post('vendorid'),
                'name' => $this->input->post('vendorid'),
                'contact' => $this->input->post('contact'),
                'address' => $this->input->post('Address'),
                'u_password' => md5(12345),
                'instant_id' => $this->session->userdata('user')['inst_id']
            ];
            $vanderid = $this->Model_p->create('tbl_user', $venderdata);
        }

        $purchase = [
            'pur_vendor_id' => $vanderid,
            'pur_date' => $this->input->post('purchase_date'),
            'pur_total' => $this->input->post('total'),
            'pur_created_by' => $this->session->userdata('user')['user_id'],
            'pur_created_on' => date('Y-m-d H:i:s'),
            'pur_remarks' => $this->input->post('remarks'),
            'pur_attachment' => $new_name,
            'is_stock' => $is_stock,
            'instant_id' => $this->session->userdata('user')['inst_id']
        ];

        $pur_id = $this->Model_p->create('purchase', $purchase);



        $id = $this->input->post('p_id');
        $price = $this->input->post('product_price');
        $quatity = $this->input->post('product_quantity');
        $subtotal = $this->input->post('subtotal');

        foreach ($id as $key => $value) {
            $item = [
                'puritem_itemid' => $value,
                'puritem_purid' => $pur_id,
                'puritem_price' => $price[$key],
                'puritem_qty' => $quatity[$key],
                'puritem_total' => $subtotal[$key]
            ];
            $item_idd = $this->Model_p->create('purchase_items', $item);
            if (!empty($is_stock)) {
                if (!empty($quatity[$key])) {
                    for ($i = 0; $i < $quatity[$key]; $i++) {
                        $stoook = [
                            'stock_voucheritemline_id' => $item_idd,
                            'stock_itemid' => $value,
                            'stock_price' => $price[$key],
                            'stock_condition' => '+',
                            'stock_entry_date' => date('Y-m-d H:i:s')
                        ];
                        $this->Model_p->create('stock', $stoook);
                    }
                }
            }
        }
        if (!empty($this->input->post('paid'))) {
            $payments = [
                'pur_purchase_id' => $pur_id,
                'purpay_amount' => $this->input->post('paid'),
                'rem_amount' => $this->input->post('total'),
                'payment_by' => $this->session->userdata('user')['user_id'],
                'payment_on' => date('Y-m-d H:i:s')
            ];
            $this->Model_p->create('purchase_payment', $payments);
        }
        redirect('purchase/single_purchase/' . $pur_id);
    }

    public function update_purchase($id)
    {

        $new_name = $this->input->post('oldimage');
        if ($_FILES['file']['name'] != '') {
            $new_name = $_FILES["file"]['name'];
            $adver = realpath(APPPATH . '../media/');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new_name
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new_name;
            } else {
                $file = $this->upload->display_errors();
            }
        }
        $vanderid = '';
        if (is_numeric($this->input->post('vendorid'))) {
            $vanderid = $this->input->post('vendorid');
        } else {

            $venderdata = [
                'user_type' => 2,
                'email' => $this->input->post('email'),
                'user_name' => $this->input->post('vendorid'),
                'name' => $this->input->post('vendorid'),
                'contact' => $this->input->post('contact'),
                'address' => $this->input->post('Address'),
                'u_password' => md5(12345),
                'instant_id' => $this->session->userdata('user')['inst_id']
            ];
            $vanderid = $this->Model_p->create('tbl_user', $venderdata);
        }

        $purchase = [
            'pur_vendor_id' => $vanderid,
            'pur_date' => $this->input->post('purchase_date'),
            'pur_total' => $this->input->post('total'),
            'pur_created_by' => $this->session->userdata('user')['user_id'],
            'pur_created_on' => date('Y-m-d H:i:s'),
            'pur_remarks' => $this->input->post('remarks'),
            'pur_attachment' => $new_name,
            'instant_id' => $this->session->userdata('user')['inst_id']
        ];

        $this->Model_p->updateRecord('purchase', ['pur_id' => $id], $purchase);
        $pur_id = $id;



        $id = $this->input->post('p_id');
        $price = $this->input->post('product_price');
        $quatity = $this->input->post('product_quantity');
        $subtotal = $this->input->post('subtotal');
        $itemssss = $this->db->where('puritem_purid', $pur_id)->get('purchase_items')->result();
        if (!empty($itemssss)) {
            foreach ($itemssss as $ii) {
                $this->db->where('stock_voucheritemline_id', $ii->puritem_id)->delete('stock');
            }
        }
        $this->db->where('puritem_purid', $pur_id)->delete('purchase_items');
        foreach ($id as $key => $value) {
            $item = [
                'puritem_itemid' => $value,
                'puritem_purid' => $pur_id,
                'puritem_price' => $price[$key],
                'puritem_qty' => $quatity[$key],
                'puritem_total' => $subtotal[$key]
            ];

            $item_idd = $this->Model_p->create('purchase_items', $item);
            if (!empty($is_stock)) {
                if (!empty($quatity[$key])) {
                    for ($i = 0; $i < $quatity[$key]; $i++) {
                        $stoook = [
                            'stock_voucheritemline_id' => $item_idd,
                            'stock_itemid' => $value,
                            'stock_price' => $price[$key],
                            'stock_condition' => '+',
                            'stock_entry_date' => date('Y-m-d H:i:s'),
                            'instant_id' => $this->session->userdata('user')['inst_id']
                        ];
                        $this->Model_p->create('stock', $stoook);
                    }
                }
            }
        }
        if (!empty($this->input->post('paid'))) {
            $payments = [
                'pur_purchase_id' => $pur_id,
                'purpay_amount' => $this->input->post('paid'),
                'payment_by' => $this->session->userdata('user')['user_id'],
                'payment_on' => date('Y-m-d H:i:s'),
                'instant_id' => $this->session->userdata('user')['inst_id']
            ];
            $this->Model_p->create('purchase_payment', $payments);
        }
        redirect('purchase/single_purchase/' . $pur_id);
    }

    public function cancelbill($param)
    {
        $purchase = [
            'is_cancel' => 1,
            'cancel_by' => $this->session->userdata('user')['user_id'],
            'cancel_on' => date('Y-m-d H:i:s'),
            'cancel_reason' => $this->input->post('reason'),
            'instant_id' => $this->session->userdata('user')['inst_id']
        ];

        $this->Model_p->updateRecord('purchase', ['pur_id' => $param], $purchase);
        redirect('purchase/single_purchase/' . $param);
    }

    public function addstock($param)
    {
        $purchase = [
            'is_stock' => 1,
            'instant_id' => $this->session->userdata('user')['inst_id'],
        ];

        $this->Model_p->updateRecord('purchase', ['pur_id' => $param], $purchase);
        $itemssss = $this->db->where('puritem_purid', $param)->get('purchase_items')->result();
        if (!empty($itemssss)) {
            foreach ($itemssss as $ii) {
                if (!empty($ii->puritem_qty)) {
                    for ($i = 0; $i < $ii->puritem_qty; $i++) {
                        $stoook = [
                            'stock_voucheritemline_id' => $ii->puritem_id,
                            'stock_itemid' => $ii->puritem_itemid,
                            'stock_price' => $ii->puritem_price,
                            'stock_condition' => '+',
                            'stock_entry_date' => date('Y-m-d H:i:s'),
                            'instant_id' => $this->session->userdata('user')['inst_id']
                        ];
                        $this->Model_p->create('stock', $stoook);
                    }
                }
            }
        }
        redirect('purchase/single_purchase/' . $param);
    }

    public function paybill($param)
    {

        $payments = [
            'pur_purchase_id' => $param,
            'purpay_amount' => $this->input->post('amount'),
            'rem_amount' => $this->input->post('total'),
            'payment_by' => $this->session->userdata('user')['user_id'],
            'payment_on' => date('Y-m-d H:i:s'),
            'instant_id' => $this->session->userdata('user')['inst_id']
        ];
        $this->Model_p->create('purchase_payment', $payments);
        redirect('purchase/single_purchase/' . $param);
    }

    public function purchase_voucer()
    {

        $chek = '';
        $chek =  $this->input->post('all');
        if ($chek == '') {
            $data['purchase'] = $this->Model_p->getAllpurchases();
        } elseif ($chek == 2) {
            $data['purchase'] = $this->Model_p->getAllpurchasesstocked();
        } elseif ($chek == 3) {
            $data['purchase'] = $this->Model_p->getAllpurchasescanceled();
        }



        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('purchase/purchase_vouchers');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function purchase_stock()
    {
        $data['purchase'] = $this->Model_p->getpurchasess(1);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('purchase/purchase_vouchers');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function purchase_unstock()
    {
        $data['purchase'] = $this->Model_p->getpurchasess(0);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('purchase/purchase_vouchers');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function purchase_print($id)
    {
        $data['purchase'] = $this->Model_p->getpurchasess(0);

        $data['purchase'] = $this->Model_p->get_globalSingWithCond('purchase', ['pur_id' => $id]);
        $data['purchase_item'] = $this->Model_p->getPurchaseItems($id);
        $data['purchase_payment'] = $this->Model_p->getPurchasesPayments($id);
        $data['purchase_vendor'] = $this->Model_p->get_globalSingWithCond('tbl_user', ['uid' => $data['purchase']->pur_vendor_id]);
        $data['settings'] = $this->Model_p->get_globalSingWithCond('logo_setting', ['logo_set_id' => 1]);

        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting', $data);
        $this->load->view('include/sidebar');
        $this->load->view('purchase/purchase_print');
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }

    public function purchasepaymenthistory()
    {

        $data['purchase_payment'] = $this->Model_p->getPurchasesPaymentsall();
    }
}
