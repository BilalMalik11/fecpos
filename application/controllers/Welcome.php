<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user')['user_id']) {
            redirect('Auth/login');
        }
    }

    public function index()
    {
      $data = $this->db->join('category', 'category.category_id = products.category_id' , 'left')
                        ->join('tbl_type', 'tbl_type.type_id = products.type_id')
                        ->where('products.is_trash', 0)
                        ->where('products.instant_id', $this->session->userdata('user')['inst_id'])
                        ->get('products')->result();
        $abc = [];
        if (!empty($data)) {
            foreach ($data as $products) {

                $abc[$products->product_id]['product'] = $products;
                $abc[$products->product_id]['pur'] = $pur = $this->db->where('stock_itemid',$products->product_id)
                                                            ->where('stock_condition','+')->get('stock')->num_rows();
                $abc[$products->product_id]['sal'] = $sal = $this->db->where('stock_itemid',$products->product_id)
                                                            ->where('stock_condition','-')->get('stock')->num_rows();
                $abc[$products->product_id]['stock'] = $pur - $sal; 
                $abc[$products->product_id]['proimage'] = $this->db->where('product_id', $products->product_id)->get('product_img')->result();
            }
            
        }
        $data['pro'] = $abc;
            
            $data['result'] = $this->Model_p->get_globalSingWithCond('tbl_user', ['uid' => $this->session->userdata('user')['user_id']], ['instant_id' => $this->session->userdata('user')['inst_id']] );

        $data['bestsell'] = $this->Model_p->getsaleAvgItems(); 
        $data['allprodsnum'] = $this->db->join('purchase_items', 'purchase_items.puritem_itemid = products.product_id')
            ->where('purchase_items.puritem_qty >=',  1)
            ->where('products.is_trash', 0)
            ->get('products')->num_rows();
 



        $this->load->view('include/head');
        $this->load->view('include/header');
        $this->load->view('include/menusetting');
        $this->load->view('include/sidebar');
        $this->load->view('operation/dashboard', $data);
        $this->load->view('include/foot');
        $this->load->view('include/footer');
    }


}
