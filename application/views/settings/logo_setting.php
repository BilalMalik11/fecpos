
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Print Logo Setting</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Settings</a></li>
                            <li class="breadcrumb-item active" aria-current="page">update</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <form action="<?= base_url("settings/update_logo_set"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">

                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Website Name</label>
                                        <input class="form-control" value="<?php if (!empty($da->app_name)) { echo $da->app_name;} ?>" name="app_name" > 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control"  value="<?php if(!empty($da->logo_set_email)) {  echo $da->logo_set_email;} ?>" name="logo_set_email"  > 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone / Contact</label>
                                        <input class="form-control"  value="<?php if(!empty($da->logo_set_contact)) { echo $da->logo_set_contact;} ?>" name="logo_set_contact" > 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alingment</label>
                                        <select name="logo_set_align" class="custom-select2 form-control" >
                                            <option value="" >select</option>
                                            <option <?php if(!empty($da->logo_set_align)) { if ($da->logo_set_align == 'center') { ?> selected <?php }} ?> value="center" >Center</option>
                                            <option <?php if(!empty($da->logo_set_align)) { if ($da->logo_set_align == 'left') { ?> selected <?php }} ?> value="left" >Left</option>
                                            <option <?php if(!empty($da->logo_set_align)) { if ($da->logo_set_align == 'right') { ?> selected <?php }} ?> value="right" >Right</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea  name="logo_set_address" rows="4" class="form-control">  <?php if(!empty($da->logo_set_address)) { echo $da->logo_set_address; } ?> </textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">  
                                <label>Logo:</label>
                                <?php if (!empty($da->logo_set_logo)) { ?>
                                    <img src="<?= base_url('media/logo/') . $da->logo_set_logo; ?>" class="avatar-photo"/>
                                <?php } else { ?>
                                    <img src="<?= base_url('media/placeholder.png'); ?>" class="avatar-photo"/>
                                <?php } ?>
                                <input type="file" name="file" onchange="displayImg(this)"  placeholder="Image">
                                <input type="hidden" name="hiddenimg"  value="<?php if(!empty($da->logo_set_logo)){ echo $da->logo_set_logo;}  ?>" >
                            </div>
                        </div>
                    </div>



                    <div class="row  ">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary  btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Save Settings</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
