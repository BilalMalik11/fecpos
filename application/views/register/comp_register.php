<!DOCTYPE html>
<html>
    <style>
        
        #fade {
  display: none;
  position: fixed;
  top: 0%;
  left: 0%;
  width: 100%;
  height: 100%;
  background-color: black;
  z-index: 1001;
  -moz-opacity: 0.8;
  opacity: .80;
  filter: alpha(opacity=80);
}

#light {
  display: none;
  position: absolute;
  top: 50%;
  left: 50%;
  max-width: 600px;
  max-height: 360px;
  margin-left: -300px;
  margin-top: -180px;
  border: 2px solid #FFF;
  background: #FFF;
  z-index: 1002;
  overflow: visible;
}

#boxclose {
  float: right;
  cursor: pointer;
  color: #fff;
  border: 1px solid #AEAEAE;
  border-radius: 3px;
  background: #222222;
  font-size: 31px;
  font-weight: bold;
  display: inline-block;
  line-height: 0px;
  padding: 11px 3px;
  position: absolute;
  right: 2px;
  top: 2px;
  z-index: 1002;
  opacity: 0.9;
}

.boxclose:before {
  content: "×";
}

#fade:hover ~ #boxclose {
  display:none;
}

.test:hover ~ .test2 {
  display: none;
}
    </style>
    <head>
        <!-- Basic Page Info -->
        <meta charset="utf-8">
        <title> </title>

        <link rel="stylesheet" type="text/css" href=" <?= base_url('vendors/styles/core.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('vendors/styles/icon-font.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/datatables/css/dataTables.bootstrap4.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/datatables/css/responsive.bootstrap4.min.css') ?>">
        <!--<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/select2/dist/css/select2.min.css') ?>">-->
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/bootstrap/bootstrap.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('vendors/styles/style.css') ?>">
        <script src="<?= base_url('src/scripts/jquery.min.js') ?>"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/sweetalert2/sweetalert2.css') ?>">
        <script src="<?= base_url('src/plugins/sweetalert2/sweetalert2.all.js') ?>"></script>
        <script src="<?= base_url('src/plugins/sweetalert2/sweet-alert.init.js') ?>"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



    </head>
    <body class="login-page">
      

<div id="light">
  <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
  <video id="VisaChipCardVideo" width="600" controls>
      <source src="<?= base_url('media/posvid.mp4') ?>" type="video/mp4">
      <!--Browser does not support <video> tag -->
    </video>
</div>

<div id="fade" onClick="lightbox_close();"></div>

<div>
  
</div>

        <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 col-lg-7">
                        <img src="<?= base_url('vendors/images/login-page-img.png') ?>" alt="">
                    </div>
                    <div class="col-md-6 col-lg-5">
                        <div class="login-box bg-white box-shadow border-radius-10">
                            <div class="login-title">
                                <h2 class="text-center text-primary">Login</h2>
                              
                            </div>
                            <form action="<?= base_url('auth/login_valid'); ?>" method="post">

                                <div class="input-group custom">
                                    <input type="text" name="c_name" class="form-control form-control-lg" placeholder="Username">
                                    <div class="input-group-append custom">
                                        <span class="input-group-text"><i class="icon-copy dw dw-user1"></i></span>
                                    </div>
                                </div>
                                <div class="input-group custom">
                                    <input type="password" name="c_pass" class="form-control form-control-lg" placeholder="Password">
                                    <div class="input-group-append custom">
                                        <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group mb-0">
                                            <button class="btn btn-primary btn-lg btn-block" onclick="register()">Sign In</button>
                                            <!-- 										<a class="btn btn-primary btn-lg btn-block">Sign In</a> -->

               <!-- <a class="btn btn-primary btn-lg btn-block" href="<?= base_url('welcome/index') ?>">Sign In</a> -->
                                        </div>
                                        <br>
                                        <!-- <button class="btn btn-danger btn-lg btn-block">Register</button> -->
                                           									

             
                                        


                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>

<script>
    window.document.onkeydown = function(e) {
  if (!e) {
    e = event;
  }
  if (e.keyCode == 27) {
    lightbox_close();
  }
}

function lightbox_open() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  window.scrollTo(0, 0);
  document.getElementById('light').style.display = 'block';
  document.getElementById('fade').style.display = 'block';
  lightBoxVideo.play();
}

function lightbox_close() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  document.getElementById('light').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
  lightBoxVideo.pause();
}
</script>