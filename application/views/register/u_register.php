
        <link rel="stylesheet" type="text/css" href=" <?= base_url('vendors/styles/core.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('vendors/styles/icon-font.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/datatables/css/dataTables.bootstrap4.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/datatables/css/responsive.bootstrap4.min.css') ?>">
        <!--<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/select2/dist/css/select2.min.css') ?>">-->
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/bootstrap/bootstrap.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('vendors/styles/style.css') ?>">
        <script src="<?= base_url('src/scripts/jquery.min.js') ?>"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/sweetalert2/sweetalert2.css') ?>">
        <script src="<?= base_url('src/plugins/sweetalert2/sweetalert2.all.js') ?>"></script>
        <script src="<?= base_url('src/plugins/sweetalert2/sweet-alert.init.js') ?>"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Registration</h4>
                    </div>
                   
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <form action="<?= base_url("auth/register_user"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">

                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input class="form-control" value="" name="u_name" > 
                                    </div>
                                </div>
                                <input type="hidden" name="hid" value="1" id="hid">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input class="form-control"  value="" name="u_country"  > 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Website Name</label>
                                        <input class="form-control" value="" name="web_name" > 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control"  value="" name="u_email"  > 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone / Contact</label>
                                        <input class="form-control"  value="" name="u_contact" > 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control"  required="" name="u_pass" > 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea  name="u_address" rows="2" class="form-control">  </textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">  
                                <label>Image:</label>
                                 <img src="<?= base_url('media/placeholder.png'); ?>" class="avatar-photo"/>
                             
                                <input type="file" name="file" onchange="displayImg(this)"    placeholder="Image">
                                <input type="hidden" name="hiddenimg"  value="" >
                            </div>
                        </div>
                    </div>



                    <div class="row  ">
                        <div class="col-md-12 ">
                            <button type="submit" onclick="register()" class="btn btn-primary  btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Submit</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script>  
  function displayImg(obj) {


if (obj.files && obj.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $(obj).closest('div').find('img').attr('src', e.target.result);
    };

    reader.readAsDataURL(obj.files[0]);
}

}</script>
 
