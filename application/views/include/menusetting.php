
<div class="right-sidebar">
    <div class="sidebar-title">
        <h3 class="weight-600 font-16 text-blue">
            Layout Settings
            <span class="btn-block font-weight-400 font-12">User Interface Settings</span>
        </h3>
        <div class="close-sidebar" data-toggle="right-sidebar-close">
            <i class="icon-copy ion-close-round"></i>
        </div>
    </div>
    <div class="right-sidebar-body customscroll">
        <div class="right-sidebar-body-content">
            <h4 class="weight-600 font-18 pb-10">Header Background</h4>
            <div class="sidebar-btn-group pb-30 mb-10">
                <a href="javascript:void(0);" class="btn btn-outline-primary header-white active headerr" data-value="header-white" onclick="changeSettings();">White</a>
                <a href="javascript:void(0);" class="btn btn-outline-primary header-dark headerr" data-value="header-dark" onclick="changeSettings();">Dark</a>
            </div>

            <h4 class="weight-600 font-18 pb-10">Sidebar Background</h4>
            <div class="sidebar-btn-group pb-30 mb-10">
                <a href="javascript:void(0);" class="btn btn-outline-primary sidebar-light sidebarr" data-value="sidebar-light" onclick="changeSettings();">White</a>
                <a href="javascript:void(0);" class="btn btn-outline-primary sidebar-dark sidebarr" data-value="sidebar-dark" onclick="changeSettings();">Dark</a>
            </div>

            <h4 class="weight-600 font-18 pb-10">Menu Dropdown Icon</h4>
            <div class="sidebar-radio-group pb-10 mb-10">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebaricon-1" name="menu-dropdown-icon" data-value="icon-style-1" onclick="changeSettings();" class="custom-control-input menu" value="icon-style-1" checked="">
                    <label class="custom-control-label" for="sidebaricon-1"><i class="fa fa-angle-down"></i></label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebaricon-2" name="menu-dropdown-icon" data-value="icon-style-2" onclick="changeSettings();" class="custom-control-input menu" value="icon-style-2">
                    <label class="custom-control-label" for="sidebaricon-2"><i class="ion-plus-round"></i></label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebaricon-3" name="menu-dropdown-icon" data-value="icon-style-3" onclick="changeSettings();" class="custom-control-input menu" value="icon-style-3">
                    <label class="custom-control-label" for="sidebaricon-3"><i class="fa fa-angle-double-right"></i></label>
                </div>
            </div>

            <h4 class="weight-600 font-18 pb-10">Menu List Icon</h4>
            <div class="sidebar-radio-group pb-30 mb-10">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebariconlist-1" name="menu-list-icon" data-value="icon-list-style-1" onclick="changeSettings();" class="custom-control-input submenu" value="icon-list-style-1" checked="">
                    <label class="custom-control-label" for="sidebariconlist-1"><i class="ion-minus-round"></i></label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebariconlist-2" name="menu-list-icon" data-value="icon-list-style-2" onclick="changeSettings();" class="custom-control-input submenu" value="icon-list-style-2">
                    <label class="custom-control-label" for="sidebariconlist-2"><i class="fa fa-circle-o" aria-hidden="true"></i></label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebariconlist-3" name="menu-list-icon" data-value="icon-list-style-3" onclick="changeSettings();" class="custom-control-input submenu" value="icon-list-style-3">
                    <label class="custom-control-label" for="sidebariconlist-3"><i class="dw dw-check"></i></label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebariconlist-4" name="menu-list-icon" data-value="icon-list-style-4" onclick="changeSettings();" class="custom-control-input submenu" value="icon-list-style-4" checked="">
                    <label class="custom-control-label" for="sidebariconlist-4"><i class="icon-copy dw dw-next-2"></i></label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebariconlist-5" name="menu-list-icon" data-value="icon-list-style-5" onclick="changeSettings();" class="custom-control-input submenu" value="icon-list-style-5">
                    <label class="custom-control-label" for="sidebariconlist-5"><i class="dw dw-fast-forward-1"></i></label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sidebariconlist-6" name="menu-list-icon" data-value="icon-list-style-6" onclick="changeSettings();" class="custom-control-input submenu" value="icon-list-style-6">
                    <label class="custom-control-label" for="sidebariconlist-6"><i class="dw dw-next"></i></label>
                </div>
            </div>

             
        </div>
    </div>
</div>
<script type="text/javascript">
    function changeSettings() {
        var header = '';
        $('.headerr').each(function () {
            if ($(this).hasClass('active')) {
                header = $(this).val();
            }
        });
        var sidebar = '';
        $('.sidebarr').each(function () {
            if ($(this).hasClass('active')) {
                sidebar = $(this).val();
            }
        });
        var menu = '';
        $('.menu').each(function () {
            if ($(this).prop('checked') == true) {
                menu = $(this).val();
            }
        });
        var submenu = '';
        $('.submenu').each(function () {
            if ($(this).prop('checked') == true) {
                submenu = $(this).val();
            }
        });
        $.ajax({
            type: 'post',
            data: {'header': header, 'sidebar': sidebar, 'menu': menu, 'submenu': submenu},
            url: "<?= base_url('admin/layout_settings'); ?>",
            success: function ()
            {

            }

        });
    }
</script>