<style>
    .brand-logo a img {
        max-width: 275px !important;
    }
</style>

<?php $method = strtolower($this->uri->segment(2)) ?>
<?php
$layoutsettings = $this->Model_p->get_globalSingWithCond('layout_settings', ['layset_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);
if (!empty($layoutsettings)) {
    $sidebar = $layoutsettings->sidebar;
    $header = $layoutsettings->header;
    $sidebar_icon = $layoutsettings->sidebar_icon;
    $submenu_icon = $layoutsettings->submenu_icon;
}

?>


<div class="left-side-bar ">
    <div class="brand-logo ">
        <a href="#" class="" style="height: 100%;   padding: 0px !important; margin: 0px !important;">
            <img style="height: 100%; width: 100%;" src="<?= base_url('media/poslogo.jpeg') ?>" alt="">
        </a>
        <div class="close-sidebar" data-toggle="left-sidebar-close">
            <i class="ion-close-round"></i>
        </div>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu <?php if (!empty($layoutsettings)) { ?> <?= $sidebar_icon; ?> <?= $submenu_icon; ?> <?php } ?>">
            <ul id="accordion-menu">

                <?php if ($this->session->userdata('user')['is_company'] == 0) { ?>

                    <li class="dropdown">
                        <a href="<?php echo base_url('Welcome') ?>" class="dropdown-toggle no-arrow">
                            <span class="micon dw dw-computer-1"></span><span class="mtext">Dashboard</span>
                        </a>
                    </li>


                    <li class="dropdown">
                        <a href="<?= base_url('Stock/stock') ?>" class="<?php if ($method == 'stock') { ?> active <?php } ?> dropdown-toggle no-arrow">
                            <span class="micon dw dw-crane"></span><span class="mtext">Stock View</span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle">
                            <span class="micon dw dw-library"></span><span class="mtext">Products</span>
                        </a>
                        <ul class="submenu">
                            <li><a class="<?php if ($method == 'add_product') { ?> active <?php } ?>" href="<?php echo base_url('Products/add_product'); ?>"><span class="mtext">Add New</span></a></li>
                            <li><a class="<?php if ($method == 'products') { ?> active <?php } ?>" href="<?php echo base_url('Products/products'); ?>"><span class="mtext">All Products</span></a></li>
                            <li><a class="<?php if ($method == 'prd_settings' || $method == 'categories' || $method == 'types') { ?> active <?php } ?>" href="<?php echo base_url('Products/prd_settings'); ?>"><span class="mtext">Set Category / Type</span></a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle">
                            <span class="micon dw dw-calculator"></span><span class="mtext">Purchase</span>
                        </a>
                        <ul class="submenu">
                            <li><a class="<?php if ($method == 'purchase') { ?> active <?php } ?>" href="<?php echo base_url('Products/Purchase'); ?>"><span class="mtext">Add New</span></a></li>
                            <li><a class="<?php if ($method == 'purchase_voucer') { ?> active <?php } ?>" href="<?php echo base_url('Purchase/purchase_voucer'); ?>"><span class="mtext">All Purcahses</span></a></li>
                            <!--<li><a class="<?php if ($method == 'request') { ?> active <?php } ?>" href="<?php echo base_url('Purchase/request'); ?>"><span class="mtext">Request Quote</span></a></li>-->
                            <li><a class="<?php if ($method == 'purchase_stock') { ?> active <?php } ?>" href="<?php echo base_url('Purchase/purchase_stock'); ?>"><span class="mtext">Stocked</span></a></li>
                            <li><a class="<?php if ($method == 'purchase_unstock') { ?> active <?php } ?>" href="<?php echo base_url('Purchase/purchase_unstock'); ?>"><span class="mtext">Unstocked</span></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle">
                            <span class="micon dw dw-shopping-cart"></span><span class="mtext">Sales</span>
                        </a>
                        <ul class="submenu">
                            <li><a class="<?php if ($method == 'sales') { ?> active <?php } ?>" href="<?php echo base_url('Products/sales_products'); ?>"><span class="mtext">Add New</span></a></li>
                            <!--<li><a href="#"><span class="mtext">Terminal</span></a></li>-->
                            <li><a class="<?php if ($method == 'sales_voucer') { ?> active <?php } ?>" href="<?php echo base_url('sales/sales_voucer'); ?>"><span class="mtext">Sale Voucher</span></a></li>
                        </ul>
                    </li>
                    <?php if ($this->session->userdata('user')['role'] == 2) { ?>

                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle">
                                <span class="micon dw dw-credit-card"></i></span><span class="mtext">Charts of Account</span>
                            </a>
                            <ul class="submenu">
                                <li><a class="dropdown" href="<?php echo base_url('products/expense'); ?>"><span>Expenses</span></a></li>
                                <!--<li><a href="#"><span class="mtext">Terminal</span></a></li>-->
                                <li><a class="dropdown" href="<?php echo base_url('products/expense_list'); ?>"><span>Expenses List</span></a></li>

                                <li><a class="dropdown" href="<?php echo base_url('products/salary'); ?>"><span>Salary</span></a></li>

                                <li><a class="dropdown" href="<?php echo base_url('products/exp_report'); ?>"><span>Expense Report</span></a></li>



                            </ul>
                        </li>
                    <?php } ?>

                    <!--       <li class="dropdown">
                    <a href="#" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-computer-1"></span><span class="mtext">Reports</span>
                    </a>
                </li> -->
                    <li class="dropdown">
                        <a href="<?php echo base_url('Products/sales_report'); ?>" class="dropdown-toggle no-arrow">
                            <span class="micon fa fa-file"></span><span class="mtext">Reports</span>
                        </a>

                    </li>

                <?php } ?>



                <!--                <li class="dropdown ">
                    <a href="javascript:void(0);" class="dropdown-toggle" <?php
                                                                            if ($method == 'update_user') {
                                                                                if ($user->user_type == 1) {
                                                                                    $method = 'administrators';
                                                                                } elseif ($user->user_type == 2) {
                                                                                    $method = 'vendors';
                                                                                } else {
                                                                                    $method = 'customers';
                                                                                }
                                                                            }
                                                                            ?>>
                        <span class="micon dw dw-user-11"></span><span class="mtext">Administrators</span>
                    </a>

                    <ul class="submenu">

                        <li><a class="<?php if ($method == 'vendors') { ?> active <?php } ?>" href="<?php echo base_url('Admin/vendors'); ?>"><span class="mtext">Vendors </span></a></li>
<li><a class="<?php if ($method == 'customers') { ?> active <?php } ?>" href="<?php echo base_url('Admin/customers'); ?>"><span class="mtext">Customers</span></a></li>
                    </ul>



                </li>-->
                <?php if ($this->session->userdata('user')['is_company'] == 1) { ?>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle">
                            <span class="micon dw dw-user-13"></span><span class="mtext">Authorization</span>
                        </a>
                        <ul class="submenu">
                            <?php if ($this->session->userdata('user')['role'] == 2) { ?> <li><a class="<?php if ($method == 'add_user') { ?> active <?php } ?>" href="<?php echo base_url('Admin/add_user'); ?>"><span class="mtext">Add User</span></a></li> <?php } ?>
                            <li><a class="<?php if ($method == 'administrators') { ?> active <?php } ?>" href="<?php echo base_url('Admin/administrators'); ?>"><span class="mtext">All Users</span></a></li>
                            <li><a class="<?php if ($method == 'roles') { ?> active <?php } ?>" href="<?= base_url('admin/roles') ?>"><span class="mtext">Roles</span></a></li>
                            <!--   <li><a href="#"><span class="mtext">Permissions</span></a></li> -->
                        </ul>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?= base_url('Settings/logo_settings') ?>" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-settings2"></span><span class="mtext">Settings</span>
                    </a>


                </li>








            </ul>
        </div>
    </div>
</div>
<div class="mobile-menu-overlay"></div>
<div class="main-container">