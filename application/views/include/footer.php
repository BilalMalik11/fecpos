<script src="<?php echo base_url('vendors/scripts/core.js') ?>"></script>
<script src="<?php echo base_url('vendors/scripts/script.js') ?>"></script>
<script src="<?php echo base_url('vendors/scripts/process.js') ?>"></script>
<script src="<?php echo base_url('vendors/scripts/layout-settings.js') ?>"></script>

<script src="<?php echo base_url('src/plugins/datatables/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('src/plugins/datatables/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src=" <?php echo base_url('src/plugins/datatables/js/dataTables.responsive.min.js') ?> "></script>
<script src="<?php echo base_url('src/plugins/datatables/js/responsive.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('src/plugins/bootstrap/bootstrap.min.js') ?>"></script>

<!-- buttons for Export datatable -->
<script src="<?php echo base_url('src/plugins/datatables/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('src/plugins/datatables/js/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('src/plugins/datatables/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('src/plugins/datatables/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('src/plugins/datatables/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('src/plugins/datatables/js/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('src/plugins/datatables/js/vfs_fonts.js'); ?>"></script>
<!-- Datatable Setting js -->
<script src="<?php echo base_url('vendors/scripts/datatable-setting.js'); ?>"></script>

<!-- <script src=" <?php echo base_url('vendors/scripts/dashboard.js') ?> "></script> -->
<script src="<?= base_url('src/plugins/switchery/switchery.min.js') ?>"></script>

<script src="<?= base_url('src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') ?>"></script>

<script src="<?= base_url('src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js') ?>"></script>
<script src="<?= base_url('vendors/scripts/advanced-components.js') ?>"></script>
<script src="<?= base_url('src/plugins/slick/slick.min.js'); ?>"></script>
<script type="text/javascript">
    function displayImg(obj) {


        if (obj.files && obj.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(obj).closest('div').find('img').attr('src', e.target.result);
            };

            reader.readAsDataURL(obj.files[0]);
        }

    }
</script>