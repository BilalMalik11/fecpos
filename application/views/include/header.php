<?php $userinfo = $this->Model_p->getsingleuser($this->session->userdata('user')['user_id']); ?>
<?php
$layoutsettings = $this->Model_p->get_globalSingWithCond('layout_settings', ['layset_id' => 1], ['instant_id' => $this->session->userdata('user')['inst_id']]);
if (!empty($layoutsettings)) {
    $sidebar = $layoutsettings->sidebar;
    $header = $layoutsettings->header;
    $sidebar_icon = $layoutsettings->sidebar_icon;
    $submenu_icon  = $layoutsettings->submenu_icon;
}
?>
<link rel="stylesheet" type="text/css" href=" <?= base_url('vendors/styles/core.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('vendors/styles/icon-font.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/datatables/css/dataTables.bootstrap4.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/datatables/css/responsive.bootstrap4.min.css') ?>">
<!--<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/select2/dist/css/select2.min.css') ?>">-->
<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/bootstrap/bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('vendors/styles/style.css') ?>">
<script src="<?= base_url('src/scripts/jquery.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/sweetalert2/sweetalert2.css') ?>">
<script src="<?= base_url('src/plugins/sweetalert2/sweetalert2.all.js') ?>"></script>
<script src="<?= base_url('src/plugins/sweetalert2/sweet-alert.init.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url('src/plugins/slick/slick.css'); ?>">

</head>
<?php if (!empty($layoutsettings)) { ?>
<body class="<?= $sidebar; ?> <?= $header; ?> ">
<?php } ?>

<?php  if($this->uri->segment(2) != 'Products' &&  $this->uri->segment(2)  != 'sales_products'  ){?>


    <div class="header">
        <div class="header-left">
            <div class="menu-icon dw dw-menu"></div>
            <div class=" dw dw-home ml-3" style="height: 30px;width: 50px"><a class="text-info m-1" href="<?= base_url('Welcome/index') ?>">Home</a></div>
        </div>

        <div class="header-right ">

            <div class="user-info-dropdown">
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                        <span class="user-icon">
                            <img src="<?php if (!empty($userinfo->u_image)) {
                                            echo base_url('media/' . $userinfo->u_image);
                                        } else { ?><?= base_url('media/placeholder.png'); ?><?php } ?>" alt="">
                        </span>
                        <!-- <span class="user-name"><?= $userinfo->name; ?></span> -->
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                        <a class="dropdown-item" href="#"><i class="dw dw-pen"></i> <?php if ($this->session->userdata('user')['role'] == 1) {
                                                                                        echo 'Sales Person';
                                                                                    } elseif ($this->session->userdata('user')['role'] == 2) {
                                                                                        echo 'Admin';
                                                                                    } ?> </a>
                        <a class="dropdown-item" href="<?php echo base_url('Admin/single_user'); ?>"><i class="dw dw-user1"></i> Profile</a>
                        <a class="dropdown-item" href=" <?php echo base_url('Settings/logo_settings'); ?>"><i class="dw dw-settings2"></i> Setting</a>

                        <a class="dropdown-item" href="<?php echo base_url('Admin/logout'); ?>"><i class="dw dw-logout"></i> Log Out</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php } ?>