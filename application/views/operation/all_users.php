<?php
$title = '';
if (strtolower($this->uri->segment(2)) == 'customers') {
    $title = 'Customers';
} else if (strtolower($this->uri->segment(2)) == 'vendors') {

    $title = 'Vendors';
} else {
    $title = 'Administrators';
}
?>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="title">
                        <h4>All <?= $title; ?></h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">HR MANAGMENT</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $title; ?></li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30" style="padding: 0px">

            <div class="pt-20 pb-20">
                <table class="data-table table stripe hover nowrap" >
                    <thead>

                        <tr>
                            <th>S</th>
                            <th>Name</th>
                            
                                <th>Role</th>
                           
                            <th>Cnic</th>
                            <th>Image</th>
                            <th class="datatable-nosort text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $s = 1;
                        foreach ($data as $single_rec) {
                            ?>



                            <tr>    
                                <?php $role = '';
                                if($single_rec->role == 1){
$role = 'Sales Person';

                                }elseif ($single_rec->role == 2) {
                                     $role = 'Admin';
                                }
                                 ?>
                                
                                <td class="table-plus"><?php echo $s++; ?></td>
                                <td><?php echo $single_rec->name; ?></td>
                                 
                                    <td><?php echo $role ?></td>
                            
                                <td><?php echo $single_rec->cnic; ?></td>
                                <td><img style= "width:50px; height:50px" src="<?php
                                    if (empty($single_rec->u_image)) {
                                        echo base_url('media/placeholder.png');
                                    } else {
                                        ?><?php echo base_url('media/') . $single_rec->u_image; ?><?php } ?>" ></td>



                                <td>
                                    <?php if( $this->session->userdata('user')['role'] == 2){ ?>
                                    <div class="dropdown text-right">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="<?php echo base_url() . 'Admin/update_user/' . $single_rec->uid; ?>"  ><i class="dw dw-eye"></i> View</a>


                                            <a class="dropdown-item" href="<?php echo base_url() . 'Admin/delete_user/' . $single_rec->uid; ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                        </div>


                                    </div>
                                <?php }else { ?>
                                   <div class="dropdown text-right">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="fa fa-lock"></i>
                                        </a>
                                    


                                    </div>
                               <?php } ?>



                                </td>
                            </tr>

                        <?php } ?>

                    </tbody>
                </table>

            </div>


        </div>


    </div>
</div>
