<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Product Settings</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Settings</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12 mb-30 btn" onclick="window.location.href = '<?= base_url('Products/categories'); ?>'">
                <div class="card card-box text-center ">
                    <div class="card-body">
                        <h1 class="card-title">Categories</h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 mb-30 btn" onclick="window.location.href = '<?= base_url('Products/types'); ?>'">
                <div class="card card-box text-center">
                    <div class="card-body">
                        <h1 class="card-title">Types</h1>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
