<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Add New Product</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ADD NEW</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <form action="<?= base_url("products/new_Product"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">


                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" required="" name="nam" class="form-control" placeholder="Name">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label>SKU/Barcode:</label>
                                <input type="text" name="barcode" class="form-control" placeholder="Leave it empty for autogeneration">

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Type:</label>
                                <select class=" form-control " required="" name="type">
                                    <option value="">Select a Type</option>
                                    <?php foreach ($type as $t) { ?>
                                        <option value="<?= $t->type_id ?>"> <?= $t->type_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Category:</label>
                                <select class="form-control " name="category">
                                    <option value="">Select an option</option>
                                    <?php foreach ($cate as $c) { ?>
                                        <option value="<?= $c->category_id ?>"> <?= $c->category_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tags:</label>
                                <select name="tags[]" class="form-control select2new" multiple="">
                                    <?php foreach ($tags as $c) { ?>
                                        <option value="<?= $c->tag_id ?>"> <?= $c->tag_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Image:</label>
                                <input type="file" name="file[]" required="" multiple onchange="displayImages(this)">
                                <div class="imagesdisplay" style=" width: 100%"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Purchase Price:</label>
                                        <input type="text" name="purchase_price" class="form-control">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sale Price:</label>
                                        <input type="text" required="" name="saleprice" class="form-control">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Discounted Price:</label>
                                        <input type="text" name="discount_price" class="form-control">

                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label>Color:</label>
                                        <input type="color" name="color" class="form-control">

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row text-right">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Add Product</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>


<script>
    function displayImages(input) {
        $(input).closest('.form-group').find('.imagesdisplay').html('');
        if (input.files) {
            var filesAmount = input.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    var html = '<img style="height: 140px;" src="' + event.target.result + '"  >';
                    $(input).closest('.form-group').find('.imagesdisplay').append(html);
                }
                reader.readAsDataURL(input.files[i]);

            }
        }
    }
</script>