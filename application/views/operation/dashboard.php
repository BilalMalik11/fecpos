<?php
 $toal_product= $this->db->select('*,count(product_id)as ttl_prd')->where('instant_id', 
 $this->session->userdata('user')['inst_id'])->get('products')->row();
 $sale = $this->db->select('* , sum(sale_total) as ttlsale')->where('sale_date' , date('Y-m-d'))->get('sale')->row();
$stk = 0;
$soldprd = 0;
 foreach ($pro as $key => $prroo) {
   
  $stk+=($prroo['stock']); 
  
 }

 ?>

<style type="text/css">


.resize{

padding: 0px !important;
width: 230px !important;

 

}

.btnsize{
  height: 80px;
  border-radius: 0px !important 
}


</style>
  <link rel="stylesheet" href="<?= base_url('plugins/fontawesome-free/css/all.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('dist/css/adminlte.min.css')?>">
 
  <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
              <div class="title col-md-3 mb-2 btn-group  " >
                <button class="btn btn-light  " name="btn1" onclick="daily(this)"> Daily</button>
                 <button class="btn btn-light " name="btn2" onclick="monthly(this)"> Monthly</button>
                  <button class="btn btn-light" name="btn3" onclick="yearly(this)"> Yearly</button>
                 </div>
        </div>
        <div class="row">
             
        
          <div class="col-12 col-sm-6 col-md-3 mainresize">

            <div class="info-box mb-3 resize">
            <!--   <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-calendar" aria-hidden="true"></i></span> -->
           <button class="info-box-icon btnsize  elevation-1 border-0" data-toggle="modal" data-target="#exampleModalA" style="background:#00bfec  "><i class="fas fa-calendar text-white"></i></button>
              <div class="info-box-content">
                <span class="info-box-text ml-3 mt-3 ">Total Products</span>
                <span class="info-box-number ml-5 "><?= $toal_product->ttl_prd?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

        
          <div class="col-12 col-sm-6 col-md-3 mainresize">
            <div class="info-box mb-3 resize">
             <!--  <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-shopping-bag" aria-hidden="true"></i></span> -->
               <button style="background-color: #00a757" class="info-box-icon btnsize  elevation-1 border-0" data-toggle="modal" data-target="#exampleModalB"><i class="fas fa-shopping-bag text-white"></i></button>
              <div class="info-box-content">
                <span class="info-box-text  ml-3 mt-3 ">Total Stock</span>
                <span class="info-box-number  ml-5"><?= $stk?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3 mainresize">
            <div class="info-box mb-3 resize">
             <!--  <span class="info-box-icon bg-success elevation-1"><i class="fas fa fa-line-chart"></i></span> -->
          <button  class="info-box-icon btnsize bg-success elevation-1 border-0" style="background: #f19b10 !important; padding: 0px !important; margin: 0px !important; " data-toggle="modal" data-target="#exampleModalC"><i class="fas fa fa-line-chart text-white "></i></button>
              <div class="info-box-content">
                <span class="info-box-text  ml-4 mt-3 ">Sales</span>
                <span class="info-box-number  ml-5"><?= $sale->ttlsale?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3 mainresize">
            <div class="info-box resize">
              <!-- <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cart-arrow-down"></i></span> -->
          <button class="info-box-icon btnsize  elevation-1 border-0" style="background-color: #ea1f61" data-toggle="modal" data-target="#exampleModalD"><i class="fas fa-cart-arrow-down text-white"></i></button>

              <div class="info-box-content">
                <span class="info-box-text  ml-3 mt-3 ">Product Sold</span>
                <span class="info-box-number  ml-5">
                  <?=  $stk?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <!--    <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                     
                    </div>
                  </div>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-8">
                    <p class="text-center">
                    </p>
                     <div id="daily" style="height: 300px; display: none; "></div>
                     <div id="monthly" style="height: 300px;  "></div>
                     <div id="yearly" style="height: 300px; display: none; "></div>
                  </div>
                  <!-- /.col -->
                    <div id="container_pie" class="col-md-4 p-0 m-0" style="height: 290px; "></div>
                </div>
                <!-- /.row -->
              </div>
              <!-- ./card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
         <div class="modal fade" id="exampleModalA" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Total Products</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"><h1 >Total Products :  <span class="text-primary"><?= $toal_product->ttl_prd?></span></h1></div>
    </div>
  </div>
</div>
 <div class="modal fade" id="exampleModalB" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Total Stock</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
 <div class="modal-body">
   <h1 >In Stock :  <span class="text-primary"><?=  $stk?></span></h1>
 </div>
    </div>
  </div>
</div>
 <div class="modal fade" id="exampleModalC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

  <div class="modal-body">
   <h1 >Today Sale:  <span class="text-primary"><?=   $sale->ttlsale?></span></h1>
 </div>
    </div>
  </div>
</div>
 <div class="modal fade" id="exampleModalD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Sold</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> <div class="modal-body">
   <h1 >Product Sold :  <span class="text-primary"><?=  $stk?></span></h1>
 </div>
    </div>
  </div>
</div>
        <!-- Main row -->
      </div><!--/. container-fluid -->
    </section>

    <script src=" <?= base_url('plugins/chart.js/Chart.min.js')?>"></script>
   <script src=" <?= base_url('plugins/chart.js/highcharts.js')?>"></script>
    
<!-- PAGE SCRIPTS -->
<script src="<?= base_url('dist/js/pages/dashboard2.js')?>"></script>
<script type="text/javascript">

Highcharts.chart('monthly', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Monthly Sales'
    },
    credits: {
      enabled: false
    },
    xAxis: [{
        categories: ['1', '2', '3', '4', '5', '6',
            '7', '8', '9','10','11','12'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}%',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },

       
    }],

    tooltip: {
        shared: true
    },
    legend: {
      layout: 'vertical',
        y: 100,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            'rgba(255,255,255,0.25)'
    },
    
    series: [{
        type: 'column',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
        

    }]
});

Highcharts.chart('daily', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Daily Sales'
    },
    credits: {
      enabled: false
    },
    xAxis: [{
        categories: ['1', '2', '3', '4', '5', '6',
            '7', '8', '9', '10','11','12','13' , '14' , '15' , '16' , '17' , '18' , '19','20','21','22','23','24','25','26','27','28','29','30'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}%',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },

       
    }],

    tooltip: {
        shared: true
    },
    legend: {
      layout: 'vertical',
        y: 100,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            'rgba(255,255,255,0.25)'
    },
    series: [{
        type: 'column',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 129.2, 144.0 ],
        

    }]
});

Highcharts.chart('yearly', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Yearly Sales'
    },
    credits: {
      enabled: false
    },
    xAxis: [{
        categories: ['2016', '2017', '2018', '2019', '2020', '2021'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}%',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },

       
    }],

    tooltip: {
        shared: true
    },
    legend: {
      layout: 'vertical',
        y: 100,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            'rgba(255,255,255,0.25)'
    },
    series: [{
        type: 'column',
        data: [ 106.4, 144.0, 176.0, 148.5, 216.4, 194.1],
        

    }]
});
 Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

// Build the chart
Highcharts.chart('container_pie', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Top 5 Products'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
     credits: {
      enabled: false
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b> {point.y:.f} %',
                 distance: -50,
                  filter: {
            property: 'percentage',
            operator: '>',
            value: 4
          }
            }
        }
    },
    colors: ['#B22222', '#3366CC', '#FF8C00', '#109618', '#008B8B', ' #C47451', '#FFF263', '#6AF9C4'],
    series: [{
        name: '',
        data: [
            { name: '', y: 1 },
            { name: ' ', y: 2 },
            { name: '', y: 3 },
            { name: '', y: 4 },
            { name: '', y: 5 },
        ]
    }]
});

function daily(para)
{          
  $('#daily').css('display', 'block ');
  $('#monthly').css('display' , 'none');
  $('#yearly').css('display' , 'none');
} 

function monthly(para)
{
  $('#daily').css('display', 'none ');
  $('#monthly').css('display' , 'block');
  $('#yearly').css('display' , 'none');
} 

function yearly(para)
{
    $('#daily').css('display', 'none ');
  $('#monthly').css('display' , 'none');
  $('#yearly').css('display' , 'block'); 
} 

</script>