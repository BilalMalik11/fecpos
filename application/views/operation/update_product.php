
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Update Product</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Update Product</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <form action="<?= base_url("products/update_Product"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">


                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" value="<?= $Pro['product']->title ?>" name="nam" class="form-control" placeholder="Name">
                            </div>
                        </div>

                        <input type="hidden" name="hid" value="<?= $Pro['product']->product_id ?>">

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label>SKU/Barcode:</label>
                                <input type="text" name="barcode" value="<?= $Pro['product']->barcode ?>"  class="form-control" placeholder="Barcode">

                            </div>
                        </div>






                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Type:</label>
                                <select class=" form-control select2" name="type"> 


                                    <option>Select a Type</option>
                                    <?php foreach ($type as $t) { ?>


                                        <option <?php if ($t->type_id == $Pro['product']->type_id) { ?> selected <?php } ?> value="<?= $t->type_id ?>"> <?= $t->type_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Category:</label>
                                <select class=" form-control select2" name="category" >
                                    <option>Select an option</option>
                                    <?php foreach ($cate as $c) { ?>


                                        <option <?php if ($c->category_id == $Pro['product']->category_id) { ?> selected <?php } ?> value="<?= $c->category_id ?>"> <?= $c->category_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tags:</label>
                                <select name="tags[]" class="form-control select2new" multiple="">
                                    <?php foreach ($tags as $c) { ?>
                                        <option <?php
                                        if (!empty($prd_tags)) {
                                            foreach ($prd_tags as $tagdb) {
                                                ?><?php if ($tagdb->tag_id == $c->tag_id) { ?> selected="" <?php } ?><?php
                                                }
                                            }
                                            ?> value="<?= $c->tag_id ?>"> <?= $c->tag_name ?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Images:</label>
                                <input type="file" name="file[]" multiple onchange="displayImages(this)">
                                <div style="col-md-12">
                                    <div style="row">
                                        <?php
                                        if (!empty($Pro['proimage'])) {

                                            foreach ($Pro['proimage'] as $img) {
                                                ?>
                                                <div class="col-md-3 exsist" style="float: left;">
                                                    <img style="width: 100%;" src="<?= base_url("media/products/" . $img->img_url) ?>"  >
                                                    <input type="hidden" name="oldimgs[]" value="<?= $img->img_url; ?>" />
                                                    <button type="button" class="btn btn-danger btn-block" onclick="$(this).closest('.exsist').remove();"><i class="fa fa-trash"></i></button>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="imagesdisplay" style=" width: 100%"></div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sale Price:</label>
                                        <input type="text" value="<?= $Pro['product']->sale_price ?>" name="saleprice" class="form-control">

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Purchase Price:</label>
                                        <input type="text" value="<?= $Pro['product']->purchase_price ?>" name="purchase_price" class="form-control">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Discounted Price:</label>
                                        <input type="text" name="discount_price" value="<?= $Pro['product']->discount_price ?>" class="form-control">

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Discount Tags:</label>
                                        <input type="text" value="<?= $Pro['product']->discount_tags ?>" name="discount_tag" class="form-control">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label>Color:</label>
                                        <input type="color" name="color" value="<?= $Pro['product']->color ?>" class="form-control">

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row text-right">
                        <div class="col-md-2 ">
                            <button type="submit" class="btn btn-primary btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Update Information</button>
                        </div>
                    </div>
                </div>


        </div>

        </form>
    </div>
</div>


<script>
    function displayImages(input) {
        $(input).closest('.form-group').find('.imagesdisplay').html('');
        if (input.files) {
            var filesAmount = input.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function (event) {
                    var html = '<img style="height: 140px;" src="' + event.target.result + '"  >';
                    $(input).closest('.form-group').find('.imagesdisplay').append(html);
                }
                reader.readAsDataURL(input.files[i]);

            }
        }
    }

</script>