 

<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>My Profile</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Administration</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <form action="<?= base_url("Admin/update_single_user"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">

                    <div class="row">
                    	<div class="col-md-9">
                    	<div class="row">
                    		  <div class="col-md-6">
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" name="nam" value="<?php echo $data->name; ?>" class="form-control" placeholder="Name">
                            </div>
                     		   </div>
  									      <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact:</label>
                                        <input type="text" name="contct" value="<?php echo $data->contact; ?>"  class="form-control" placeholder="Contact">
                                    </div>
                                </div>
                              		   <input type="hidden" name="hid" value="<?php echo $data->uid; ?>">
                    	</div>
                    	      
                            <div class="row">
                              <div class="col-md-12">
                            <div class="form-group">
                                <label>Address:</label>
                                <textarea class="form-control" name="adres" placeholder="Enter Address..."><?php echo $data->address; ?></textarea>
                            </div>
                            </div>
                        </div>

                    	</div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Image:</label>
                                
                                
                                <?php if(!empty($data->u_image)){ ?>
                                <img src="<?=   base_url('media/').$data->u_image ?>"  >
								 <?php } else { ?>
        <img src="<?= base_url('media/placeholder.png'); ?>"  />
								 <?php } ?>
						<input type="file" name="file" onchange="displayImg(this)"  placeholder="Image">
						<input type="hidden" value="<?=$data->u_image?>" name="hiddenimg" >
                            </div>
                        </div>
                    </div>
                 
 

                    <div class="row text-right">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Update Information</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
 