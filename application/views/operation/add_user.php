
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Add New User</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">HR MANAGMENT</a></li>
                            <li class="breadcrumb-item active" aria-current="page">ADD NEW</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <form action="<?= base_url("Admin/add_new_user"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">

                    <div class="row">
 
                        <div class="col-md-9" >
                         <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" name="nam" class="form-control" placeholder="Name">
                            </div>
                        </div>
 
                        <div class="col-md-6 role_div">
                        <div class="form-group">
                                <label>Role:</label>
                                <select name="role" class="form-control">
                                        <option value="0">Select Role</option>
                                    <?php foreach ($data as $key => $value) { ?>
                                        <option value="<?= $value->role_id ?>"> <?= $value->role_name ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

               </div>


                           <div class="col-md-12">
                            <div class="form-group">
                                <label>Address:</label>
                                <textarea class="form-control" name="adres" placeholder="Enter Address..."></textarea>
                            </div>
                        </div>
                
                    </div>
                         <div class="col-md-3">
                            <div class="form-group">
                                <label>Image:</label>
                                <img src="<?= base_url('media/placeholder.png'); ?>" class="avatar-photo"/>
                                <input type="file" name="file" onchange="displayImg(this)"  placeholder="Image">
                            </div>
                        </div>
                    
  </div>
                     
                  
                         
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cnic:</label>
                                        <input type="text" name="cni" class="form-control" placeholder="Cnic">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Contact:</label>
                                        <input type="text" name="contct" class="form-control" placeholder="Contact">
                                    </div>
                                </div>
                                       <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Salary:</label>
                                        <input type="text" name="salary" class="form-control" placeholder="salary">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email:</label>
                                        <input type="text" name="emal" autocomplete="off" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password:</label>
                                        <input type="Password" autocomplete="off" name="pass" class="form-control" placeholder="Password">
                                    </div>
                                </div>

                            </div>
                        
                 

                    <div class="row text-right">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Save Information</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
<script type="text/javascript">
    function displayHideRole(obj) {
        if ($(obj).val() > 1) {
            $('.role_div').hide();
            $('.type_div').removeClass('col-md-3');
            $('.type_div').addClass('col-md-6');
            $('.role_div').find('select').val(0);
        } else {
            $('.type_div').addClass('col-md-3');
            $('.type_div').removeClass('col-md-6');
            $('.role_div').show();
        }
    }

</script>