
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="title">
                        <h4>All  Categories</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item"><a href="#">Settings</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> Categories</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12">



                    <div class="header-right  float-right">




                        <div class="header-search">
                            <form>
                                <div class="form-group mb-0  mt-3">
                                    <button type="button" data-toggle="modal" data-target="#myModal" class=" btn btn-primary">Add New</button>

                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">

            <div class="pd-20">
                <table class="data-table table stripe hover nowrap">
                    <thead>

                        <tr>
                            <!--
                                    <th class="table-plus datatable-nosort">Name</th> -->

                            <th>S</th>
                            <th>Category</th>
                            <th>Short Description</th>

                            <th class="datatable-nosort text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $s = 1;
                        foreach ($data as $single_rec) {
                            # code...
                            ?>
                                                                                        <!-- <td><?php echo $single_rec->cat_id; ?></td> -->


                            <tr>
                                <td class="table-plus"><?php echo $s++; ?></td>
                                <td><?php echo $single_rec->category_name; ?></td>
                                <td><?php echo $single_rec->cat_shortdesc; ?></td>

                                <td> <?php if( $this->session->userdata('user')['role'] == 2){ ?>
                                    <div class="dropdown text-right">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal<?= $single_rec->category_id ?>"><i class="dw dw-eye"></i> View</a>


                                            <a class="dropdown-item" href="<?php echo base_url() . 'Products/delete_category/' . $single_rec->category_id; ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                        </div>


                                    </div>

<?php }else { ?>
                                   <div class="dropdown text-right">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="fa fa-lock"></i>
                                        </a>
                                    


                                    </div>
                               <?php } ?>

                                </td>
                            </tr>
                        <div class="modal fade" id="myModal<?= $single_rec->category_id ?>" tabindex="-1" role="dialog">
                            <div class="modal-dialog">
                                <form action="<?= base_url("Products/update_category"); ?>" method="post">




                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Category Update</h5>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Category Name:</label>
                                                        <input type="text" name="category" value="<?php echo $single_rec->category_name ?>" class="form-control">
                                                        <input type="hidden" name="rec_id" value="<?php echo $single_rec->category_id; ?>">

                                                                                                                                                                                    <!--  <input type="hidden" name="hid" value="<?php echo $single_rec->cat_id ?>"> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Short Description:</label>
                                                        <textarea class="form-control" name="desc"  ><?php echo $single_rec->cat_shortdesc ?></textarea>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary update-rec execute-loader">Update</button>
                                        </div>
                                    </div>
                                </form>


                            </div>
                        </div>
                    <?php } ?>

                    </tbody>
                </table>

            </div>


        </div>


    </div>
</div>
