<?php $title = strtolower($this->uri->segment(2)) ?>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="title">
                        <h4>All Roles</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Authoraization</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Roles</li>
                        </ol>
                    </nav>
                </div>

                <div class="col-md-6 col-sm-12">

                    <!-- <div class="pull-right">
                        <button type="button" data-toggle="modal" tooltip="Add Role" data-target="#myModal" class="btn btn-primary btn-sm scroll-click"> Add New</button>
                    </div> -->

                </div>


            </div>

        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30" style="padding: 0px">

            <div class="pt-20 pb-20">
                <table class="data-table table stripe hover nowrap">
                    <thead>

                        <tr>
                            <th>S</th>
                            <th>Title</th>
                            <th>Short Desc</th>

                        <!--     <th class="datatable-nosort text-right">Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $s = 1;
                        foreach ($data as $single_rec) {
                            # code...
                        ?>



                            <tr>
                                <td class="table-plus"><?php echo $s++; ?></td>


                                <td><?php echo $single_rec->role_name; ?></td>
                                <td><?php echo $single_rec->short_desc; ?></td>



<!-- 
                                <td>
                                    <?php if ($this->session->userdata('user')['role'] == 2) { ?>
                                        <div class="dropdown text-right">
                                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                <i class="dw dw-more"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#upModal<?= $single_rec->role_id ?>" data-toggle="modal"><i class="dw dw-eye"></i> View</a>


                                                <a class="dropdown-item" href="<?php echo base_url() . 'Admin/delete_Role/' . $single_rec->role_id; ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                            </div>


                                        </div>
                                    <?php } else { ?>
                                        <div class="dropdown text-right">
                                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                <i class="fa fa-lock"></i>
                                            </a>



                                        </div>
                                    <?php } ?>


                                </td> -->

                                <div class="modal fade" id="upModal<?= $single_rec->role_id ?>" tabindex="-1" role="dialog">
                                    <div class="modal-dialog">
                                        <form action="<?= base_url("Admin/update_role"); ?>" method="post">


                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-blue">Update Role</h5>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Title:</label>
                                                                <input type="text" name="title" value="<?= $single_rec->role_name ?>" class="form-control">

                                                                <input type="hidden" value="<?= $single_rec->role_id ?>" name="hid">

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Sort Description:</label>
                                                                <textarea class="form-control" name="desc" rows="10"><?= $single_rec->short_desc ?></textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary update-rec execute-loader">Update</button>
                                                </div>
                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </tr>

                        <?php } ?>

                    </tbody>
                </table>

            </div>


        </div>


    </div>
</div>