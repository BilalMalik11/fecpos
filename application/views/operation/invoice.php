    <style>
        #invoice-POS {
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
            padding: 2mm;
            margin: 0 auto;
            width: 44mm;
            background: #FFF;
        }

        ::selection {
            background: #f31544;
            color: #FFF;
        }

        ::moz-selection {
            background: #f31544;
            color: #FFF;
        }

        h1 {
            font-size: 1.5em;
            color: #222;
        }

        h2 {
            font-size: .9em;
        }

        h3 {
            font-size: 1.2em;
            font-weight: 300;
            line-height: 2em;
        }

        p {
            font-size: .7em;
            color: #666;
            line-height: 1.2em;
        }

        #top,
        #mid,
        #bot {
            /* Targets all id with 'col-' */
            border-bottom: 1px solid #EEE;
        }

        #top {
            min-height: 100px;
        }

        #mid {
            min-height: 80px;
        }

        #bot {
            min-height: 50px;
        }

        #top .logo {
            float: left;
            height: 60px;
            width: 60px;
            background: url(http://michaeltruong.ca/images/logo1.png) no-repeat;
            background-size: 60px 60px;
        }

        .clientlogo {
            float: left;
            height: 60px;
            width: 60px;
            background: url(http://michaeltruong.ca/images/client.jpg) no-repeat;
            background-size: 60px 60px;
            border-radius: 50px;
        }

        .info {
            display: block;
            float: left;
            margin-left: 0;
        }

        .title {
            float: right;
        }

        .title p {
            text-align: right;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        td {
            padding: 5px 0 5px 15px;
            border: 1px solid #EEE
        }

        .tabletitle {
            padding: 5px;
            font-size: .5em;
            background: #EEE;
        }

        .service {
            border-bottom: 1px solid #EEE;
        }

        .item {
            width: 24mm;
        }

        .itemtext {
            font-size: .5em;
        }

        #legalcopy {
            margin-top: 5mm;
        }
    </style>



    <div class="main-container">
        <div class="pd-ltr-20 xs-pd-20-10">
            <div class="min-height-200px">
                <div class="page-header">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">

                            <div class="header-left">
                                <div class="menu-icon dw dw-menu"></div>
                                <div class="search-toggle-icon dw dw-search2" data-toggle="header_search"></div>
                                <div class="header-search">
                                    <form>
                                        <div class="form-group mb-0">
                                            <i class="dw dw-search2 search-icon"></i>
                                            <input type="text" class="form-control search-input" placeholder="Search Here">

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="toolbar hidden-print">
                    <div class="text-right">
                        <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
                        <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
                    </div>
                    <hr>
                </div>
                <div id="invoice">

                    <div id="invoice-POS">

                        <center id="top">
                            <div class="logo"></div>
                            <div class="info">
                                <h2>SBISTechs Inc</h2>
                            </div>
                            <!--End Info-->
                        </center>
                        <!--End InvoiceTop-->

                        <div id="mid">
                            <div class="info">
                                <h2>Contact Info</h2>
                                <p>
                                    Address : street city, state 0000</br>
                                    Email : JohnDoe@gmail.com</br>
                                    Phone : 555-555-5555</br>
                                </p>
                            </div>
                        </div>
                        <!--End Invoice Mid-->

                        <div id="bot">

                            <div id="table">
                                <table>
                                    <tr class="tabletitle">
                                        <td class="item">
                                            <h2>Item</h2>
                                        </td>
                                        <td class="Hours">
                                            <h2>Qty</h2>
                                        </td>
                                        <td class="Rate">
                                            <h2>Sub Total</h2>
                                        </td>
                                    </tr>

                                    <tr class="service">
                                        <td class="tableitem">
                                            <p class="itemtext">Communication</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">5</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">$375.00</p>
                                        </td>
                                    </tr>

                                    <tr class="service">
                                        <td class="tableitem">
                                            <p class="itemtext">Asset Gathering</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">3</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">$225.00</p>
                                        </td>
                                    </tr>

                                    <tr class="service">
                                        <td class="tableitem">
                                            <p class="itemtext">Design Development</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">5</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">$375.00</p>
                                        </td>
                                    </tr>

                                    <tr class="service">
                                        <td class="tableitem">
                                            <p class="itemtext">Animation</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">20</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">$1500.00</p>
                                        </td>
                                    </tr>

                                    <tr class="service">
                                        <td class="tableitem">
                                            <p class="itemtext">Animation Revisions</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">10</p>
                                        </td>
                                        <td class="tableitem">
                                            <p class="itemtext">$750.00</p>
                                        </td>
                                    </tr>


                                    <tr class="tabletitle">
                                        <td></td>
                                        <td class="Rate">
                                            <h2>tax</h2>
                                        </td>
                                        <td class="payment">
                                            <h2>$419.25</h2>
                                        </td>
                                    </tr>

                                    <tr class="tabletitle">
                                        <td></td>
                                        <td class="Rate">
                                            <h2>Total</h2>
                                        </td>
                                        <td class="payment">
                                            <h2>$3,644.25</h2>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <!--End Table-->

                            <div id="legalcopy">
                                <p class="legal"><strong>Thank you for your business!</strong>  Payment is expected within 31 days; please process this invoice within that time. There will be a 5% interest charge per month on late invoices.
                                </p>
                            </div>

                        </div>
                        <!--End InvoiceBot-->
                    </div>
                    <!--End Invoice-->


                </div>








            </div>

        </div>
    </div>




    <script type="text/javascript">
        $('#printInvoice').click(function() {

            let divToPrint = $('#invoice-POS')
            var newWin = window.open('', 'Print-Window');

            newWin.document.open();

            newWin.document.write('<html><body onload="window.print()">' + divToPrint.html() + '</body></html>');

            newWin.document.close();

            setTimeout(function() {
                newWin.close();
            }, 10);
        });
    </script>