
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="title">
                        <h4>All Product  </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Product</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> All Products</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12">



                    <div class="header-right  float-right">




                        <div class="header-search">
                            <form>
                                <div class="form-group mb-0  mt-3">
                                    <a  href="<?= base_url('products/add_product') ?>"  class=" btn btn-primary"> Add New</a>

                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">

            <div class="pd-20">
                <table class="data-table table stripe hover nowrap">
                    <thead>
                        <tr>
                            <th> S #</th>
                            <th>Item Title</th>
                            <th>Barcode</th>
                            <th>Type</th>
                            <th> Category</th>
                            <th> Purchase </th>
                            <th> Sale </th>
                            <th> Print Barcode </th>
                            <th> Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $s = 1;
                        $catename = '-';
                        $typename = '-';
                      if (!empty ($Pro)){
                        foreach ($Pro as $p) {

                            foreach ($cate as $c) {
                                if ($p['product']->category_id == $c->category_id) {
                                    $catename = $c->category_name;
                                }
                            }
                            foreach ($type as $t) {
                                if ($p['product']->type_id == $t->type_id) {
                                    $typename = $t->type_name;
                                }
                            }
                            ?>



                            <tr>
                                <td><?= $s++ ?></td>
                               
   
          <?php
            $bar  = '';
     $nam  = '';
           $bar   =   $p["product"]->barcode;
                $nam = $p['product']->title;
           ?> 

                                <td><?= $p['product']->title ?> </td>
                         <td class="DivIdToPrint">
                            <?php if(!empty( base_url().'')) ?>
     <img style="height: 80px" src='<?php echo base_url(). "img/barcode.php?barcode=.$bar.&width=240&text=$nam/$bar "?>' />
              </td>
                                <td><?= $typename ?> </td>
                                <td><?= $catename ?> </td>

                                <td><?= $p['product']->purchase_price ?> </td>

                                <td><?= $p['product']->sale_price ?> </td>
                                <td><button onclick="printDiv(this);" class="btn btn-sm btn-warning"><i class="fa fa-print"></i></button></td>
                                 <td>  <?php if( $this->session->userdata('user')['role'] == 2){ ?>
                                    <div class="dropdown">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="<?= base_url("Products/edit_product/") . $p['product']->product_id; ?>"  ><i class="dw dw-edit2"></i> Edit</a>
                                            <?php if($this->session->userdata('user')['role'] != 1){ ?>
                                            <a class="dropdown-item" href="<?= base_url("Products/delete_pro/") . $p['product']->product_id; ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php }else { ?>
                                   <div class="dropdown text-right">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="fa fa-lock"></i>
                                        </a>
                                    


                                    </div>
                               <?php } ?>
                                </td>
                            </tr>

                        <?php } } ?>
                    </tbody>
                </table>

            </div>


        </div>
    </div>




</div>

<script>
    function printDiv(arg) {

        var divToPrint = $(arg).closest('tr').find('.DivIdToPrint');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">' + divToPrint.html() + '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 10);

    }
    var suc = ''
<?php
if (!empty($this->session->flashdata('success'))) {
    if ($this->session->flashdata('success')) {
        ?>
            suc = '<?= $this->session->flashdata('success'); ?>';

            swal({
                title: suc,

                type: 'success',
                confirmButtonClass: 'btn btn-success',

            })

        <?php
    }
}


?>
function printbar(arg) {
   alert($(arg).closest('tr').find('.printb').val())
}
</script>