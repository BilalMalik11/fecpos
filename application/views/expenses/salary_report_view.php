<style type="text/css">
    @media print {
        .header {
            display: none;
        }

        .footer-wrap {
            display: none;
        }

        .noprint {
            display: none;

        }
    }
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Salary Report </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Report</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                           Salary Report</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">

            <div class="pd-20">



                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="text-primary">Salary Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <form method="post" action="<?= base_url('Products/getsalaryreport') ?>">
                                <div class="card-body row">
                           
                         
                                                        <div class="col-md-12">
                            <div class="form-group ">
                                <label>Salary For The Month Of</label>
                                <select name="s_month"   class="form-control  "> 
                                 <option value="0">--select--</option>
                                 <option value="january">January </option>
                                 <option value="february">February </option>
                                 <option value="march">March </option>
                                 <option value="april">April </option>
                                 <option value="may">May </option>
                                 <option value="june">June </option>
                                 <option value="july">July </option>
                                 <option value="august">August </option>
                                 <option value="september">September </option>
                                 <option value="october">October </option>
                                 <option value="november">November </option>
                                 <option value="december">December </option>
               
                                </select>
                            </div>
                        </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>


                </div>




            </div>





        </div>
    </div>

</div>
 <script>
 function selsubtype(id){
 let coatype =  $(id).find('option:selected').val()

$('.coa_subtype').find('option').each(function (){

 if(coatype == $(this).data('type')){
   $(this).show()
 }
 else{
   $(this).hide()
   
 }

})

 }
</script>  
