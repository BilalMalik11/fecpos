
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="title">
                        <h4>Expenses List  </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Expense</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> Expenses List  </li>
                        </ol>
                    </nav>
                </div>
               
            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">

        
                <table class="table">
                    <thead>
                        <tr>
                         
                            <th>Recipient</th>
                            <th>COA_Type</th>
                            <th>COA_Subtype</th>
                            <th> Amount</th>
                            <th> Description </th>
                            <th> Date </th>
                            <th> File </th>
                               <th> Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (!empty($expense)) {
                              foreach($expense as $exp) {
 
                          ?>

                            <tr>
                             
                                <td><?= $exp->coa_name ?> </td>
                                <td><?= $exp->coa_type_name ?> </td>
                                 <td><?= $exp->coa_subtype_name ?> </td>
                                <td><?= $exp->amount ?> </td>
                                 <td><?= implode(' ', array_slice(explode(' ', $exp->coa_description), 0, 5));  ?></td> 
                                  <td> <?= date('Y/M/d', strtotime($exp->date ))?></td>
                          <td>
                             <?php if(!empty($exp->img)){ ?>
                         <a href="<?= base_url('/media/heads/').$exp->img ?>" download>
                         <i class="fa fa-download" aria-hidden="true"></i>
                         </a>
                          <?php   } else {?>
                           <a href="#" class="text-danger" >
                           <i class="fa fa-plus-circle"></i>
                             </a>
                              <?php }?>
                                </td> 

                            <td><a href="<?= base_url('products/postcoa/'.$exp->coa_id)?>" class="btn btn-info btn-sm">Post</a> 
                                       <?php $dis = ''; $cancel = "http://localhost/fecpos/Products/deletecoa/$exp->coa_id"; if($exp->updatethis  == '0') {

                                    $dis = 'disabled';
                                    $cancel = '#';

                                  } ?>
                                  <a <?= $dis ?>  href="<?=$cancel?>" class="btn btn-danger btn-sm">Cancel</a>
                               
                                  <button <?= $dis ?> class="btn btn-primary btn-sm " data-toggle="modal"  data-target="#exampleModal<?=$exp->coa_id?>">View</button>  
       

                                  </td>
 <div class="modal fade" id="exampleModal<?=$exp->coa_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Head</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> 
      <div class="modal-body">
        <form action="<?= base_url('Products/update_expense') ?>" method="post" enctype="multipart/form-data">
            
             
                <input type="hidden" name="coa" value="<?= $exp->coa_id ?> " class="form-control">
  
           <div class="row">
           <div class="col-md-6"> <div class="form-group">
                <label>Recipient</label>
                <input type="text" name="recipient" value="<?= $exp->coa_name ?> " class="form-control" >
            </div>
          </div>
          <div class="col-md-6">  <div class="form-group">
                <label>COA_Type</label>
               <select class="form-control" name="coa_type" onchange="selsubtype(this)" >
                               <option value="0">--select--</option>
                               <?php foreach ($coa_type as $key => $coat) { ?>
                               <option <?php if($coat->coa_type_id == $exp->coa_type_id){ ?> selected <?php } ?>  value="<?= $coat->coa_type_id?>"><?= ucwords($coat->coa_type_name)?></option>   
                                   <?php }  ?>
                               </select>
            </div></div>
         
            <div class="col-md-6"> <div class="form-group">
                <label>COA_Subtype</label>
                <select class="form-control coa_subtype" name="coa_subtype">
                        <option value="0">--select--</option>
                        <?php foreach ($coa_subtype as $key => $coast) { ?>
                        <option <?php if($coast->coa_subtype_typeid == $exp->coa_subtype_typeid){ ?> selected <?php } ?>   data-type="<?= $coast->coa_subtype_typeid?>" value="<?= $coast->coa_subtype_id?>"><?= ucwords($coast->coa_subtype_name)?></option>
                                 <?php }  ?>
                         </select>
            </div></div>
            <div class="col-md-6"> <div class="form-group">
                <label>Amount</label>
                <input type="text" name="amount"  require class="form-control">
            </div></div>
            <div class="col-md-6"> <div class="form-group">
                <label>Description</label>
                <input type="text" name="coa_desc" value="<?= $exp->coa_description ?>" class="form-control">
            </div> </div>
            <div class="col-md-6"> <div class="form-group">
                <label>Uploaded File</label> <br>
               <?php if(!empty($exp->img)){ ?>
<a href="<?= base_url('media/heads/').$exp->img ?>" download>
 <?= $exp->img ?> <i class="fa fa-download font-weight-bold" aria-hidden="true"></i>
</a>
<?php } else {?>
  <a href="#" class="text-danger" >
<i class="fa fa-plus-circle"></i>
</a>
<?php }?>
</div></div>

<div class="col-md-6"> <div class="form-group">
                <label>File</label>
                <input type="File" name="file"  class="form-control">
                 <input type="hidden" name="hiddenfile"class="form-control" value="<?php if(!empty($exp->img)){ echo $exp->img; }   ?>">
            </div> </div>
              

            <div class="col-md-6 mt-4"  > <div class="modal-footer">
      <button type="submit" class=" btn btn-success">Submit</button>
      </div>
      </div> </div>    

        </form>
      </div>
     
    </div>
  </div>
</div>
     
                            </tr>

               <?php }} ?>
                    </tbody>
                </table>

            </div>


        </div>
    </div>




</div>

 <script>
 function selsubtype(id){
 let coatype =  $(id).find('option:selected').val()

$('.coa_subtype').find('option').each(function (){

 if(coatype == $(this).data('type')){
   $(this).show()
 }
 else{
   $(this).hide()
   
 }

})

 }
</script>  