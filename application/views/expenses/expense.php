<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Expense</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Expense</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Chart of Account</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
         <div class="card-box mb-30">
            <form action="<?= base_url("Products/add_expense"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">

                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                 <label>COA Type </label>
<!--                                   <input class="form-control"   name="coa_type" placeholder=" " >  -->
                               <select class="form-control" name="coa_type" onchange="selsubtype(this)">
                               <option value="0">--select--</option>
                               <?php foreach ($coa_type as $key => $coat) { ?>
                               <option  value="<?= $coat->coa_type_id?>"><?= $coat->coa_type_name?></option>   
                                   <?php }  ?>
                               </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                 <label>COA Subtype </label>
                               <!--    <input class="form-control"   name="coa_subtype" placeholder=" " >  -->

                        <select class="form-control coa_subtype" name="coa_subtype">
                        <option value="0">--select--</option>
                        <?php foreach ($coa_subtype as $key => $coast) { ?>
                        <option   data-type="<?= $coast->coa_subtype_typeid?>" value="<?= $coast->coa_subtype_id?>"><?= $coast->coa_subtype_name?></option>
                                 <?php }  ?>
                         </select>
                                    </div>
                                </div>   
                            </div>
                            
                            <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                         <label>Recipient Name </label>
                                        <input class="form-control"   name="rec_name" placeholder=" Enter Name" > 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label>Amount </label>
                                        <input class="form-control"   name="coa_amount" placeholder=" Enter Amount" > 
                                    </div>
                                </div>
                              
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea  name="coa_desc" class="form-control" style="height: 100px;"> </textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">  
                                <label>File</label>
                                <?php if (!empty($coast->file)) { ?>
                                    <img src="<?= base_url('media/logo/') . $coast->file; ?>" class="avatar-photo"/>
                                <?php } else { ?>
                                    <img src="<?= base_url('media/placeholder.png'); ?>" class="avatar-photo"/>
                                <?php } ?>
                                <input type="file" name="file" onchange="displayImg(this)"  placeholder="Image">
                                <input type="hidden" name="hiddenimg"  value="<?php if(!empty($coast->file)){ echo $coast->file;}  ?>" >
                            </div>
                        </div>
                    </div>



                    <div class="row  ">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary  btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Submit</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>

    </div>
</div>


  <script>
 function selsubtype(id){
 let coatype =  $(id).find('option:selected').val()
// alert(coatype)
$('.coa_subtype').find('option').each(function (){

// alert($(this).data('type'))

 if(coatype == $(this).data('type')){
 
   $(this).show()
   // $('.coa_subtype').find('option').css('display' , 'block')
 }else{

  // $('.coa_subtype').find('option').css('display' , 'none')

   $(this).hide()
   
 
 }

})

 }
</script>  