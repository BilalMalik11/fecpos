<style type="text/css">
    @media print {
        .header {
            display: none;
        }

        .footer-wrap {
            display: none;
        }

        .noprint {
            display: none;

        }
    }
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Expense Report </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Report</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                           Expense Report</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">

            <div class="pd-20">



                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="text-primary">Expense Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <form method="post" action="<?= base_url('Products/getheadsreport') ?>">
                                <div class="card-body row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>COA Type</label>
                               <select class="form-control" name="coa_type" onchange="selsubtype(this)">
                               <option value="0">--select--</option>
                               <?php foreach ($coa_type as $key => $coat) { ?>
                               <option  value="<?= $coat->coa_type_id?>"><?= $coat->coa_type_name?></option>   
                                   <?php }  ?>
                               </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                         <label>COA Subtype</label>
                                    <select class="form-control coa_subtype" name="coa_subtype">
                                    <option value="0">--select--</option>
                                   <?php foreach ($coa_subtype as $key => $coast) { ?>
                                    <option   data-type="<?= $coast->coa_subtype_typeid?>" value="<?= $coast->coa_subtype_id?>"><?= $coast->coa_subtype_name?></option>
                                   <?php }  ?>
                                    </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>COA Name</label>
                                           <select class="form-control coa_name" name="coa_name">
                                            <option value="0">--select--</option>
                                            <?php foreach ($coa_name as $key => $coan) { ?>
                                           <option   data-type="<?= $coan->coa_id?>" value="<?= $coan->coa_name?>"><?= $coan->coa_name?></option>
                                           <?php }  ?>
                                           </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>


                </div>




            </div>





        </div>
    </div>

</div>
 <script>
 function selsubtype(id){
 let coatype =  $(id).find('option:selected').val()

$('.coa_subtype').find('option').each(function (){

 if(coatype == $(this).data('type')){
   $(this).show()
 }
 else{
   $(this).hide()
   
 }

})

 }
</script>  
