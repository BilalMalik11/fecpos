<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Salary</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Salary</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Expenses</li>
                        </ol>
                    </nav>


                </div>
  <div class="col-md-6 col-sm-12">
       <a href="<?= base_url('Products/salary_report_view') ?>" class="btn btn-primary" style="float: right"> Salary Report</a>
  </div>
            </div>
        </div>
        <!-- Simple Datatable start -->
          <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <form action="<?= base_url("Products/add_salary"); ?>" enctype="multipart/form-data" method="post">
                <div class="pd-20">


                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                 <label>Employee Name</label>
                    <select name="emp_name"   class="form-control  " onchange="getsal(this)"> 
                    <option value="0">--select--</option>
                   <?php foreach($employee as $emp)  {?>
                   <option data-sal="<?=$emp->salary?>" value="<?= $emp->uid ?>"> <?= $emp->name?> </option>
                   <?php } ?> 
                    </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label>Salary For The Month Of</label>
                                <select name="s_month"   class="form-control  "> 
                                 <option value="0">--select--</option>
                                 <option value="january">January </option>
                                 <option value="february">February </option>
                                 <option value="march">March </option>
                                 <option value="april">April </option>
                                 <option value="may">May </option>
                                 <option value="june">June </option>
                                 <option value="july">July </option>
                                 <option value="august">August </option>
                                 <option value="september">September </option>
                                 <option value="october">October </option>
                                 <option value="november">November </option>
                                 <option value="december">December </option>
               
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Salary Amount</label>
                                 <input type="text" name="salary"  class="form-control sal"  >
                            </div>
                        </div>

                      
                    </div>


                    <div class="row text-right">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary btn-block btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u fetch-meta">Pay Salary </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>

<script type="text/javascript">
    

function  getsal(obj){
 
 let sal = $(obj).find('option:selected').data('sal')
 
  $('.sal').val(sal)
}

</script>