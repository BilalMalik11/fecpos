<?php
$payments = 0;
if (!empty($purchase_payment)) {
    foreach ($purchase_payment as $pp) {
        $payments += $pp->salpay_amount;
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style type="text/css">
    @media print {
        .header {
            display: none;
        }

        .footer-wrap {
            display: none;
        }

        .noprint {
            display: none;

        }

    }
</style>

<body onload="window.print();">

    <div class="pd-ltr-20 xs-pd-20-10 table-responsive">
        <div class="min-height-200px">
            <div class="invoice-wrap    col-sm-12">
                <a href="<?= base_url('Products/sales_report') ?>" class="btn btn-primary pull-right noprint">New Search</a>
                <div class="btn btn-success noprint" onclick="window.print();">PRINT</div>

                <div class="invoice-box">
                    <h4 class=" mb-5 text-center">Over All Sales Report <small>(Date wise)</small> </h4>
                    <p class="font-14 mb-5 text-right weight-800">Date:<?= date('d M Y ') ?> </p>
                    <div class="invoice-header ">


                        <div class="  logo text-<?= $settings->logo_set_align; ?> ">
                            <img style="max-width: 150px !important;" src="<?= base_url('media/logo/' . $settings->logo_set_logo); ?>" alt="">

                        </div>

                    </div>
                    <h4 class="text-center  weight-800"><?= $settings->app_name; ?></h4>
                    <div class="col-md-12  col-xs-12">
                        <div class="text-center">

                            <p class="font-14 weight-800 mb-1"><?= $settings->logo_set_address; ?></p>
                            <p class="font-14  weight-800 mb-1"><?= $settings->logo_set_contact; ?></p>
                            <p class="font-14 weight-800 mb-1"><?= $settings->logo_set_email; ?></p>
                        </div>
                    </div>
                    <div class="row pb-30">

                    </div>
                    <div class=" ">
                        <table class="  table">
                            <div class=" ">

                                <tr>
                                    <th>Sale ID</th> 

                                    <th>Date</th>

                                    <th>Subtotal</th>
                                </tr>

                                <!-- <div class="invoice-  ">Product</div>
                        <div class="invoice-sub">Date</div>
                        <div class="invoice-rate">Discount</div>
                        <div class="invoice-hours">Quantity</div>
                        <div class="invoice-subtotal">Subtotal</div> -->
                            </div>
                            <div class="invoice-desc-body" style="min-height:unset;">

                                <?php
                                $grandtotal = 0;
                                $discount = 0;
                                if (!empty($rep)) {
                                ?>
                                    <?php foreach ($rep as $pi) { ?>


                                        <tr>
                                            <td> <?= $pi->sale_id; ?> </td> 
                                            <td> <?= date('d M Y  ', strtotime($pi->sale_created_on)); ?> </td>


                                            <?php $ttl =  $pi->sale_total - $pi->sales_discount   ?>
                                            <td> <?= $ttl; ?> </td>
                                        </tr>

                                    <?php
                                        $grandtotal += $ttl;
                                    }
                                    ?>
                                <?php } ?>

                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>
                                        <b> Total:</b>
                                    </th>
                                    <td>
                                        <b> <?= $grandtotal; ?></b>
                                    </td>

                                </tr>
                        </table>
                        <!-- <ul>
                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours  "><b> Grand Total:</b></div>
                                <div class="invoice-subtotal"><span class=" ">
                                        <h3> <?= $grandtotal; ?></h3>
                                    </span></div>
                            </li>



                        </ul> -->
                        <hr>

                        <h6 class="text-right   "> Power By Effiesoft</h6>
                    </div>

                </div>

            </div>

        </div>

    </div>

</body>

</html>