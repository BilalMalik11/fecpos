<?php
$payments = 0;
if (!empty($purchase_payment)) {
    foreach ($purchase_payment as $pp) {
        $payments += $pp->salpay_amount;
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
    @media print {
        .header {
            display: none;
        }

        .footer-wrap {
            display: none;
        }

        .noprint {
            display: none;

        }
    }

    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }
</style>


<body onload="window.print();"> <a href="<?= base_url('Products/sales_products') ?>" class="btn btn-primary pull-right noprint">New Sale</a>
    <div class="btn btn-success noprint" onclick="window.print();">PRINT</div>
    <div class="container" style="background:white">
        <div class="row">
            <div class="clearfix">&nbsp;</div>
            <div class="col-sm-12 col-md-12  ">
                <p class="font-14 mb-5 text-right weight-800">Date: <?= date('d F Y', strtotime($sales->sale_date)); ?> </p>
                <div class="invoice-header ">


                    <div class="  logo text-<?= $settings->logo_set_align; ?> ">
                        <img style="max-width: 150px !important;" src="<?= base_url('media/logo/' . $settings->logo_set_logo); ?>" alt="">

                    </div>

                </div>
                <h4 class="text-center mb-30  weight-1000"><?= $settings->app_name; ?></h4>
                <div class="col-md-12">
                    <div class="text-center">

                        <p class="font-14 mb-5  "><?= $settings->logo_set_address; ?></p>
                        <p class="font-14 mb-5  "><?= $settings->logo_set_contact; ?></p>
                        <p class="font-14 mb-5  "><?= $settings->logo_set_email; ?></p>
                    </div>
                </div>
                <br>
                <br>
                <div class="col-md-12 text-right">

                    <p class=" font-14"> Receipt # <?= $this->uri->segment(3); ?> </p>
                    <p class="font-14 "> Customer ID:<?php if (!empty($purchase_vendor->customer_id)) { ?> <?= $purchase_vendor->customer_id; ?> <?php }  ?> </p> <br>

                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class=" ">
                        <div class="table-responsive">
                            <table class="table table-condensed ">
                                <thead>
                                    <tr>
                                        <th><strong>Product</strong></th>
                                        <th class="text-center"><strong>Rate</strong></th>
                                        <th class="text-center"><strong>Quantity</strong></th>
                                        <th class="text-right"><strong>Subtotal</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $grandtotal = 0;
                                    $discount = 0;
                                    if (!empty($purchase_item)) {
                                    ?>
                                        <?php foreach ($purchase_item as $pi) { ?>

                                            <tr>

                                                <td><?= $pi->title; ?></td>
                                                <td class="text-center"><?= $pi->salitem_price; ?> </td>
                                                <td class="text-center"> <?= $pi->salitem_qty; ?></td>
                                                <td class="text-right"><?= $pi->salitem_total; ?> </td>
                                            </tr>

                                        <?php
                                            $grandtotal += $pi->salitem_total;
                                        }
                                        ?>
                                    <?php } ?>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td> <b> Grand Total:</b></td>
                                        <td class="text-right"><b> <?= $grandtotal; ?></b></td>

                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><b> Discount:</b></td>
                                        <td class="text-right"><b> <?= $sales->sales_discount; ?></b></td>

                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><b> Paid:</b></td>
                                        <td class="text-right"> <b> <?= $payments; ?></b></td>

                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><b> Balance:</b></td>
                                        <td class="text-right"><b> <?= $grandtotal - $payments - $sales->sales_discount; ?></td>

                                    </tr>


                                </tbody>
                            </table>


                            <table class="table ">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Payment Date </th>
                                        <th>Amount </th>
                                        <th>Sale By:</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($purchase_payment as $pp) { ?>
                                        <tr>
                                            <td>
                                                <?= date('d M Y', strtotime($pp->salpay_on)); ?>
                                            </td>
                                            <td>
                                                <?= $pp->salpay_amount ?>
                                            </td>


                                            <td><?= $pp->name; ?></td>
                                        </tr>

                                </tbody>

                            </table>
                            <hr>
                            <br>
                            <h6 class="text-right weight-500"> Powered By Effiesoft</h6>

                        <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

</html>

<script>
    jQuery(document).ready(function($) {
        window.print()
    });
</script>