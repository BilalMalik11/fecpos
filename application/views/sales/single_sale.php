<style type="text/css">
    .select2-results__option[aria-disabled="true"]{
        display:none;
    }
    .select2-container{
        width: 100% !important;
    }
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <form action="<?= base_url("Sales/update_sale"); ?>" enctype="multipart/form-data" method="post">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-4 col-sm-6">

                        <div class="title">
                            <h4>  Sales</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Sales Voucher</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Return</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-3 col-sm-6"><h3><center><b>Sales # <?= $this->uri->segment(3) ?></b></h3></center></div>
                    <div class="col-md-5 col-sm-6">
                        <div class="row">
                            <div class="col-md-6">&nbsp;</div>
                            <div class="col-md-6">
                                <label>Date</label>
                                <input type="date" name="sales_date" class="form-control" value="<?= date('Y-m-d'); ?>" />
                                <input type="hidden" name="created_by" value="<?= $this->session->userdata('user')['user_id'] ?>"   />
                                <!-- <input type="hidden" name="sales_return" value="<?= $this->uri->segment(3) ?>"   /> -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
               <!--  <div class="col-md-12">
                    <div class="card-box mb-30">
                        <div class="card-header">
                            <h4 class="h4">SEARCH</h4>
                        </div>
                        <div class="card-body" >
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>BARCODE</label>
                                        <input type="text" class="form-control inputbarcode" onblur="getproductByCode(this);" />
                                        <input type="hidden"  name="price" class="price">
                                        <input type="hidden"  name="proname" class="proname">
                                        <input type="hidden"  name="hid" class="hid">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control productType select2" onchange="getProductByType(this)">
                                            <option value="">select</option>
                                            <?php foreach ($type as $t) { ?>
                                                <option value="<?= $t->type_id ?>" ><?= $t->type_name ?></option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>PRODUCTS</label>
                                        <select class="form-control productdata select2" onchange="getProduct(this)" >
                                            <option value="">select</option>
                                            <?php foreach ($pro as $p) { ?>
                                                <option  data-id="<?= $p->product_id ?>" data-price="<?= $p->purchase_price ?>"  data-type="<?= $p->type_id ?>" data-code="<?= $p->barcode ?>" value="<?= $p->product_id ?>" ><?= $p->title ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <button type="button" id="sa-error" class="btn btn-success btn-block" onclick="appendrow(this)">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
        <!--         <div class="col-md-6">
                    <div class="card-box mb-30" >
                        <div class="card-header">
                            <h4 class="h4">Customer Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Customers</label>
                                        <select class="form-control select2new sale_custdrop"  name="customerid" onchange="getcustomer(this)">
                                            <option value="">Walk In</option>
                                            <?php foreach ($customers as $c) { ?>
                                                <option <?php if ($sale->sale_cust_id == $c->uid) { ?> selected="" <?php } ?> data-contact="<?= $c->contact ?>" data-email="<?= $c->email ?>" data-address="<?= $c->address ?>" value="<?= $c->uid ?>" ><?= $c->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text"  name="email" class="form-control cus_email" readonly />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact</label>
                                        <input type="text" name="contact"  class="form-control cus_contact" readonly />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" name="Address"  class="form-control cus_address" readonly />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-box mb-30">
                                <div class="card-header">
                                    <h4 class="h4">PRODUCTS</h4>
                                </div>
                                <div class="card-body" style="padding: 0px;">
                                    <div class="row" >
                                        <div class="col-md-12" >
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Price</th>
                                                        <th>Qty</th>
                                                        <th>SubTotal</th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbody">

                                                    <?php foreach ($proitems as $key => $pritems) { ?>

                                                        <tr> 
                                                            <td>  
                                                                <input type="text" value="<?= $pritems->title ?>" readonly="" class="form-control" />   
                                                                <input type="hidden" value="' + id + '" readonly="" name="p_id[]" class="form-control" /> 

                                                            <td>

                                                                <input type="number"  value="<?= $pritems->sale_price ?>"   class="form-control rowprice" name="product_price[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />  

                                                            <td>
                                                                <input type="number" value="<?= $pritems->salitem_qty ?>" class="form-control rowqty" name="product_quantity[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />  
                                                            </td>
                                                            <td><input type="number" readonly="" value="<?= $pritems->salitem_total ?>"  name=subtotal[] class="form-control subtotal" />   </td>
                                                           <td><button class="btn btn-lg btn-danger" type="button" onclick="removethis(this)"><i class="fa fa-trash"></i></button> </td>  

                                                        </tr> <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          <!--   <div class="col-md-12">
                                <div class="card-box mb-30" >
                                    <div class="card-header">
                                        <h4 class="h4">REMARKS</h4>
                                    </div>
                                    <div class="card-body" style="padding: 0px;">
                                        <textarea class="form-control" style="height: 390px;" rows="6" placeholder="Enter Remarks / Additional Information here..."><?= $sale->sale_remarks ?></textarea>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-box mb-30" >
                                <div class="card-header">
                                    <h4 class="h4">STATISTICS</h4>
                                </div>
                                <div class="card-body" style="padding: 0px;">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><b>TOTAL</b></td>
                                                <td><input type="number" name="total" class="form-control gtotal" readonly value="0" /></td>
                                            </tr>

                                            <tr>
                                                <td><b>DISCOUNT</b></td>
                                                <td><input type="number" class="form-control disc" onkeydown="getgtotal();" onkeypress="getgtotal();" onkeyup="getgtotal();" onblur="getgtotal();" value="0" /></td>
                                            </tr>
                                            <tr>
                                                <td><b>GRAND TOTAL</b></td>
                                                <td><input type="number" name="grandtotal"  class="form-control ggtotal" readonly value="0" /></td>
                                            </tr>
                                            <tr>

                                                <?php
                                                $paidnote = 0;
                                                if (!empty($paid)) {
                                                    foreach ($paid as $pai) {
                                                        $paidnote += $pai->salpay_amount;
                                                    }
                                                }
                                                ?>
                                                <td><b>PAID</b></td>
                                                <td><input readonly="" type="number" value="<?= $paidnote ?>" name="paid" class="form-control pay"  onkeydown="getBalance();" onkeypress="getBalance();" onkeyup="getBalance();" onblur="getBalance();"value="0" /></td>
                                            </tr>
                                            <tr>
                                                <td><b>BALANCE</b></td>
                                                <td><input type="number" class="form-control balance" readonly value="0" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php if ($paidnote > 0) { ?>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-block btn-lg btn-warning" data-toggle="modal" data-target="#paymenthistory-modal"> PAYMENT HISTORY</button>
                            </div>
                        <?php } ?>
                    </div>

                </div> -->
                   <div class="col-md-12">
                    <div class="card-box mb-30" >
                        <div class="card-header">
                            <h4 class="h4">STATISTICS</h4>
                        </div>
                        <div class="card-body" style="padding: 0px;">
                            <table class=" table table-bordered ">

                                <thead> 
                                <tr>
                                <th>TOTAL</th>    
                                <th>DISCOUNT</th>    
                                <th>GRAND TOTAL</th>    
                                <th>BALANCE</th>    
                                <th>PAID</th>    
                                </tr>
                                 </thead>
                                <tbody>
                                    
                               
                                    <tr>
                                      
                                        <td><input type="number" name="total" class="form-control gtotal" readonly value="0" /></td>
                                
                                        
                                        <td><input type="number" class="form-control disc" onkeydown=" getBalance();" onkeypress="getgtotal();" onkeyup="getgtotal();" onblur="getgtotal();" value="<?=$sale->sales_discount ?>"  name="discount" /></td>
                                   
                                        
                                        <td><input type="number" name="grandtotal"  class="form-control ggtotal" readonly value="0" /></td>
                                 
                                        <td><input type="number" class="form-control balance" readonly value="0" /></td>
                                        <td><input type="number" required  name="paid" class="form-control pay"  onkeydown="getBalance();" onkeypress="getBalance();" onkeyup="getBalance();" value="<?= $paidnote ?>" onblur="getBalance();"value="0" /></td>
                                     
                                       
                                       
                                    </tr>
                                </tbody>  
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <button type="button" data-toggle="modal" data-target="#payment-modal" class="btn btn-block btn-lg btn-success hidebtn"><i class="fa fa-save"></i> Return</button>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="clearfix">&nbsp;</div>

            </div>

        </form>
    </div>
</div>
  <div class="modal fade  " id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal  modal-dialog-centered">

        <div class="modal-content">
            <form action="<?= base_url("Sales/update_sale"); ?>"   method="post">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">RETURN PAYMENT</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>AMOUNT</label>
                                <input type="number" class="form-control returnamount" name="Return Amount" />
                                <input type="hidden"  value="<?= $this->uri->segment(3); ?>" name="sales_return" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" onclick=" return chek(); "  class="btn btn-danger ">Return</button>
                </div>
            </form>
        </div>

    </div>
</div>  
<script>
    $(document).ready(function () {
        
        $('.balance').val($('.disc').val() -  $('.balance').val()) 
        getSubtotal();
        $('.sale_custdrop').change();
        // if ($('.paid').val() == '0') {
        //     $('.hidebtn').css('display', 'none')
        // }

    });
    function chek( ) {
         if($('.pay').val() == $('.returnamount').val() ){

            return true;
         } else {
 swal("Error", "  Returned Amount does not match paid amount  !!", "error");
            return false;

         }  
    }
    function getproductByCode(arg) {
        var barcode = $(arg).val();
        var p_id;



        var type_id;
        var title;
        var pricee;
        if (barcode.trim() != '') {
            $('.productdata').find('option').each(function () {
                if (barcode == $(this).data('code')) {
                    $(this).attr('selected', 'selected');
                    type_id = $(this).data('type');
                    title = $(this).text();
                    pricee = $(this).data('price');
                    p_id = $(this).data('id');
                }
            });
            $('.productdata').trigger('change');


            $('.productType').find('option').each(function () {
                if (type_id == $(this).val()) {
                    $(this).prop('selected', true);
                }
            });
            $('.productType').trigger('change');


            $('.proname').val(title);
            $('.price').val(pricee);
            $('.hid').val(p_id);
        }
    }
    function getProductByType(arg) {
        $('.productdata').val('destory');
        var typeid = $(arg).val();
        if (typeid != '') {
            $('.productdata').find('option').each(function () {
                if (typeid == $(this).data('type')) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).removeAttr('disabled');
//                $(this).removeAttr('disabled');
                }
            });
        } else {
            $('.productdata').find('option').each(function () {
                $(this).removeAttr('disabled');
            });
        }
        $('.productdata').select2("destroy");
        $('.productdata').select2();
    }

    function getProduct(arg) {

        var barcode = $(arg).find('option:selected').data('code')
        var price = $(arg).find('option:selected').data('price')
        var title = $(arg).find('option:selected').text()
        $('.inputbarcode').val(barcode)
        $('.proname').val(title)
        $('.price').val(price)
        var p_id = $(arg).find('option:selected').data('id')
        $('.hid').val(p_id)


        var typeid = $(arg).find('option:selected').data('type');

        $('.productType').find('option').each(function (index, el) {
            if (typeid == $(this).val()) {
                $(this).attr('selected', 'selected');
            } else {
                $(this).removeAttr('selected', 'selected');
            }

        });
    }

    function appendrow() {
        var title = $('.proname').val()
        var price = $('.price').val()
        var id = $('.hid').val();


        var tr = ' <tr>  <td>  <input type="text" value="' + title + '" readonly="" class="form-control" />      <input type="hidden" value="' + id + '" readonly="" name="p_id[]" class="form-control" />      <td><input type="number"  value="' + price + '"   class="form-control rowprice" name="product_price[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />                   <td><input type="number" value="1" class="form-control rowqty" name="product_quantity[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />     <td><input type="number" readonly="" value="' + price + '"  name=subtotal[] class="form-control subtotal" />      <td><button class="btn btn-lg btn-danger" type="button" onclick="removethis(this)"><i class="fa fa-trash"></i></button> </td>   </tr>'

        if ($('.productdata').val() != '') {
            $('.tbody').append(tr)
            getSubtotal();
        } else {
            swal("Error", "Please Select a Product  !!", "error");
        }
    }

    function removethis(arg) {

        $(arg).closest('tr').remove();
        getSubtotal();
         getgtotal();
        getBalance()
    }

    function getSubtotal() {
        var total = 0;
        $('.rowprice').each(function () {
            var rowprice = $(this).closest('tr').find('.rowprice').val();
            var rowqty = $(this).closest('tr').find('.rowqty').val();
            $(this).closest('tr').find('.subtotal').val(rowprice * rowqty);
            total += rowprice * rowqty;
        });
        $('.gtotal').val(total);
        getBalance();
        getgtotal();
    }
    function getBalance() {
        var gtotal = $('.gtotal').val();
  var disc = $('.disc').val();
        var pay = $('.pay').val();
let balance = $('.balance')
      balance.val(gtotal - pay);
          balance.val(  balance.val() - disc);
    }
    function getgtotal() {
        var total = $('.gtotal').val();
        if (total == '') {
            total = 0;
        }
        var disc = $('.disc').val();
        if (disc == '') {
            disc = 0;
        }
        $('.ggtotal').val(total - disc);
    }

    function getcustomer(arg) {

        var contact = $(arg).find('option:selected').data('contact');
        var email = $(arg).find('option:selected').data('email');
        var address = $(arg).find('option:selected').data('address');


        if ($(arg).find('option:selected').val() != '') {
            if ($.isNumeric($(arg).find('option:selected').val())) {
                $('.cus_email').prop('readonly', true);
                $('.cus_contact').prop('readonly', true);
                $('.cus_address').prop('readonly', true);

                $('.cus_email').val(email);
                $('.cus_contact').val(contact);
                $('.cus_address').val(address);
            } else {
                $('.cus_email').prop('readonly', false);
                $('.cus_contact').prop('readonly', false);
                $('.cus_address').prop('readonly', false);
                $('.cus_email').val('');
                $('.cus_contact').val('');
                $('.cus_address').val('');
            }
        } else {

            $('.cus_email').prop('readonly', true);
            $('.cus_contact').prop('readonly', true);
            $('.cus_address').prop('readonly', true);

            $('.cus_email').val(email);
            $('.cus_contact').val(contact);
            $('.cus_address').val(address);
        }


    }


</script>
<div class="modal fade  " id="paymenthistory-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-success" >PAYMENTS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body" style="padding:0px;">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Created By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($paid)) {
                                    foreach ($paid as $pp) {
                                        ?>
                                        <tr>
                                            <td><?= date('d M Y', strtotime($pp->salpay_on)); ?></td>
                                            <td><?= $pp->salpay_amount; ?></td>
                                            <td><?= $pp->name; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>