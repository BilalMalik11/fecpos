
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="title">
                        <h4>  Sales Voucher  </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Sales</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> All Sales</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12">



                    <div class="header-right  float-right">




                        <div class="header-search">
                            <form>
                                <div class="form-group mb-0  mt-3">


                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">

            <div class="pd-20">
                <table class="data-table table stripe hover nowrap">
                    <thead>
                        <tr>
                            <th> S #</th>
                            <th>Date</th>
                            <th>Sale Id</th>
                            <th>Customer</th>
                            <th>Created On</th>
                            <th> Total</th>

                            <th class="text-center"> Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $ss = 1;
                        foreach ($sale as $s) {
                            ?>



                            <tr>
                                <td><?= $ss++ ?></td> 
                                <td > <?= date('m/d/Y', strtotime($s->sale_date)) ?>  </td> 

                                <td><b><?= $s->sale_id ?></b></td>
                                <td ><?php if(!empty($s->name)){ ?><?= $s->name ?><?php }else{?> Walkin<?php } ?></td> 
                                <td > <?= date('m/d/Y H:i a', strtotime($s->sale_created_on)); ?>  </td> 
                                <td > <?= $s->sale_total ?>  </td> 




                                <td  class="text-center">
                                    <div class="dropdown">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="<?= base_url("sales/single_sales/" . $s->sale_id); ?>"  ><i class="dw dw-eye"></i> Return</a>
                                            <a class="dropdown-item" href="<?= base_url("sales/sale_print/" . $s->sale_id); ?>"  ><i class="dw dw-print"></i> Print</a>

                                        </div>
                                    </div>
                                </td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>

            </div>


        </div>
    </div>




</div>

<script>
    function printDiv(arg) {

        var divToPrint = $(arg).closest('tr').find('#DivIdToPrint');
        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">' + divToPrint.html() + '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 10);

    }
    var suc = ''
<?php
if (!empty($this->session->flashdata('success'))) {
    if ($this->session->flashdata('success')) {
        ?>
            suc = '<?= $this->session->flashdata('success'); ?>';

            swal({
                title: suc,

                type: 'success',
                confirmButtonClass: 'btn btn-success',

            })

        <?php
    }
}
?>
</script>