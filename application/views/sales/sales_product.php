 <style type="text/css">
     .select2-results__option[aria-disabled="true"] {
         display: none;
     }

     .select2-container {
         width: 100% !important;
     }

     .frm {

         height: 10px !important;
     }
 </style>
 <div class="pd-ltr-20 xs-pd-20-10">
     <div class=" dw dw-home ml-2" style="height: 30px;width: 50px"><a class="text-info m-1" href="<?= base_url('Welcome/index') ?>">Home</a></div>
 </div>

 <div class="min-height-200px">
     <form action="<?= base_url("Sales/insert_sale"); ?>" enctype="multipart/form-data" method="post">
         <div class="page-header">
             <div class="row">
                 <div class="col-md-12">
                     <div class="card-box mb-30">
                         <div class="card-header">
                             <h4 class="h4">Customer Information</h4>
                         </div>
                         <div class="card-body">
                             <div class="row">
                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Customers ID</label>
                                         <select class="form-control select2new frm" onchange="getcustomer(this)">
                                             <option value="">Customer ID</option>
                                             <?php foreach ($customers as $c) { ?>
                                                 <option data-contact="<?= $c->contact ?>" data-email="<?= $c->email ?>" data-address="<?= $c->address ?>" value="<?= $c->customer_id ?>"><?= $c->customer_id ?></option>
                                             <?php } ?>
                                         </select>
                                     </div>
                                 </div>
                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Customers Name</label>
                                         <select class="form-control select2new frm" name="customerid" onchange="getcustomer(this)">
                                             <option value="">Walk In</option>
                                             <?php foreach ($customers as $c) { ?>
                                                 <option data-contact="<?= $c->contact ?>" data-email="<?= $c->email ?>" data-address="<?= $c->address ?>" value="<?= $c->customer_id ?>"><?= $c->cusomer_name ?></option>
                                             <?php } ?>
                                         </select>
                                     </div>
                                 </div>
                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Email</label>
                                         <input type="text" name="email frm" class="form-control cus_email" readonly />
                                     </div>
                                 </div>
                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Contact</label>
                                         <input type="text" name="contact frm" class="form-control cus_contact" readonly />
                                     </div>
                                 </div>
                                 <!-- <div class="col-md-12">
                                     <div class="form-group">
                                         <label>Address</label>
                                         <input type="text" name="Address frm" class="form-control cus_address" readonly />
                                     </div>
                                 </div> -->
                             </div>
                         </div>
                     </div>
                 </div>



             </div>
         </div>
         <div class="col-md-5 col-sm-6">
             <div class="row">
                 <div class="col-md-6">&nbsp;</div>
                 <!-- <div class="col-md-6"> -->
                 <!-- <label>Date</label> -->
                 <!-- <input type="date" name="sales_date" class="form-control" value="<?= date('Y-m-d'); ?>" /> -->
                 <input type="hidden" name="created_by" value="<?= $this->session->userdata('user')['user_id'] ?>" />
                 <input type="hidden" name="price" class="price">
                 <input type="hidden" name="proname" class="proname">
                 <input type="hidden" name="hid" class="hid">
                 <input type="hidden" class="form-control inputbarcode" onblur="getproductByCode(this);" />
                 <!-- </div> -->
             </div>
         </div>

         <div class="row">
             <div class="col-md-6">
                 <div class="card-box mb-30">
                     <div class="card-header">
                         <h4 class="h4">SEARCH</h4>
                     </div>
                     <div class="card-body">
                         <div class="row">
                             <?php foreach ($pro as $p) { ?>

                                 <!-- <option data-id="<?= $p->product_id ?>" data-price="<?= $p->sale_price ?>" data-type="<?= $p->type_id ?>" data-code="<?= $p->barcode ?>" value="<?= $p->product_id ?>"><?= $p->title ?></option> -->
                                 <!-- 
                                     <div class="form-group">
                                         <label>PRODUCTS</label>
                                         <select class="form-control productdata select2" onchange="getProduct(this)">
                                             <option value="">select</option>
                                          
                                         </select>
                                     </div> -->



                                 <button type="button" class="btn btn-success btn-lg    p-1 mr-1" onclick="getProduct(this); appendrow()" data-id="<?= $p->product_id ?>" data-price="<?= $p->sale_price ?>" data-type="<?= $p->type_id ?>" data-code="<?= $p->barcode ?>" value="<?= $p->product_id ?>"><?= $p->title ?></button>



                             <?php } ?>
                             <!-- <button type="button" id="sa-error" class="btn btn-success btn-block" onclick="appendrow(this)">Add</button> -->


                             <!--   <div class="col-md-4">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <button type="button" id="sa-error" class="btn btn-success btn-block" onclick="appendrow(this)">Add</button>
                                    </div>
                                </div> -->

                         </div>

                     </div>
                 </div>
             </div>

             <div class="col-md-6">
                 <div class="card-box mb-30">
                     <div class="card-header">
                         <h4 class="h4">PRODUCTS</h4>
                     </div>
                     <div class="card-body" style="padding: 0px;">
                         <div class="row">
                             <div class="col-md-12">
                                 <table class="table" style="  max-height: calc(100vh - 9rem);
      overflow-y: auto;">
                                     <thead>
                                         <tr>
                                             <th>Product</th>
                                             <th>Price</th>
                                             <th>Qty</th>
                                             <th>SubTotal</th>
                                             <th>&nbsp;</th>
                                         </tr>
                                     </thead>
                                     <tbody class="tbody">

                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <!--            <div class="col-md-6">
                    <div class="card-box mb-30" >
                        <div class="card-header">
                            <h4 class="h4">Customer Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Customers ID</label>
                                        <select class="form-control select2new frm"    onchange="getcustomer(this)">
                                             <option value="">Customer ID</option>
                                            <?php foreach ($customers as $c) { ?>
                                                <option data-contact="<?= $c->contact ?>" data-email="<?= $c->email ?>" data-address="<?= $c->address ?>" value="<?= $c->customer_id ?>" ><?= $c->customer_id ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Customers Name</label>
                                        <select class="form-control select2new frm"  name="customerid" onchange="getcustomer(this)">
                                            <option value="">Walk In</option>
                                            <?php foreach ($customers as $c) { ?>
                                                <option data-contact="<?= $c->contact ?>" data-email="<?= $c->email ?>" data-address="<?= $c->address ?>" value="<?= $c->customer_id ?>" ><?= $c->cusomer_name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text"  name="email frm" class="form-control cus_email" readonly />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact</label>
                                        <input type="text" name="contact frm"  class="form-control cus_contact" readonly />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" name="Address frm"  class="form-control cus_address" readonly />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
         </div>
         <div class="row">
             <div class="col-md-12">
                 <!--  <div class="row">
                        <div class="col-md-12">
                            <div class="card-box mb-30">
                                <div class="card-header">
                                    <h4 class="h4">PRODUCTS</h4>
                                </div>
                                <div class="card-body" style="padding: 0px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Price</th>
                                                        <th>Qty</th>
                                                        <th>SubTotal</th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbody">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                             <div class="col-md-12">
                            <div class="card-box mb-30" >
                                <div class="card-header">
                                    <h4 class="h4">REMARKS</h4>
                                </div>
                                <div class="card-body" style="padding: 0px;">
                                    <textarea class="form-control" style="height: 390px;" rows="6" placeholder="Enter Remarks / Additional Information here..." name="remarks"></textarea>
                                </div>
                            </div>
                        </div>
                    </div> -->
             </div>
             <div class="col-md-12">
                 <div class="card-box mb-30">
                     <div class="card-header">
                         <h4 class="h4">STATISTICS</h4>
                     </div>
                     <div class="card-body" style="padding: 0px;">
                         <table class=" table table-bordered ">

                             <thead>
                                 <tr>
                                     <th>TOTAL</th>
                                     <th>DISCOUNT</th>
                                     <th>GRAND TOTAL</th>
                                     <th>BALANCE</th>
                                     <th>PAID</th>
                                 </tr>
                             </thead>
                             <tbody>


                                 <tr>

                                     <td><input type="number" name="total" class="form-control gtotal" readonly value="0" /></td>


                                     <td><input type="number" class="form-control disc" onkeydown=" getBalance();" onkeypress="getgtotal();" onkeyup="getgtotal();" onblur="getgtotal();" value="0" name="discount" /></td>


                                     <td><input type="number" name="grandtotal" class="form-control ggtotal" readonly value="0" /></td>

                                     <td><input type="number" class="form-control balance" readonly value="0" /></td>
                                     <td><input type="number" required name="paid" class="form-control pay" onkeydown="getBalance();" onkeypress="getBalance();" onkeyup="getBalance();" onblur="getBalance();" value="0" /></td>



                                 </tr>
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
         <div class="row">


             <div class="col-md-12">
                 <button type="submit" onclick=" return chek(); " class="btn btn-block btn-lg btn-success"><i class="fa fa-save"></i> Sale</button>
             </div>
             <div class="clearfix">&nbsp;</div>
         </div>

     </form>
 </div>
 </div>

 <script>
     function chek() {
         if ($('.pay').val() == $('.ggtotal').val()) {

             return true;
         } else {
             swal("Error", " Please Enter the Total Paid Amount  !!", "error");
             return false;

         }
     }

     function appendsale() {
         var title = $('.proname').val()
         var price = $('.price').val()
         var id = $('.hid').val();


         var tr = ' <tr>  <td>  <input type="text" value="' + title + '" readonly="" class="form-control" />      <input type="hidden" value="' + id + '" readonly="" name="p_id[]" class="form-control" />      <td><input type="number"  value="' + price + '"   class="form-control rowprice" name="product_price[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />                   <td><input type="number" value="1" class="form-control rowqty" name="product_quantity[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />     <td><input type="number" readonly="" value="' + price + '"  name=subtotal[] class="form-control subtotal" />      <td><button class="btn btn-lg btn-danger" type="button" onclick="removethis(this)"><i class="fa fa-trash"></i></button>    </tr>'

         if ($('.productdata').val() != '') {
             $('.tbody').append(tr)
             getSubtotal();
         } else {
             swal("Error", "Please Select a Product  !!", "error");
         }
     }

     function getproductByCode(arg) {

         var barcode = $(arg).val();
         var p_id;



         var type_id;
         var title;
         var pricee;
         if (barcode.trim() != '') {
             $('.productdata').find('option').each(function() {
                 if (barcode == $(this).data('code')) {
                     $(this).attr('selected', 'selected');
                     type_id = $(this).data('type');
                     title = $(this).text();
                     pricee = $(this).data('price');
                     p_id = $(this).data('id');
                 }
             });
             $('.productdata').trigger('change');


             $('.productType').find('option').each(function() {
                 if (type_id == $(this).val()) {
                     $(this).prop('selected', true);
                 }
             });
             $('.productType').trigger('change');


             $('.proname').val(title);
             $('.price').val(pricee);
             $('.hid').val(p_id);
         }
         appendrow()
     }

     function getProductByType(arg) {
         $('.productdata').val('destory');
         var typeid = $(arg).val();
         if (typeid != '') {
             $('.productdata').find('option').each(function() {
                 if (typeid == $(this).data('type')) {
                     $(this).attr('disabled', true);
                 } else {
                     $(this).removeAttr('disabled');
                     //                $(this).removeAttr('disabled');
                 }
             });
         } else {
             $('.productdata').find('option').each(function() {
                 $(this).removeAttr('disabled');
             });
         }
         $('.productdata').select2("destroy");
         $('.productdata').select2();
     }

     function getProduct(arg) {

         var barcode = $(arg).data('code')
         var price = $(arg).data('price')
         var title = $(arg).text()
         $('.inputbarcode').val(barcode)
         $('.proname').val(title)
         $('.price').val(price)
         var p_id = $(arg).data('id')
         $('.hid').val(p_id)


         //  var typeid = $(arg).find('option:selected').data('type');

         //  $('.productType').find('option').each(function(index, el) {
         //      if (typeid == $(this).val()) {
         //          $(this).attr('selected', 'selected');
         //      } else {
         //          $(this).removeAttr('selected', 'selected');
         //      }

         //  });
     }

     function appendrow() {
         var title = $('.proname').val()
         var price = $('.price').val()
         var id = $('.hid').val();


         var tr = ' <tr>  <td>  <input type="text" value="' + title + '" readonly="" class="form-control" />      <input type="hidden" value="' + id + '" readonly="" name="p_id[]" class="form-control" />      <td><input type="number"  value="' + price + '"   class="form-control rowprice" name="product_price[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />                   <td><input type="number" value="1" class="form-control rowqty" name="product_quantity[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />     <td><input type="number" readonly="" value="' + price + '"  name=subtotal[] class="form-control subtotal" />      <td><button class="btn btn-lg btn-danger" type="button" onclick="removethis(this)"><i class="fa fa-trash"></i></button>    </tr>'

         if ($('.productdata').val() != '') {
             $('.tbody').append(tr)
             getSubtotal();
         } else {
             swal("Error", "Please Select a Product  !!", "error");
         }
     }

     function removethis(arg) {

         $(arg).closest('tr').remove();
         getSubtotal();
     }

     function getSubtotal() {
         var total = 0;
         $('.rowprice').each(function() {
             var rowprice = $(this).closest('tr').find('.rowprice').val();
             var rowqty = $(this).closest('tr').find('.rowqty').val();
             $(this).closest('tr').find('.subtotal').val(rowprice * rowqty);
             total += rowprice * rowqty;
         });
         $('.gtotal').val(total);
         getBalance();
         getgtotal();
     }

     function getBalance() {
         var gtotal = $('.gtotal').val();
         var disc = $('.disc').val();
         var pay = $('.pay').val();
         let balance = $('.balance')
         balance.val(gtotal - pay);
         balance.val(balance.val() - disc);
         // $('.balance').val(total - disc);
     }

     function getgtotal() {
         var total = $('.gtotal').val();
         if (total == '') {
             total = 0;
         }
         var disc = $('.disc').val();
         if (disc == '') {
             disc = 0;
         }
         $('.ggtotal').val(total - disc);
         $('.balance').val(total - disc);
     }

     function getcustomer(arg) {

         var contact = $(arg).find('option:selected').data('contact');
         var email = $(arg).find('option:selected').data('email');
         var address = $(arg).find('option:selected').data('address');


         if ($(arg).find('option:selected').val() != '') {
             if ($.isNumeric($(arg).find('option:selected').val())) {
                 $('.cus_email').prop('readonly', true);
                 $('.cus_contact').prop('readonly', true);
                 $('.cus_address').prop('readonly', true);

                 $('.cus_email').val(email);
                 $('.cus_contact').val(contact);
                 $('.cus_address').val(address);
             } else {
                 $('.cus_email').prop('readonly', false);
                 $('.cus_contact').prop('readonly', false);
                 $('.cus_address').prop('readonly', false);
                 $('.cus_email').val('');
                 $('.cus_contact').val('');
                 $('.cus_address').val('');
             }
         } else {

             $('.cus_email').prop('readonly', true);
             $('.cus_contact').prop('readonly', true);
             $('.cus_address').prop('readonly', true);

             $('.cus_email').val(email);
             $('.cus_contact').val(contact);
             $('.cus_address').val(address);
         }


     }
 </script>