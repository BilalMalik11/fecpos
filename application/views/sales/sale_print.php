<?php
$payments = 0;
if (!empty($purchase_payment)) {
    foreach ($purchase_payment as $pp) {
        $payments += $pp->salpay_amount;
    }
}
?>
<style type="text/css">
    @media print {
        .header {
            display: none;
        }

        .footer-wrap {
            display: none;
        }

        .noprint {
            display: none;

        }
    }
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="invoice-wrap">
            <a href="<?= base_url('Products/sales') ?>" class="btn btn-primary pull-right noprint">New Sale</a>
            <div class="btn btn-success noprint" onclick="window.print();">PRINT</div>

            <div class="invoice-box">
                <p class="font-14 mb-5 text-right weight-800">Date: <?= date('d F Y', strtotime($sales->sale_date)); ?> </p>
                <div class="invoice-header ">


                    <div class="  logo text-<?= $settings->logo_set_align; ?> ">
                        <img style="max-width: 150px !important;" src="<?= base_url('media/logo/' . $settings->logo_set_logo); ?>" alt="">

                    </div>

                </div>
                <h4 class="text-center mb-30  weight-1000"><?= $settings->app_name; ?></h4>
                <div class="col-md-12">
                    <div class="text-center">

                        <p class="font-14 mb-5  weight-800"><?= $settings->logo_set_address; ?></p>
                        <p class="font-14 mb-5  weight-800"><?= $settings->logo_set_contact; ?></p>
                        <p class="font-14 mb-5  weight-800"><?= $settings->logo_set_email; ?></p>
                    </div>
                </div>
                <div class="row pb-30">
                    <div class="col-md-12 text-right">

                        <b class="mb-15" weight-800> Receipt # <?= $this->uri->segment(3); ?> </b> <br>
                        <b class="mb-15" weight-800> Customer ID:<?php if (!empty($purchase_vendor->customer_id)) { ?> <?= $purchase_vendor->customer_id; ?> <?php }  ?> </b> <br>

                    </div>
                </div>
                <div class="invoice-desc">
                    <div class="invoice-desc-head clearfix">
                        <div class="invoice-sub weight-800">Product</div>
                        <div class="invoice-rate weight-800">Rate</div>
                        <div class="invoice-hours weight-800">Quantity</div>
                        <div class="invoice-subtotal weight-800">Subtotal</div>
                    </div>
                    <div class="invoice-desc-body" style="min-height:unset;">
                        <ul>
                            <?php
                            $grandtotal = 0;
                            $discount = 0;
                            if (!empty($purchase_item)) {
                            ?>
                                <?php foreach ($purchase_item as $pi) { ?>
                                    <li class="clearfix">
                                        <div class="invoice-sub weight-800"><?= $pi->title; ?></div>
                                        <div class="invoice-rate weight-800"><?= $pi->salitem_price; ?></div>
                                        <div class="invoice-hours weight-800"><?= $pi->salitem_qty; ?></div>
                                        <div class="invoice-subtotal weight-800"><span class="weight-600"><?= $pi->salitem_total; ?></span></div>
                                    </li>


                                <?php
                                    $grandtotal += $pi->salitem_total;
                                }
                                ?>
                            <?php } ?>
                        </ul>
                        <ul>
                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours weight-800">Grand Total:</div>
                                <div class="invoice-subtotal"><span class="weight-800"><?= $grandtotal; ?></span></div>
                            </li>
                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours   weight-800">Discount:</div>
                                <div class="invoice-subtotal "><span class="weight-800"><?= $sales->sales_discount; ?></span></div>
                            </li>
                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours   weight-800">Paid:</div>
                                <div class="invoice-subtotal   "><span class="weight-800"><?= $payments; ?></span></div>
                            </li>

                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours weight-800">Balance:</div>
                                <div class="invoice-subtotal "><span class="weight-800"><?= $grandtotal - $payments - $sales->sales_discount; ?></span></div>
                            </li>
                        </ul>
                    </div>
                    <?php if (!empty($purchase_payment)) { ?>
                        <div class="invoice-desc">
                            <!--     <div class="invoice-desc-head clearfix" style="border-bottom:2px solid gray;">
                                <div class="invoice-sub">Payment History</div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours"></div>
                                <div class="invoice-subtotal"></div>
                            </div> -->
                            <div class="invoice-desc-head clearfix">
                                <div class="invoice-rate weight-800">Payment Date</div>
                                <div class="invoice-sub weight-800">Amount</div>
                                <div class="invoice-hours"></div>
                                <div class="invoice-subtotal"></div>
                            </div>
                            <div class="invoice-desc-body">
                                <ul>
                                    <?php foreach ($purchase_payment as $pp) { ?>
                                        <li class="clearfix" style="list-style: none;">
                                            <div class="invoice-rate weight-800"><?= date('d M Y', strtotime($pp->salpay_on)); ?></div>
                                            <div class="invoice-sub weight-800"><?= $pp->salpay_amount ?></div>
                                            <div class="invoice-hours weight-800">Created By:</div>
                                            <div class="invoice-subtotal"><span class=" weight-800 "><?= $pp->name; ?></span></div>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <br>
                                <br>

                                <h6 class="text-right weight-500"> Power By Effiesoft</h6>
                            </div>
                        </div>
                    <?php } ?>

                </div>

            </div>

        </div>

    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        window.print()
    });
</script>