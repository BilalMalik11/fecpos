<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog  ">
        <form action="<?= base_url("Products/new_category"); ?>" method="post">


            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-blue">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row ">
                        <div class="col-md-12  " >
                            <div class="form-group">
                                <label>Category:</label>
                                <input type="text" name="category" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12  ">
                            <div class="form-group">
                                <label>Sort Description:</label>
                                <textarea class="form-control" name="desc"></textarea>
                            </div>
                        </div>
                    </div>






                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary update-rec execute-loader">Save</button>
                </div>
            </div>
        </form>


    </div>
</div>