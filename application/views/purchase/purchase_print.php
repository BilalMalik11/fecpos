<?php
$payments = 0;
if (!empty($purchase_payment)) {
    foreach ($purchase_payment as $pp) {
        $payments += $pp->purpay_amount;
    }
}
?>
<style type="text/css">
    @media print{
        .header{
            display:none;
        }
        .footer-wrap{
            display:none;
        }
        .noprint{
            display:none;
            
        }
    }
    
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px"> 
        <div class="invoice-wrap">
            <div class="btn btn-success noprint" onclick="window.print();">PRINT</div>
            <div class="invoice-box">
                  <p class="font-14 mb-5 text-right">Date:  <?= date('d F Y', strtotime($purchase->pur_date)); ?> </p>
                  <div class="invoice-header ">


                    <div class="  logo text-<?= $settings->logo_set_align; ?> ">
                        <img style="max-width: 150px !important;" src="<?= base_url('media/logo/' . $settings->logo_set_logo); ?>" alt="">

                    </div>

                </div>
                 <h4 class="text-center mb-30 weight-600"><?= $settings->app_name; ?></h4> 
                <div class="col-md-12">
                    <div class="text-center">

                        <p class="font-14 mb-5"><?= $settings->logo_set_address; ?></p>
                        <p class="font-14 mb-5"><?= $settings->logo_set_contact; ?></p>
                        <p class="font-14 mb-5"><?= $settings->logo_set_email; ?></p>
                    </div>
                </div>
                    <div class="row pb-30">
                    <div class="col-md-12 text-right">
                        
                        <b class="mb-15"><small> Receipt # <?= $this->uri->segment(3); ?></small></b> <br>
                        <b class="mb-15"><small> Customer ID:<?php if (!empty($purchase_vendor->customer_id)) { ?> <?= $purchase_vendor->customer_id; ?> <?php }  ?></small> </b> <br>  
                  
                    </div>
                </div> 
                <div class="invoice-desc">
                    <div class="invoice-desc-head clearfix">
                        <div class="invoice-sub">Product</div>
                        <div class="invoice-rate">Rate</div>
                        <div class="invoice-hours">Quantity</div>
                        <div class="invoice-subtotal">Subtotal</div>
                    </div>
                    <div class="invoice-desc-body" style="min-height:unset;">
                        <ul>
                            <?php
                            $grandtotal = 0;
                            if (!empty($purchase_item)) {
                                ?>
                                <?php foreach ($purchase_item as $pi) { ?>
                                    <li class="clearfix">
                                        <div class="invoice-sub"><?= $pi->title; ?></div>
                                        <div class="invoice-rate"><?= $pi->puritem_price; ?></div>
                                        <div class="invoice-hours"><?= $pi->puritem_qty; ?></div>
                                        <div class="invoice-subtotal"><span class="weight-600"><?= $pi->puritem_total; ?></span></div>
                                    </li>

                                    <?php
                                    $grandtotal += $pi->puritem_total;
                                }
                                ?>
                            <?php } ?>
                        </ul>
                        <ul>
                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours">Grand Total:</div>
                                <div class="invoice-subtotal"><span class="weight-600"><?= $grandtotal; ?></span></div>
                            </li>
                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours text-success">Paid:</div>
                                <div class="invoice-subtotal text-success"><span class="weight-600"><?= $payments; ?></span></div>
                            </li>
                            <li class="clearfix">
                                <div class="invoice-sub"></div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours text-danger">Balance:</div>
                                <div class="invoice-subtotal text-danger"><span class="weight-600"><?= $grandtotal - $payments; ?></span></div>
                            </li>
                        </ul>
                    </div>
                    <?php if (!empty($purchase_payment)) { ?>
                        <div class="invoice-desc" >
                            <div class="invoice-desc-head clearfix" style="border-bottom:2px solid gray;">
                                <div class="invoice-sub">Payment History</div>
                                <div class="invoice-rate"></div>
                                <div class="invoice-hours"></div>
                                <div class="invoice-subtotal"></div>
                            </div>
                            <div class="invoice-desc-head clearfix">
                                <div class="invoice-rate">Payment Date</div>
                                <div class="invoice-sub">Amount</div>
                                <div class="invoice-hours"></div>
                                <div class="invoice-subtotal"></div>
                            </div>
                            <div class="invoice-desc-body">
                                <ul>
                                    <?php foreach ($purchase_payment as $pp) { ?>
                                        <li class="clearfix" style="list-style: none;">
                                            <div class="invoice-rate"><?= date('d M Y', strtotime($pp->payment_on)); ?></div>
                                            <div class="invoice-sub"><?= $pp->purpay_amount ?></div>
                                            <div class="invoice-hours">Created By:</div>
                                            <div class="invoice-subtotal"><span class="weight-600"><?= $pp->name; ?></span></div>
                                        </li>
                                    <?php } ?>
                                </ul>
                                  <br>
                                   <br>

                        <h6 class="text-right"><small>Power By</small> Effiesoft</h6>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
            
        </div>
         
    </div>
</div>
<script>
    
    jQuery(document).ready(function($) {
        window.print()
    });

</script>