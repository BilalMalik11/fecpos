<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="title">
                        <h4> Purchase Voucher </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Purchase</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> All Purchases</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12">



                    <div class="header-right  float-right">




                        <div class="row">
                            <form action="<?= base_url('purchase/purchase_voucer') ?>" method="post">
                                <button type="submit" class="btn btn-primary mr-2">All</button>
                                <input type="hidden" name="all" value="">
                            </form>
                            <form action="<?= base_url('purchase/purchase_voucer') ?>" method="post">
                                <button type="submit" class="btn btn-primary  mr-2">Stocked</button>
                                <input type="hidden" name="all" value="2">
                            </form>
                            <form action="<?= base_url('purchase/purchase_voucer') ?>" method="post">
                                <button type="submit" class="btn btn-primary">Canceled</button>
                                <input type="hidden" name="all" value="3">
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">

            <div class="pd-20">



                <table class="data-table table stripe hover nowrap">

                    <thead>
                        <tr>
                            <th> S #</th>
                            <th>Date</th>
                            <th>Purchase Id</th>
                            <th>Vendor</th>
                            <th>Created On</th>
                            <th> Total</th>

                            <th class="text-center"> Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $ss = 1;
                        foreach ($purchase as $p) {
                        ?>



                            <tr>
                                <td><?= $ss++ ?></td>

                                <td> <?= date('m/d/Y', strtotime($p->pur_date)); ?> </td>
                                <td><b><?= $p->pur_id ?></b></td>
                                <td><?php if (!empty($p->name)) { ?><?= $p->name ?><?php } else { ?> Walkin<?php } ?></td>
                                <td> <?= date('m/d/Y H:i a', strtotime($p->pur_created_on)); ?> </td>
                                <td> <?= $p->pur_total ?> </td>




                                <td class="text-center"> <?php if ($this->session->userdata('user')['role'] == 1) { ?>
                                        <div class="dropdown">
                                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                <i class="dw dw-more"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                <a class="dropdown-item" href="<?= base_url("purchase/single_purchase/" . $p->pur_id); ?>"><i class="dw dw-eye"></i> View</a>
                                                <a class="dropdown-item" href="<?= base_url("purchase/purchase_print/" . $p->pur_id); ?>"><i class="dw dw-print"></i> Print</a>


                                            </div>
                                        </div> <?php } else { ?>
                                        <div class="dropdown text-right">
                                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                <i class="fa fa-lock"></i>
                                            </a>



                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>

            </div>


        </div>
    </div>




</div>