<style type="text/css">
    .select2-results__option[aria-disabled="true"] {
        display: none;
    }

    .select2-container {
        width: 100% !important;
    }
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <form action="<?= base_url("Purchase/update_purchase/" . $this->uri->segment(3)); ?>" enctype="multipart/form-data" method="post">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-4 col-sm-6">

                        <div class="title">
                            <h4>Single Purchase</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">PURCHASE</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add New</li>
                            </ol>
                        </nav>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <h3>
                            <center><b>Purchase # <?= $this->uri->segment(3); ?></b>
                        </h3>
                        </center>
                    </div>
                    <div class="col-md-5 col-sm-6">
                        <div class="row">

                            <div class="col-md-6">&nbsp;</div>
                            <div class="col-md-6">
                                <label>Date</label>
                                <input type="date" class="form-control" name="purchase_date" value="<?= date('Y-m-d', strtotime($purchase->pur_date)); ?>" />

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Simple Datatable start -->

            <div class="row">
                <div class="col-md-6">
                    <div class="card-box mb-30">
                        <div class="card-header">
                            <h4 class="h4">SEARCH</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>BARCODE</label>
                                        <input type="text" class="form-control inputbarcode" onblur="getproductByCode(this);" />
                                        <input type="hidden" name="price" class="price">
                                        <input type="hidden" name="proname" class="proname">
                                        <input type="hidden" name="hid" class="hid">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control productType select2" onchange="getProductByType(this)">
                                            <option value="">select</option>
                                            <?php foreach ($type as $t) { ?>
                                                <option value="<?= $t->type_id ?>"><?= $t->type_name ?></option>
                                            <?php } ?>


                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>PRODUCTS</label>
                                        <select class="form-control productdata select2" onchange="getProduct(this)">
                                            <option value="">select</option>
                                            <?php foreach ($pro as $p) { ?>
                                                <option data-id="<?= $p->product_id ?>" data-price="<?= $p->purchase_price ?>" data-type="<?= $p->type_id ?>" data-code="<?= $p->barcode ?>" value="<?= $p->product_id ?>"><?= $p->title ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label> &nbsp;</label>
                                        <button type="button" class="btn btn-success btn-block" onclick="appendrow(this)">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-box mb-30">
                        <div class="card-header">
                            <h4 class="h4">Vendor Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Vendors</label>
                                        <select class="form-control select2new vendor" name="vendorid" onchange="getVendors(this)">

                                            <option value="">Add New</option>
                                            <?php foreach ($vendors as $v) { ?>
                                                <option <?php if ($purchase->pur_vendor_id == $v->uid) { ?> selected="" <?php } ?> data-contact="<?= $v->contact ?>" data-email="<?= $v->email ?>" data-address="<?= $v->address ?>" value="<?= $v->uid ?>"><?= $v->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control cus_email" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contact</label>
                                        <input type="text" name="contact" class="form-control cus_contact" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" name="Address" class="form-control cus_address" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="card-box mb-30">
                        <div class="card-header">
                            <h4 class="h4">PRODUCTS</h4>
                        </div>
                        <div class="card-body" style="padding: 0px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Qty</th>
                                                <th>SubTotal</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody">
                                            <?php if (!empty($purchase_item)) { ?>
                                                <?php foreach ($purchase_item as $pt) { ?>
                                                    <tr>
                                                        <td>
                                                            <input type="text" value="<?= $pt->title ?>" readonly="" name="" class="form-control" /> <input type="hidden" value="<?= $pt->puritem_itemid; ?>" readonly="" name="p_id[]" class="form-control" />
                                                        </td>
                                                        <td>
                                                            <input type="number" value="<?= $pt->puritem_price ?>" class="form-control rowprice" name="product_price[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />
                                                        </td>
                                                        <td>
                                                            <input type="number" value="<?= $pt->puritem_qty ?>" class="form-control rowqty" name="product_quantity[]" onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />
                                                        </td>
                                                        <td>
                                                            <input type="number" readonly="" value="<?= $pt->puritem_total ?>" name=subtotal[] class="form-control subtotal" />
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-lg btn-danger" type="button" onclick="removethis(this)"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                    <div class="card-box mb-30">
                        <div class="card-header">
                            <h4 class="h4">Attachment if any <?php if (!empty($purchase->pur_attachment)) { ?><a style="float:right;color:blue;" href="<?= base_url('media/' . $purchase->pur_attachment); ?>" download=""><i class="fa fa-download"></i> </a><?php } ?></h4>
                        </div>
                        <div class="card-body" style="padding: 0px;">
                            <?php if (!empty($purchase->pur_attachment)) { ?>
                                <img src="<?= base_url('media/' . $purchase->pur_attachment); ?>" class="avatar-photo" />
                            <?php } else { ?>
                                <img src="<?= base_url('media/placeholder.png'); ?>" class="avatar-photo" />
                            <?php } ?>
                            <input type="hidden" name="oldimage" value="<?= $purchase->pur_attachment; ?>" />
                            <input type="file" name="file" class="form-control" onchange="displayImg(this);" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="card-box mb-30">
                        <div class="card-header">
                            <h4 class="h4">REMARKS</h4>
                        </div>
                        <div class="card-body" style="padding: 0px;">
                            <textarea class="form-control" name="remarks" style="height: 230px;" rows="6" placeholder="Enter Remarks / Additional Information here..."><?= $purchase->pur_remarks; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-box mb-30">
                        <div class="card-header">
                            <h4 class="h4">STATISTICS</h4>
                        </div>
                        <div class="card-body" style="padding: 0px;">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><b>TOTAL</b></td>
                                        <td><input type="number" name="total" class="form-control gtotal" readonly value="0" /></td>
                                    </tr>
                                    <?php
                                    $paid = 0;
                                    if (!empty($purchase_payment)) {
                                        foreach ($purchase_payment as $pay) {
                                            $paid += $pay->purpay_amount;
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td><b>PAID</b></td>
                                        <td><input type="number" name="paid" readonly="" class="form-control pay" onkeydown="getBalance();" value="<?= $paid; ?>" onkeypress="getBalance();" onblur="getBalance();" /></td>
                                    </tr>
                                    <tr>
                                        <td><b>BALANCE</b></td>
                                        <td><input type="number" class="form-control balance" readonly value="0" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mb-3">
                    <div class="row">
                        <?php if (empty($purchase->is_cancel)) { ?>
                            <?php if (empty($paid) && empty($purchase->is_stock)) { ?>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-block btn-lg btn-success"><i class="fa fa-save"></i> UPDATE INFORMATION</button>
                                </div>
                            <?php } ?>
                            <?php if (empty($purchase->is_stock)) { ?>
                                <div class="col-md-4">
                                    <a href="<?= base_url('purchase/addstock/' . $this->uri->segment(3)); ?>" class="btn btn-block btn-lg btn-warning"><i class="fa fa-truck"></i> STOCK</a>
                                </div>
                            <?php } ?>
                            <?php if (empty($paid) && empty($purchase->is_stock)) { ?>
                                <div class="col-md-4">
                                    <button type="button" data-toggle="modal" data-target="#cancel-modal" class="btn btn-block btn-lg btn-danger"> CANCEL</button>
                                </div>
                            <?php } ?>
                            <?php
                            $balance = $purchase->pur_total - $paid;

                            if (!empty($purchase->is_stock) && $balance > 0) {
                            ?>
                                <div class="col-md-9">
                                    <button type="button" class="btn btn-block btn-lg btn-success" data-toggle="modal" data-target="#payment-modal"> PAY</button>
                                </div>
                            <?php } ?>
                            <?php if (!empty($paid)) { ?>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-block btn-lg btn-warning" data-toggle="modal" data-target="#paymenthistory-modal"> PAYMENT HISTORY</button>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-lg btn-danger" data-toggle="modal" data-target="#cancelreason-modal"> CANCEL REASON</button>
                            </div>
                        <?php } ?>

                        <div class="clearfix">&nbsp;</div>
                    </div>
                </div>

            </div>

        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.vendor').change();
        getSubtotal();
        getBalance();
    });

    function getproductByCode(arg) {
        var barcode = $(arg).val();


        var type_id;
        var p_id;
        var title;
        var pricee;
        if (barcode.trim() != '') {
            $('.productdata').find('option').each(function() {
                if (barcode == $(this).data('code')) {
                    $(this).attr('selected', 'selected');
                    type_id = $(this).data('type');
                    title = $(this).text();
                    pricee = $(this).data('price');
                    p_id = $(this).data('id');
                }
            });
            $('.productdata').trigger('change');


            $('.productType').find('option').each(function() {
                if (type_id == $(this).val()) {
                    $(this).prop('selected', true);
                }
            });
            $('.productType').trigger('change');


            $('.proname').val(title);
            $('.price').val(pricee);
            $('.hid').val(p_id);
        }
    }

    function getProductByType(arg) {
        $('.productdata').val('destory');
        var typeid = $(arg).val();
        if (typeid != '') {
            $('.productdata').find('option').each(function() {
                if (typeid == $(this).data('type')) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).removeAttr('disabled');
                    //                $(this).removeAttr('disabled');
                }
            });
        } else {
            $('.productdata').find('option').each(function() {
                $(this).removeAttr('disabled');
            });
        }
        $('.productdata').select2("destroy");
        $('.productdata').select2();
    }

    function getProduct(arg) {

        var barcode = $(arg).find('option:selected').data('code')
        var p_id = $(arg).find('option:selected').data('id')
        var price = $(arg).find('option:selected').data('price')
        var title = $(arg).find('option:selected').text()
        $('.inputbarcode').val(barcode)
        $('.proname').val(title)
        $('.price').val(price)
        $('.hid').val(p_id)


        var typeid = $(arg).find('option:selected').data('type');
        $('.productType').find('option').each(function(index, el) {
            if (typeid == $(this).val()) {
                $(this).attr('selected', 'selected');
            } else {
                $(this).removeAttr('selected', 'selected');
            }

        });

    }

    function appendrow() {
        var title = $('.proname').val()
        var price = $('.price').val()
        var id = $('.hid').val();
        var tr = ' <tr>  <td>  <input type="text" value="' + title + '" readonly="" name="" class="form-control" />  <input type="hidden" value="' + id + '" readonly="" name="p_id[]" class="form-control" />          </td><td><input type="number"  value="' + price + '" class="form-control rowprice" name="product_price[]"  onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />                  </td> <td><input type="number" value="1" class="form-control rowqty" name="product_quantity[]"  onblur="getSubtotal();" onkeyup="getSubtotal();" onkeydown="getSubtotal();" />    </td> <td><input type="number" readonly="" value="' + price + '" name=subtotal[] class="form-control subtotal" />     </td> <td><button class="btn btn-lg btn-danger" type="button" onclick="removethis(this)"><i class="fa fa-trash"></i></button>   </td> </tr>'

        if ($('.productdata').val() != '') {
            $('.tbody').append(tr)
            getSubtotal();
        } else {
            swal("Error", "Please Select a Product  !!", "error");
        }
    }

    function removethis(arg) {

        $(arg).closest('tr').remove();
        getSubtotal();
    }

    function getSubtotal() {
        var total = 0;
        $('.rowprice').each(function() {
            var rowprice = $(this).closest('tr').find('.rowprice').val();
            var rowqty = $(this).closest('tr').find('.rowqty').val();
            $(this).closest('tr').find('.subtotal').val(rowprice * rowqty);
            total += rowprice * rowqty;
        });
        $('.gtotal').val(total);
    }

    function getBalance() {
        var gtotal = $('.gtotal').val();
        var pay = $('.pay').val();
        $('.balance').val(gtotal - pay);
    }

    function getVendors(arg) {


        var contact = $(arg).find('option:selected').data('contact');
        var email = $(arg).find('option:selected').data('email');
        var address = $(arg).find('option:selected').data('address');


        if ($(arg).find('option:selected').val() != '') {
            if ($.isNumeric($(arg).find('option:selected').val())) {
                $('.cus_email').prop('readonly', true);
                $('.cus_contact').prop('readonly', true);
                $('.cus_address').prop('readonly', true);

                $('.cus_email').val(email);
                $('.cus_contact').val(contact);
                $('.cus_address').val(address);
            } else {
                $('.cus_email').prop('readonly', false);
                $('.cus_contact').prop('readonly', false);
                $('.cus_address').prop('readonly', false);

                $('.cus_email').val('');
                $('.cus_contact').val('');
                $('.cus_address').val('');
            }
        } else {


            $('.cus_email').prop('readonly', false);
            $('.cus_contact').prop('readonly', false);
            $('.cus_address').prop('readonly', false);

            $('.cus_email').val('');
            $('.cus_contact').val('');
            $('.cus_address').val('');
        }


    }
</script>
<div class="modal fade  " id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">PAYMENT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="<?= base_url('purchase/paybill/' . $this->uri->segment(3)); ?>" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>AMOUNT</label>
                                <input type="number" class="form-control amountentertd" name="amount" />
                                <input type="hidden" class="form-control totalamounthastob" name="total" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick=" $('.totalamounthastob').val($('.balance').val() - $('.amountentertd').val())" type="submit" class="btn btn-primary">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade  " id="cancel-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger">Are you sure you want to cancel this voucher?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="<?= base_url('purchase/cancelbill/' . $this->uri->segment(3)); ?>" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>REASON</label>
                                <textarea class="form-control" name="reason"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Yes Proceed!</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade  " id="cancelreason-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger">Cancelled By: <?php
                                                                    $use = $this->db->where('uid', $purchase->cancel_by)->get('tbl_user')->row();
                                                                    //                    echo $use->name;
                                                                    ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <b class="text-danger"><?= $purchase->cancel_reason; ?></b>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <small>Cancelled On : <?= date('d M Y', strtotime($purchase->cancel_on)); ?></small>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade  " id="paymenthistory-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-success">PAYMENTS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body" style="padding:0px;">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Created By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($purchase_payment as $pp) { ?>
                                    <tr>
                                        <td><?= date('d M Y', strtotime($pp->payment_on)); ?></td>
                                        <td><?= $pp->purpay_amount; ?></td>
                                        <td><?= $pp->name; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>