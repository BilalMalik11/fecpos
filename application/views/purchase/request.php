<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Request Quote</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Purchase</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Request Quote</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-6 col-sm-12 mb-30 btn" onclick="window.location.href = '<?= base_url(''); ?>'">
                <div class="card card-box text-center ">
                    <div class="card-body">
                        <h1 class="card-title">Assign Product to Vendors</h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-30 btn" onclick="window.location.href = '<?= base_url(''); ?>'">
                <div class="card card-box text-center">
                    <div class="card-body">
                        <h1 class="card-title">Generate Request for Quote</h1>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
