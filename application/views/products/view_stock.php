<style type="text/css">
    .select2-results__option[aria-disabled="true"]{
        display:none;
    }
    .select2-container{
        width: 100% !important;
    }
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="title">
                        <h4>Product  </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Stock View</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="row">
                        <div class="col-md-6">&nbsp;</div>
                        <div class="col-md-6">
                           <!--  <input type="text" class="form-control" placeholder="Search Product...."/> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-list">
            <ul class="row">
                <?php if (!empty($pro)) { ?>
                    <?php foreach ($pro as $prd) { ?>
                        <li class="col-lg-2 col-md-3 col-sm-12">
                            <div class="product-box">
                                <div class="producct-img">
                                    <?php if (!empty($prd['proimage'][0]->img_url)) { ?>
                                        <img style="height:250px;" src="<?= base_url('media/products/' . $prd['proimage'][0]->img_url); ?>" alt="">
                                    <?php } else { ?>
                                        <img style="height:500px;" src="media/placeholder.png" alt="">
                                    <?php } ?>
                                </div>
                                <div class="product-caption">
                                    <h4><a href="#"><?= $prd['product']->title ?></a></h4>
                                    <div class="price">
                                        <b>Purchase Price: <?= $prd['product']->purchase_price ?></b>
                                        <b>Sale Price: <?= $prd['product']->sale_price ?> </b>
                                    </div>
                                    <a href="#" class="btn btn-outline-danger" disabled >IN STOCK: <?= $prd['stock']; ?></a> 
                                    <a href="<?= base_url('Products/edit_product/' . $prd['product']->product_id); ?>" class="btn btn-outline-primary">Edit Product</a>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="blog-pagination mb-30">
            <div class="btn-toolbar justify-content-center mb-15">
                <div class="btn-group">
                    <a href="#" class="btn btn-outline-primary prev"><i class="fa fa-angle-double-left"></i></a>
                    <a href="#" class="btn btn-outline-primary">1</a>
                    <a href="#" class="btn btn-outline-primary">2</a>
                    <span class="btn btn-primary current">3</span>
                    <a href="#" class="btn btn-outline-primary">4</a>
                    <a href="#" class="btn btn-outline-primary">5</a>
                    <a href="#" class="btn btn-outline-primary next"><i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div> 
