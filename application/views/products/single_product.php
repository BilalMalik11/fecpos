<style type="text/css">
    .select2-results__option[aria-disabled="true"]{
        display:none;
    }
    .select2-container{
        width: 100% !important;
    }
</style>
<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
       
            <div class="page-header">
                <div class="row">
                    <div class="col-md-4 col-sm-6">

                        <div class="title">
                            <h4>Product</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Products</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Single Product</li>
                            </ol>
                        </nav>
                    </div>
                  
                  

                </div>
            </div>
 
            <div class="product-wrap">
                    <div class="product-detail-wrap mb-30">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="product-slider slider-arrow">
                                    <?php foreach ($pro['proimage'] as $img) {?>
                                         
                                  
                                    <div class="product-slide">
                                        <img src="<?= base_url('media/products/').$img->img_url ?>" alt="">
                                    </div>
                                <?php } ?>
                                    
                                </div>
                                <div class="product-slider-nav">
                                     <?php foreach ($pro['proimage'] as $img) {?>
                                         
                                    <div class="product-slide-nav">
                                         <img src="<?= base_url('media/products/').$img->img_url ?>" alt="">
                                    </div>
                                     <?php } ?>
                                      
                                     
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="product-detail-desc pd-20 card-box height-100-p">
                                    <h4 class="mb-20 pt-20"><?= $pro['product']->title?></h4>
                                    <ul>
            <li> <strong>Type   :</strong> <?= $pro['product']->type_name?></li>

            <li> <strong>Category    :</strong> <?= $pro['product']->category_name?></li>
            <li> <strong>Purchase Price   :</strong> <?= $pro['product']->purchase_price?></li>
            <li> <strong>Sale Price   :</strong> <?= $pro['product']->sale_price?></li>                     
           <li> <strong>Color   :</strong> <button class="btn btn-sm" style="height: 20px; width: 50px; background: <?= $pro['product']->color?>" ></button> </li>
       </ul>
                               <br>
                               <br>
                               <br>
                               <!--      <div class="mx-w-150">
                                        <div class="form-group">
                                            <label class="text-blue">quantity</label>
                                            <input id="demo3_22" type="text" value="1" name="demo3_22">
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-md-6 col-6">
                                            <a href="<?= base_url('Products/edit_product/').$pro['product']->product_id ?>" class="btn btn-primary btn-block">Edit</a>
                                        </div>
                                        <div class="col-md-6 col-6">
                                            <a href="#" class="btn btn-outline-primary btn-block">Stock View</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                </div>
    </div>
</div> 


 <script>
        jQuery(document).ready(function() {
            jQuery('.product-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: true,
                speed: 1000,
                fade: true,
                asNavFor: '.product-slider-nav'
            });
            jQuery('.product-slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.product-slider',
                dots: false,
                infinite: true,
                arrows: false,
                speed: 1000,
                centerMode: true,
                focusOnSelect: true
            });
            $("input[name='demo3_22']").TouchSpin({
                initval: 1
            });
        });
    </script>
   