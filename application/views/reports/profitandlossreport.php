<?php
$payments = 0;
if (!empty($purchase_payment)) {
    foreach ($purchase_payment as $pp) {
        $payments += $pp->salpay_amount;
    }
}



?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style type="text/css">
    @media print {
        .header {
            display: none;
        }

        .footer-wrap {
            display: none;
        }

        .noprint {
            display: none;

        }
    }
</style>

<body onload="window.print();">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="invoice-wrap">
                <a href="<?= base_url('Products/sales_report') ?>" class="btn btn-primary pull-right noprint">New Search</a>
                <div class="btn btn-success noprint" onclick="window.print();">PRINT</div>

                <div class="invoice-box">
                    <h4 class=" mb-5 text-center"> Profit & Loss Report </h4>
                    <p class="font-14 mb-5 text-right">Date:<?= date('d M Y ') ?> </p>
                    <div class="invoice-header ">


                        <div class="  logo text-<?= $settings->logo_set_align; ?> ">
                            <img style="max-width: 150px !important;" src="<?= base_url('media/logo/' . $settings->logo_set_logo); ?>" alt="">

                        </div>

                    </div>
                    <h4 class="text-center   weight-600"><?= $settings->app_name; ?></h4>
                    <div class="col-md-12">
                        <div class="text-center">

                            <p class="font-14 mb-1"><?= $settings->logo_set_address; ?></p>
                            <p class="font-14 mb-1"><?= $settings->logo_set_contact; ?></p>
                            <p class="font-14 mb-1"><?= $settings->logo_set_email; ?></p>
                        </div>
                    </div>
                    <div class="row pb-30">
                        <!--         <div class="col-md-12 text-right">
                        
                        <b class="mb-15"><small> Receipt # <?= $this->uri->segment(3); ?></small></b> <br>
                        <b class="mb-15"><small> Customer ID:<?php if (!empty($purchase_vendor->customer_id)) { ?> <?= $purchase_vendor->customer_id; ?> <?php }  ?></small> </b> <br>  
                  
                    </div> -->
                    </div>
                    <div class="invoice-desc">
                        <table class="  table">
                            <div class="invoice-desc-head clearfix">

                                <tr>

                                    <th style="font-size: larger;">Total Purchase </th>
                                    <th style="font-size: larger;">Total Sales</th>
                                    <th style="font-size: larger;">Total Expense</th>



                                </tr>

                                <!-- <div class="invoice-  ">Product</div>
                        <div class="invoice-sub">Date</div>
                        <div class="invoice-rate">Discount</div>
                        <div class="invoice-hours">Quantity</div>
                        <div class="invoice-subtotal">Subtotal</div> -->
                            </div>
                            <div class="invoice-desc-body" style="min-height:unset;">

                                <?php
                                $grandtotal = 0;
                                $discount = 0;
                                if (!empty($pur)) {
                                ?>



                                    <tr>

                                        <td> <?= $pur->totalpriceofpurchse; ?></td>
                                        <td> <?= $sale->totalpriceofsell; ?></td>


                                        <td> <?= $heads->ttlheadamunt; ?></td>




                                    </tr>

                                <?php
                                    //  $grandtotal += $pi->puritem_total;
                                }
                                ?>

                                <tr> &nbsp;

                                    <td></td>
                                    <td><b style="font-size: larger;">Total Purchase :</b></td>
                                    <td class="text-right"><b> <?= $pur->totalpriceofpurchse; ?></b></td>

                                </tr>
                                <tr class="mt-5" style="margin-top: 20px;">
                                    &nbsp;

                                    <td></td>
                                    <td> <b style="font-size: larger;"> Total Sales:</b></td>
                                    <td class="text-right"><b> <?= $sale->totalpriceofsell  ?></b></td>

                                </tr>


                                <tr> &nbsp;

                                    <td></td>
                                    <td><b style="font-size: larger;"> Sale Profit <small>(Sale - Purchase)</small> : </b></td>
                                    <td class="text-right"> <b> <?= $profit =  $sale->totalpriceofsell - $pur->totalpriceofpurchse    ?></b></td>

                                </tr>
                                <tr> &nbsp;

                                    <td></td>
                                    <td><b style="font-size: larger;"> Total Expenses:</b></td>
                                    <td class="text-right"> <b> <?= $heads->ttlheadamunt ?></b></td>

                                </tr>
                                <tr> &nbsp;
                                    <?php $profitall = 0;
                                    $loss = 0;
                                    if ($profit - $heads->ttlheadamunt > 0) {
                                        $profitlos = $profit - $heads->ttlheadamunt;
                                    } else {
                                        $loss =  $profit - $heads->ttlheadamunt;
                                    } ?>
                                    <td></td>
                                    <td><b style="font-size: larger;"> Loss <small>(Sale Profit - Expenses)</small>:</b></td>
                                    <td class="text-right"><b> <?= $loss   ?></td>

                                </tr>
                                <tr> &nbsp;

                                    <td></td>
                                    <td><b style="font-size: larger;"> Gain <small>(Sale Profit - Expenses)</small>:</b></td>
                                    <td class="text-right"><b> <?= $profitall  ?></td>

                                </tr>
                                <tr> &nbsp;

                                    <td></td>
                                    <td><b style="font-size: larger;"> Over All Profit <small>(Sale Profit - Expenses)</small>:</b></td>
                                    <td class="text-right"><b> <?= $profitall    ?></td>

                                </tr>

                        </table>


                        <hr>

                        <h6 class="text-right"><small>Power By</small> Effiesoft</h6>
                    </div>

                </div>

            </div>

        </div>

    </div>
    </div>
</body>

</html>