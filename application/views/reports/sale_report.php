<style type="text/css">
    @media print {
        .header {
            display: none;
        }

        .footer-wrap {
            display: none;
        }

        .noprint {
            display: none;

        }
    }
</style>


<!-- ========================================= -->

<div class="pd-ltr-20 xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Reports </h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Sales</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Report</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <!-- Simple Datatable start -->
        <div class="card-box mb-30">
            <div class="pd-20">
                <div id="accordion">
                    <div class="card mt-4">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="text-primary">Sales Report</h4>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <form method="post" action="<?= base_url('Products/getsalreportdata') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card mt-4">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <h4 class="text-primary">Purchase Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion">
                            <form method="post" action="<?= base_url('Products/getpurreportdata') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card mt-4">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <h4 class="text-primary">Profit Lost Statement</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                            <form method="post" action="<?= base_url('products/getprofitlossstt') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>

                    <br>
                    <br>
                    <div class="card mt-4">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    <h4 class="text-primary"> Credit Debit Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordion">
                            <form method="post" action="<?= base_url('products/purchasepaymenthistory') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card mt-4">
                        <div class="card-header" id="headingSix">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                    <h4 class="text-primary">Receivable Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#accordion">
                            <form method="post" action="<?= base_url('products/receivable') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- <div class="card mt-4">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <h4 class="text-primary">Purchase Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion">
                            <form method="post" action="<?= base_url('reports/purchase') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
            

                      <div class="card mt-4">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                    <h4 class="text-primary">Payable Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFive" class="collapse show" aria-labelledby="headingFive" data-parent="#accordion">
                            <form method="post" action="<?= base_url('reports/paybill') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
                      <div class="card">
                        <div class="card-header" id="headingSix">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                    <h4 class="text-primary">Receivable Report</h4>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#accordion">
                            <form method="post" action="<?= base_url('reports/receivable') ?>">
                                <div class="card-body row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <input type="date" name="fromdate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <input type="date" name="todate" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Search</button>

                                </div>
                            </form>
                        </div>
                    </div> -->




                    <!-- <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <h4 class="text-primary text-center">Purchase Report</h4>
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
          <form method="post" action="<?= base_url('Products/getpurreportdata') ?>" >
      <div class="card-body row">
      <div class="col-md-6">
                        <div class="form-group">
                            <label>From Date</label>
                            <input type="date"  name="fromdate" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>To Date  </label>
                            <input type="date" name="todate" class="form-control">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block"  >Search</button>
      </div>
  </form>
    </div>
  </div> -->
                    <!--   <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
           
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body row">
     <div class="col-md-6">
                        <div class="form-group">
                            <label>From Date</label>
                            <input type="date" name="fromdate" id="fromdate" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>To Date  </label>
                            <input type="date" name="todate" id="todate" class="form-control">
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-block" onclick="bringreport()">Search</button>
      </div>
    </div>
  </div> -->
                </div>




            </div>





        </div>
    </div>

</div>



<script>
    //    function bringreport() {
    //     var from = $('#fromdate').val()
    //     var to = $('#todate').val()

    //     var url = "<?= base_url('Products/getsalreportdata'); ?>"

    //     $.ajax({
    //         url: url,
    //         type: 'POST',
    //         dataType: 'text',
    //         data: {from: from,
    //             to: to},
    //         success:
    //                 function (data) {
    //                     alert(data)
    //                 }
    //     });


    // }
</script>