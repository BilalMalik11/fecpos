<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_p extends CI_Model
{

    public function create($table_name, $table_field_name_with_value)
    {
        $result = $this->db->insert($table_name, $table_field_name_with_value);
        if (!$result) {
            return 0;
        } else {
            return $this->db->insert_id();
        }
    }
    public function createtbl($table, $data)
    {
        $this->db->insert($table, $data);
        return  $this->db->insert_id();
    }
    public function createuser($table, $data)
    {
        $this->db->insert($table, $data);
        return  $this->db->insert_id();
    }

    public function get_global($table_name)
    {
        return $this->db->get($table_name)->result();
    }
    public function chek_global($table_name)
    {
        return $this->db->get($table_name)->row();
    }

    public function get_globalMultiWithCond($table_name, $table_field_name_with_value, $inst_id)
    {
        return $this->db->where($table_field_name_with_value)->where($inst_id)->get($table_name)->result();
    }

    public function get_globalSingWithCond($table_name, $table_field_name_with_value, $inst_id)
    {
        return $this->db->where($table_field_name_with_value)->where($inst_id)->get($table_name)->row();
    }

    public function countRows($table)
    {
        return $this->db->count_all_results($table);
    }

    public function updateRecord($table_name, $table_field_name, $table_field_data)
    {



        return $this->db->where($table_field_name)->update($table_name, $table_field_data);
    }

    public function delete($table_name, $table_field_name_with_value)
    {
        return $this->db->where($table_field_name_with_value)->delete($table_name);
    }

    public function userlogin($table, $u)
    {
        return $this->db->where('user', $u)->get($table)->row();
    }

    //     public function uploadprdPic($id) {
    //         // $count = 0;
    //         $record = $this->db->count_all_results('product');
    //         if ($id != '') {
    //             $record = $id;
    //         }
    //         $file = '';
    //         $adver = '';
    //         if ($_FILES['file']['name'] != '') {
    //             // $new =
    //             $path = $_FILES['file']['name'];
    //             // $new_name = date('s') . "prdImg" . "." . pathinfo($path, PATHINFO_EXTENSION);
    //             $path = $_FILES['file']['name'];
    //             // mkdir("Proposals/". $_SESSION["FirstName"] ."/");
    //             // $imgName    = rand(99,999);
    //             // $hash_img   = $this->password->hash($imgName);
    //             $imgName = $this->random_num(10);
    //             $hash_img = substr($imgName, 0, 4);
    //             $final_name = $hash_img . date('s');
    //             $new_name = $final_name . "." . pathinfo($path, PATHINFO_EXTENSION);
    //             // $new_name = time() . str_replace(' ', '-', $_FILES["file"]['name']);
    // // $trackingID = rand(99, 999).date('s');
    // // do
    // // {
    // // $trackingID = rand(99, 999).date('s');
    // // $tracking_record = $this->db->where('order_tracking_id',$trackingID)->get('orders')->row();
    // // }
    // // while(!empty($tracking_record));
    //             // $rand  =  chr(rand(97, 122)). chr(rand(97, 122)). chr(rand(97, 122));
    //             $rand = 'prdImg' . $record;
    //             $fldr = realpath(APPPATH . '../img_uploads/product_images/');
    //             if (!is_dir($fldr . '/' . $rand)) {
    //                 mkdir($fldr . '/' . $rand);
    //             }
    //             $adver = $fldr . '/' . $rand . '/';
    //             $config = [
    //                 'upload_path' => $adver,
    //                 'allowed_types' => 'gif|jpeg|jpg|png',
    //                 'remove_spaces' => TRUE,
    //                 'image_library' => 'gd2',
    //                 'quality' => 60,
    //                 'file_name' => $new_name
    //             ];
    //             $this->load->library('upload', $config);
    //             $this->upload->initialize($config);
    //             if ($this->upload->do_upload('file')) {
    //                 $file = $rand . '|' . $new_name;
    //             } else {
    //                 $file = $this->upload->display_errors();
    //             }
    //         }
    //         return $file;
    //     }
    // this function will recieved directory name and store in that particular directory and return image name

    public function upload($dir_name)
    {
        $file = '';
        if ($_FILES['file']['name'] != '') {
            $new_name = date('s') . str_replace(' ', '', $_FILES["file"]['name']);
            $adver = realpath(APPPATH . '../img_uploads/' . $dir_name . '/');
            $config = [
                'upload_path' => $adver,
                'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
                'remove_spaces' => true,
                'image_library' => 'gd2',
                'quality' => 60,
                'file_name' => $new_name
            ];

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $file = $new_name;
            } else {
                $file = $this->upload->display_errors();
            }
        }
        return $file;
    }

    //     public function uploadNew($module_id) {
    //         $file = '';
    //         if ($_FILES['file']['name'] != '') {
    //             $type = '';
    //             $imgArr = explode('.', $_FILES["file"]['name']);
    //             if (sizeof($imgArr) > 0) {
    //                 for ($i = 0; $i < sizeof($imgArr); $i++) {
    //                     if ($i == (sizeof($imgArr) - 1)) {
    //                         $type = $imgArr[$i];
    //                     }
    //                 }
    //             }
    //             $new_name = rand(0, 20);
    // //          $new_name=shuffle($randdd);
    //             $new_name .= date('Ynjs');
    //             $dirToModule = realpath(APPPATH . '../media/' . $this->session->userdata('user')['user_instance'] . '/' . $module_id);
    //             $year = date('Y');
    //             $month = date('n');
    //             $day = date('j');
    //             $path = $dirToModule . '/' . $year;
    //             if (!is_dir($path)) {
    //                 mkdir($path);
    //             }
    //             $path = $dirToModule . '/' . $year . '/' . $month;
    //             if (!is_dir($path)) {
    //                 mkdir($path);
    //             }
    //             $path = $dirToModule . '/' . $year . '/' . $month . '/' . $day;
    //             if (!is_dir($path)) {
    //                 mkdir($path);
    //             }
    //             $config = [
    //                 'upload_path' => $path,
    //                 'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|pdf|doc',
    //                 'remove_spaces' => true,
    //                 'image_library' => 'gd2',
    //                 'quality' => 60,
    //                 'file_name' => $new_name
    //             ];
    //             $this->load->library('upload', $config);
    //             $this->upload->initialize($config);
    //             if ($this->upload->do_upload('file')) {
    //                 $file = $new_name . '.' . $type;
    //             } else {
    //                 $file = $this->upload->display_errors();
    //             }
    //         }
    //         return $file;
    //     }
    //     public function uploadMultiGalleryImgs($folder) {
    //         $file = '';
    //         if ($_FILES['file']['name'] != '') {
    //             $type = '';
    //             $imgArr = explode('.', $_FILES["file"]['name']);
    //             if (sizeof($imgArr) > 0) {
    //                 for ($i = 0; $i < sizeof($imgArr); $i++) {
    //                     if ($i == (sizeof($imgArr) - 1)) {
    //                         $type = $imgArr[$i];
    //                     }
    //                 }
    //             }
    //             $new_name = rand(0, 20);
    // //          $new_name=shuffle($randdd);
    //             $new_name .= date('Ynjs');
    //             $dirToModule = realpath(APPPATH . '../img_uploads/' . $folder);
    //             $config = [
    //                 'upload_path' => $dirToModule,
    //                 'allowed_types' => 'gif|jpeg|jpg|png|mpeg|mpg|mp4|mov|avi|flv|wmv|',
    //                 'remove_spaces' => true,
    //                 'image_library' => 'gd2',
    //                 'quality' => 60,
    //                 'file_name' => $new_name
    //             ];
    //             $this->load->library('upload', $config);
    //             $this->upload->initialize($config);
    //             if ($this->upload->do_upload('file')) {
    //                 $file = $new_name . '.' . $type;
    //             } else {
    //                 $file = $this->upload->display_errors();
    //             }
    //         }
    //         return $file;
    //     }
    //     //this function will received three paramerter first one dirctory name and second one table name and thrid table field data
    //     public function multipleUploads($dirName, $table_name, $table_field_data) {
    //         $adver = realpath(APPPATH . "../user_uploads/" . $dirName);
    //         $this->load->library('upload');
    //         $files = $_FILES;
    //         $cpt = count($_FILES['file']['name']);
    //         $img_array = [];
    //         for ($i = 0; $i < $cpt; $i++) {
    //             echo $i;
    //             $_FILES['file']['name'] = $files['file']['name'][$i];
    //             $_FILES['file']['type'] = $files['file']['type'][$i];
    //             $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];
    //             $_FILES['file']['error'] = $files['file']['error'][$i];
    //             $_FILES['file']['size'] = $files['file']['size'][$i];
    //             if (!$_FILES["file"]['name']) {
    //                 $new_name = '';
    //             } else {
    //                 $new_name = date('s') . str_replace(' ', '', $_FILES["file"]['name']);
    //             }
    //             $config = [
    //                 'upload_path' => $adver,
    //                 'allowed_types' => 'gif|png|jpg|jpeg',
    //                 'remove_spaces' => TRUE,
    //                 'image_library' => 'gd2',
    //                 'quality' => 60,
    //                 'file_name' => $new_name
    //             ];
    //             $this->upload->initialize($config);
    //             if ($this->upload->do_upload('file')) {
    //                 $table_field_data['image_name'] = $new_name;
    //                 $this->db->insert($table_name, $table_field_data);
    //             }
    //         }
    //         return 1;
    //     }



    public function select_same($item_name, $color, $size, $art_no)
    {
        $this->db->select('*');
        $this->db->where('item_name', $item_name);
        $this->db->where('color', $color);
        $this->db->where('size', $size);
        $this->db->where('article_no', $art_no);
        $query = $this->db->get("item");
        //echo $this->db->last_query();
        $result = $query->num_rows();
        if ($result > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    public function getsaledata($id)
    {
        return $this->db->join('stock', 'stock.item_id = item.item_id')
            ->where('item.is_trash', 0)
            ->where('item.instant_id', $this->session->userdata('user')['inst_id'])

            ->where('item.item_id', $id)
            ->get('item')->row();
    }

    public function getproducts()
    {

        $data =  $this->db->join('category', 'category.category_id = products.category_id', 'left')
            ->join('tbl_type', 'tbl_type.type_id = products.type_id', 'left')
            ->where('products.is_trash', 0)
            ->where('products.instant_id', $this->session->userdata('user')['inst_id'])

            ->get('products')->result();
        if (!empty($data)) {
            foreach ($data as $products) {

                $result[$products->product_id]['product']  = $products;
                $result[$products->product_id]['proimage']  = $this->db->where('product_id', $products->product_id)->get('product_img')->result();
            }
            return $result;
        }
    }

    public function getsingleproduct($id)
    {
        $data =  $this->db
            ->join('tbl_type', 'tbl_type.type_id = products.type_id', 'left')
            ->join('category', 'category.category_id = products.category_id', 'left')
            ->where('products.is_trash', 0)
            ->where('products.instant_id', $this->session->userdata('user')['inst_id'])

            ->where('products.product_id', $id)
            ->get('products')->row();


        $result['product']  = $data;
        $result['proimage']  = $this->db->where('product_id', $data->product_id)->get('product_img')->result();

        return $result;
    }




    public function getVendors()
    {

        return $this->db->join('user_roles', 'user_roles.role_id = tbl_user.role', 'left')
            ->where('tbl_user.user_type', 2)
            ->where('tbl_user.is_trash', 0)
            ->where('tbl_user.instant_id', $this->session->userdata('user')['inst_id'])

            ->get('tbl_user')->result();
    }

    public function getAdmin()
    {

        return $this->db->join('user_roles', 'user_roles.role_id = tbl_user.role', 'left')
            ->where('tbl_user.user_type', 1)
            ->where('tbl_user.is_trash', 0)
            ->where('tbl_user.instant_id', $this->session->userdata('user')['inst_id'])


            ->get('tbl_user')->result();
    }
    public function getsingleuser($id)
    {

        return $this->db->join('user_roles', 'user_roles.role_id = tbl_user.role', 'left')
            ->where('tbl_user.uid', $id)
            ->where('tbl_user.is_trash', 0)
            ->where('tbl_user.instant_id', $this->session->userdata('user')['inst_id'])

            ->get('tbl_user')->row();
    }

    public function getpaidproducts($id)
    {
        return $this->db->join('products', 'products.product_id = sale_items.salitem_itemid', 'left')
            ->where('sale_items.salitem_saleid', $id)
            ->get('sale_items')->result();
    }
    public function getPurchaseItems($id)
    {
        return $this->db->join('products', 'products.product_id = purchase_items.puritem_itemid', 'left')
            ->where('purchase_items.puritem_purid', $id)
            ->get('purchase_items')->result();
    }

    public function getAllsales()
    {
        return $this->db->join('tbl_user', 'tbl_user.uid = sale.sale_cust_id', 'left')
            ->where('sale.instant_id', $this->session->userdata('user')['inst_id'])

            ->get('sale')->result();
    }

    public function getAllpurchases()
    {
        return $this->db->join('tbl_user', 'tbl_user.uid = purchase.pur_vendor_id', 'left')
            ->where('purchase.instant_id', $this->session->userdata('user')['inst_id'])
            ->get('purchase')->result();
    }
    public function getAllpurchasesstocked()
    {
        return $this->db->join('tbl_user', 'tbl_user.uid = purchase.pur_vendor_id', 'left')
            ->where('is_stock', 1)
            ->where('purchase.instant_id', $this->session->userdata('user')['inst_id'])

            ->get('purchase')->result();
    }
    public function getAllpurchasescanceled()
    {
        return $this->db->join('tbl_user', 'tbl_user.uid = purchase.pur_vendor_id', 'left')
            ->where('is_cancel', 1)
            ->where('purchase.instant_id', $this->session->userdata('user')['inst_id'])

            ->get('purchase')->result();
    }
    public function getpurchasess($stock_id)
    {
        return $this->db->join('tbl_user', 'tbl_user.uid = purchase.pur_vendor_id', 'left')
            ->where('is_stock', $stock_id)
            ->where('purchase.instant_id', $this->session->userdata('user')['inst_id'])

            ->get('purchase')->result();
    }
    public function getPurchasesPayments($stock_id)
    {
        return $this->db->join('tbl_user', 'tbl_user.uid = purchase_payment.payment_by', 'left')
            ->where('pur_purchase_id', $stock_id)
            ->where('purchase_payment.instant_id', $this->session->userdata('user')['inst_id'])
            ->get('purchase_payment')->result();
    }
    public function getSalePayments($stock_id)
    {
        return $this->db->join('tbl_user', 'tbl_user.uid = sale_payment.salpay_by', 'left')

            ->where('instant_id', $this->session->userdata('user')['inst_id'])
            ->where('sal_sale_id', $stock_id)
            ->get('sale_payment')->result();
    }

    public function getsaleAvgItems()
    {


        $data =   $this->db->select('*  , SUM(salitem_qty) as total_qty')
            ->join('products', 'products.product_id = sale_items.salitem_itemid')

            ->where('sale_items.instant_id', $this->session->userdata('user')['inst_id'])
            ->order_by('total_qty', 'desc')
            ->limit(10)
            ->group_by('product_id')
            ->get('sale_items')->result();
        $result = [];
        foreach ($data as $d) {
            $result[$d->salitem_id]['product'] = $d;
            $result[$d->salitem_id]['img'] = $this->db->where('product_img.product_id', $d->product_id)->get('product_img')->row();
        }
        return $result;
    }
    public function gettodaysale()
    {
        return    $this->db->where('sale_date', date('Y-m-d'))
            ->get('sale')->result();
    }

    public function getNumproduts()
    {


        $purchase =   $this->db->select(' SUM(puritem_qty) as puchse_qty')

            ->get('purchase_items')->row();

        $sale =   $this->db->select(' SUM(salitem_qty) as sell_qty')


            ->get('sale_items')->row();

        return $total = $purchase->puchse_qty - $sale->sell_qty;


        // foreach ($data as $d) {
        //      $result[$d->salitem_id]['product'] = $d;
        //      $result[$d->salitem_id]['img'] = $this->db->where( 'product_img.product_id' , $d->product_id  )->get('product_img')->row();

        // }
        // return $result;

    }
    public function getAllexpense()
    {
        $data =   $this->db->select('*, chart_of_account.is_update as updatethis')->join('coa_subtype', 'coa_subtype.coa_subtype_id = chart_of_account.subtype_id')
            ->join('coa_type', 'coa_type.coa_type_id = coa_subtype.coa_subtype_typeid')
            ->where('chart_of_account.is_trash', 0)
            ->where('chart_of_account.instant_id', $this->session->userdata('user')['inst_id'])
            ->get('chart_of_account')->result();
        return $data;
    }

    public function getheadsrepot($type, $subtype, $name, $from, $to)
    {

        $data = $this->db->join('coa_subtype', 'coa_subtype.coa_subtype_id = chart_of_account.subtype_id')
            ->join('coa_type', 'coa_type.coa_type_id = coa_subtype.coa_subtype_typeid');
        if (!empty($from)) {
            $data = $this->db->where('date >=', $from);
        }
        if (!empty($to)) {
            $data = $this->db->where('date <=', $to);
        }
        if (!empty($subtype)) {
            $data = $this->db->where('subtype_id', $subtype);
        }
        if (!empty($to)) {
            $data = $this->db->where('coa_type.coa_type_id', $type);
        }
        if (!empty($name)) {


            $data =   $this->db->like('coa_name', $name);;
        }
        $data = $this->db->where(
            'instant_id',
            $this->session->userdata('user')['inst_id']
        );

        $data = $this->db->get('chart_of_account')->result();
        return $data;
    }

    // public function getallheadsrepot($type , $subtype , $name , $from , $to){
    //     $data = $this->db->select('* , sum(amount) as totalamount')->join('coa_subtype', 'coa_subtype.coa_subtype_id = chart_of_account.subtype_id')
    //    ->join('coa_type', 'coa_type.coa_type_id = coa_subtype.coa_subtype_typeid');
    //    if(!empty($from)){
    //  $data = $this->db->where('date >' , $from )      ;
    //    }
    //  if(!empty($to)){
    //  $data = $this->db->where('date <' , $to )      ;
    //    }
    //          if(!empty($subtype)){
    //  $data = $this->db->where('subtype_id' , $subtype )      ;
    //    }
    //            if(!empty($to)){
    //  $data = $this->db->where('coa_type.coa_type_id' , $type )      ;
    //    }
    //                   if(!empty($name)){


    //  $data =   $this->db->like('chart_of_account.coa_name', $name); ;
    //    }


    //     $data = $this->db->get('chart_of_account')->result();
    //     return $data;

    // }

    public function getPurchasesPaymentsall($from, $to)
    {
        $data =  $this->db
            ->join('tbl_user', 'tbl_user.uid = purchase_payment.payment_by', 'left')
            ->join('purchase', 'purchase.pur_id = purchase_payment.pur_purchase_id');
        if (!empty($from)) {
            $data  = $this->db->where('purchase.pur_created_on >=', $from);
        }
        if (!empty($to)) {
            $data  = $this->db->where('purchase.pur_created_on <=', $to);
        }
        $data =  $this->db->order_by('purchase.pur_id', 'asc')
            ->where('purchase.instant_id', $this->session->userdata('user')['inst_id'])
            ->get('purchase_payment')->result();
        return $data;
    }

    public function getsellPaymentsall($from, $to)
    {
        $data =  $this->db
            ->join('tbl_user', 'tbl_user.uid = sale_payment.salpay_by', 'left')
            ->join('sale', 'sale.sale_id = sale_payment.sal_sale_id');
        if (!empty($from)) {
            $data  = $this->db->where('sale.sale_created_on >=', $from);
        }
        if (!empty($to)) {
            $data  = $this->db->where('sale.sale_created_on <=', $to);
        }
        $data =  $this->db->order_by('sale.sale_id', 'asc')

            ->where('sale.instant_id', $this->session->userdata('user')['inst_id'])
            ->get('sale_payment')->result();
        return $data;
    }
}
